<section class="content-header">
    <h1>
        {!!$place->name  !!}
    </h1>
</section>



 <div class="form-group col-sm-12">
    {!! Form::label('text', 'Текст описания:') !!}
    {!! Form::textarea('text', $place->text,  ['class' => 'form-control', 'id'=>'ckeditor1']) !!}
</div>



<!-- Submit Field -->

<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('places.edit', $place->id) !!}" class="btn btn-default">Отмена</a>
</div>
