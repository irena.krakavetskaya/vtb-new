@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Подсказки
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body hint">
               <div class="row">
                   {!! Form::model($hint, ['route' => ['hints.update', $hint->id], 'method' => 'patch','enctype'=>'multipart/form-data']) !!}

                        @include('hints.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection