* VIP-ложи

===================================


Запрос:

    GET /api/v1/objects/4


Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 {
                 "id": 4,                 
                 "phone": "+7 (495) 111-36-99",
                 "imgs": [
                     "img_skybox/Ftf1ST1E8MLmyQfLij9xqV5Vh1lV8ZdJUXBiUOVs.jpeg",
                     "img_skybox/CwRKhnmSbPTEbLlCFK0liAc0Zlm2hhTFv9x34Ut6.jpeg"
                 ],
                 "adv": [
                     {
                         "name": "Уютное пространство от 12 до 34 персон",
                         "img": "img_skybox/Rlzwch1WNdCAdRbDTDkUkKretOMIxg37h5qR572o.jpeg"
                     },
                     {
                         "name": "Отдельный вход",
                         "img": "img_skybox/F9sdY2slEv3psOESbePHEj06p3yyr2VKNneuQGxP.jpeg"
                     },
                     {
                         "name": "Ресторанное обслуживание",
                         "img": "img_skybox/9D0CiP1B1KNDeYG6YTQmrRWU1WaYWVSvH7xDbDgj.jpeg"
                     },
                     {
                         "name": "Гарантированные парковочные места  (20 метров от VIP-входа)",
                         "img": "img_skybox/FbXyhnhCRyUXwGd9EArxFBR13TCsQ5CpFyIXv50m.jpeg"
                     },
                     {
                         "name": "Великолепный обзор с любой точки арены",
                         "img": "img_skybox/pDH1D6QJkQzz0wO0gB4s5pPq6g4pnomd8lsdjxbn.jpeg"
                     }
                 ]
             }