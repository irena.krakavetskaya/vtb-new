<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Image;

/**
 * Class Skybox
 * @package App\Models
 * @version January 29, 2018, 8:31 am UTC
 *
 * @property string name
 * @property string name_en
 * @property string img
 */
class Skybox extends Model
{
    //use SoftDeletes;

    public $table = 'skyboxes';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'name_en',
        'img'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_en' => 'string',
        'img' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        //'img' =>'required',
    ];

    public $timestamps = false;

    public function formatImg($file, $folder, $h, $w)
    {
        $img = time()."-".$file->getClientOriginalName();
        $file->move($folder, $img);
        Image::make(sprintf($folder.'/%s', $img))->fit($h, $w)->save();//fir / crop/resize
        $img= $folder.'/'.$img;
        return $img;
    }
}
