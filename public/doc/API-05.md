* Список переменных

===================================

Запрос:

    GET /api/v1/variables
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 {
                 "update_db": "2017-12-26 14:26:47",
                 "x_api_version": "1.01"
             }