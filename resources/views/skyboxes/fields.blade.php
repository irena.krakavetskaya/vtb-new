<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Название (English):') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>


<!-- Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('img', 'Иконка:') !!}<br>
    <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">

    @if(!empty($skybox->img))
        <input name="img3" type="hidden" value="{!! $skybox->img  !!}">
        <img src="/{!! $skybox->img !!}" class="img-responsive" id="uploadedimage1">
    @endif

    <img id="uploadedimage1" width="200px" />
    <span id="imageerror1" style="font-weight: bold; color: red"></span>

    {!! Form::file('img', ['id'=>'img', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Cохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pages.edit',16) !!}" class="btn btn-default">Отмена</a>
</div>

<!--
<div class="form-group col-sm-6">
!! Form::label('name_en', 'Name En:') !!}
!! Form::text('name_en', null, ['class' => 'form-control']) !!}
-->