<?php

namespace App\DataTables;

use App\Models\Team;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class TeamDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);
        return $dataTable
            ->editColumn('logo', function (Team $team) {
                return '<img class="img-responsive" src="' . $team->logo . '">';
            })
            ->addColumn('action', 'teams.datatables_actions')
            ->rawColumns(['logo', 'action']);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Team $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {


        return $this->builder()
            ->columns($this->getColumns())
            ->addAction(['width' => '10%','title'  => 'Действия'])
            ->minifiedAjax()
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => false,
                'pageLength' => 100,
                'order'   => [[1, 'asc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'buttons' => [
                    'create',
                    /*'print',
                    [
                        'extend'  => 'collection',
                        'text'    => '<i class="fa fa-download"></i> Export',
                        'buttons' => [
                            'csv',
                            'excel',

                        ],
                    ],*/
                    //'colvis'
                ]
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'logo' => ['name' => 'logo', 'data' => 'logo','searchable' => false,'orderable' => false,'title'=>'Логотип', 'width'=>100],
            'name' => ['name' => 'name', 'data' => 'name','title'=>'Команда'],
            'name_en'=> ['name' => 'name_en', 'data' => 'name_en','title'=>'Команда (En)'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'teamsdatatable_' . time();
    }
}