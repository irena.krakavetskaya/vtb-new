<?php

use App\Models\Doc;
use App\Repositories\DocRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DocRepositoryTest extends TestCase
{
    use MakeDocTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var DocRepository
     */
    protected $docRepo;

    public function setUp()
    {
        parent::setUp();
        $this->docRepo = App::make(DocRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateDoc()
    {
        $doc = $this->fakeDocData();
        $createdDoc = $this->docRepo->create($doc);
        $createdDoc = $createdDoc->toArray();
        $this->assertArrayHasKey('id', $createdDoc);
        $this->assertNotNull($createdDoc['id'], 'Created Doc must have id specified');
        $this->assertNotNull(Doc::find($createdDoc['id']), 'Doc with given id must be in DB');
        $this->assertModelData($doc, $createdDoc);
    }

    /**
     * @test read
     */
    public function testReadDoc()
    {
        $doc = $this->makeDoc();
        $dbDoc = $this->docRepo->find($doc->id);
        $dbDoc = $dbDoc->toArray();
        $this->assertModelData($doc->toArray(), $dbDoc);
    }

    /**
     * @test update
     */
    public function testUpdateDoc()
    {
        $doc = $this->makeDoc();
        $fakeDoc = $this->fakeDocData();
        $updatedDoc = $this->docRepo->update($fakeDoc, $doc->id);
        $this->assertModelData($fakeDoc, $updatedDoc->toArray());
        $dbDoc = $this->docRepo->find($doc->id);
        $this->assertModelData($fakeDoc, $dbDoc->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteDoc()
    {
        $doc = $this->makeDoc();
        $resp = $this->docRepo->delete($doc->id);
        $this->assertTrue($resp);
        $this->assertNull(Doc::find($doc->id), 'Doc should not exist in DB');
    }
}
