<!-- Name Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name_en', 'Название (English):') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>


<!-- Img Field -->
<div class="form-group col-sm-12 col-lg-12">
    <p>
        {!! Form::label('img', 'Изображение (рекоменд. 150*150 px):') !!}
        <input type="hidden" name="MAX_UPLOAD_SIZE" value="10000000">

        @if(!empty($album->img))
            <img src="{!! url("/") !!}/{!! $album->img !!}" class="img-responsive" id="uploadedimage1">
            <input name="img1" type="hidden" value="{!! $album->img  !!}">
        @endif

        {!! Form::file('img', null, ['class' => 'form-control','id'=>'img', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
        <img id="uploadedimage1" width="100px" />
    </p>
    <p>
        <span id="imageerror1" style="font-weight: bold; color: red"></span>
    </p>
</div>

<div class="form-group col-sm-12 col-lg-12">
    <input type="hidden" name="page_id" value="{{$page_id}}">
    @if($page_id==32)
        @isset($imgs)
            @include('pages.img', $imgs)
        @else
            @include('pages.img')
        @endisset
    @elseif($page_id==33)
            {!! Form::label('link', 'Ссылка на видео:') !!}
        @isset($imgs)
            {!! Form::text('link',$imgs->link, ['class' => 'form-control']) !!}
        @else
            {!! Form::text('link',null, ['class' => 'form-control']) !!}
        @endisset
    @endif

</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pages.edit', $page_id) !!}" class="btn btn-default">Отмена</a>
</div>
