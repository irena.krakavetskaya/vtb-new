<?php

use Faker\Factory as Faker;
use App\Models\Point;
use App\Repositories\PointRepository;

trait MakePointTrait
{
    /**
     * Create fake instance of Point and save it in database
     *
     * @param array $pointFields
     * @return Point
     */
    public function makePoint($pointFields = [])
    {
        /** @var PointRepository $pointRepo */
        $pointRepo = App::make(PointRepository::class);
        $theme = $this->fakePointData($pointFields);
        return $pointRepo->create($theme);
    }

    /**
     * Get fake instance of Point
     *
     * @param array $pointFields
     * @return Point
     */
    public function fakePoint($pointFields = [])
    {
        return new Point($this->fakePointData($pointFields));
    }

    /**
     * Get fake data of Point
     *
     * @param array $postFields
     * @return array
     */
    public function fakePointData($pointFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'position' => $fake->randomDigitNotNull,
            'name' => $fake->word,
            'name_en' => $fake->word,
            'text' => $fake->word,
            'text_en' => $fake->word,
            'place_id' => $fake->randomDigitNotNull,
            'floor' => $fake->randomDigitNotNull,
            'is_active' => $fake->word
        ], $pointFields);
    }
}
