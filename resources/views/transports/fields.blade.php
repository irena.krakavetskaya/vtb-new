<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Название (English):') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<!-- Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('img', 'Схема:') !!}<br>

        <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">

        @if(!empty($transport->img))
            <input name="img3" type="hidden" value="{!! $transport->img !!}">
            <img src="{!! url("/") !!}/{!! $transport->img !!}" class="img-responsive" id="uploadedimage1">
        @endif

        <img id="uploadedimage1" width="200px" />
        <span id="imageerror1" style="font-weight: bold; color: red"></span>

    {!! Form::file('img', ['id'=>'img_nav', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}

</div>
<div class="clearfix"></div>

<!-- Text Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('text', 'Описание:') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('text_en', 'Описание (English):') !!}
    {!! Form::textarea('text_en', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('transports.index') !!}" class="btn btn-default">Отмена</a>
</div>

<!--
<div class="form-group col-sm-6">
    !! Form::label('name_en', 'Name En:') !!}
!! Form::text('name_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-12 col-lg-12">
    !! Form::label('text_en', 'Text En:') !!}
!! Form::textarea('text_en', null, ['class' => 'form-control']) !!}
        </div>

-->