@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Как добраться
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($transport, ['route' => ['transports.update', $transport->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('transports.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection