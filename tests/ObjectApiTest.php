<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ObjectApiTest extends TestCase
{
    use MakeObjectTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateObject()
    {
        $object = $this->fakeObjectData();
        $this->json('POST', '/api/v1/objects', $object);

        $this->assertApiResponse($object);
    }

    /**
     * @test
     */
    public function testReadObject()
    {
        $object = $this->makeObject();
        $this->json('GET', '/api/v1/objects/'.$object->id);

        $this->assertApiResponse($object->toArray());
    }

    /**
     * @test
     */
    public function testUpdateObject()
    {
        $object = $this->makeObject();
        $editedObject = $this->fakeObjectData();

        $this->json('PUT', '/api/v1/objects/'.$object->id, $editedObject);

        $this->assertApiResponse($editedObject);
    }

    /**
     * @test
     */
    public function testDeleteObject()
    {
        $object = $this->makeObject();
        $this->json('DELETE', '/api/v1/objects/'.$object->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/objects/'.$object->id);

        $this->assertResponseStatus(404);
    }
}
