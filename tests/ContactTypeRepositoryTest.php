<?php

use App\Models\ContactType;
use App\Repositories\ContactTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactTypeRepositoryTest extends TestCase
{
    use MakeContactTypeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ContactTypeRepository
     */
    protected $contactTypeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->contactTypeRepo = App::make(ContactTypeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateContactType()
    {
        $contactType = $this->fakeContactTypeData();
        $createdContactType = $this->contactTypeRepo->create($contactType);
        $createdContactType = $createdContactType->toArray();
        $this->assertArrayHasKey('id', $createdContactType);
        $this->assertNotNull($createdContactType['id'], 'Created ContactType must have id specified');
        $this->assertNotNull(ContactType::find($createdContactType['id']), 'ContactType with given id must be in DB');
        $this->assertModelData($contactType, $createdContactType);
    }

    /**
     * @test read
     */
    public function testReadContactType()
    {
        $contactType = $this->makeContactType();
        $dbContactType = $this->contactTypeRepo->find($contactType->id);
        $dbContactType = $dbContactType->toArray();
        $this->assertModelData($contactType->toArray(), $dbContactType);
    }

    /**
     * @test update
     */
    public function testUpdateContactType()
    {
        $contactType = $this->makeContactType();
        $fakeContactType = $this->fakeContactTypeData();
        $updatedContactType = $this->contactTypeRepo->update($fakeContactType, $contactType->id);
        $this->assertModelData($fakeContactType, $updatedContactType->toArray());
        $dbContactType = $this->contactTypeRepo->find($contactType->id);
        $this->assertModelData($fakeContactType, $dbContactType->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteContactType()
    {
        $contactType = $this->makeContactType();
        $resp = $this->contactTypeRepo->delete($contactType->id);
        $this->assertTrue($resp);
        $this->assertNull(ContactType::find($contactType->id), 'ContactType should not exist in DB');
    }
}
