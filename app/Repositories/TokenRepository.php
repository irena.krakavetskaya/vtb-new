<?php

namespace App\Repositories;

use App\Models\Token;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TokenRepository
 * @package App\Repositories
 * @version July 7, 2018, 12:07 pm UTC
 *
 * @method Token findWithoutFail($id, $columns = ['*'])
 * @method Token find($id, $columns = ['*'])
 * @method Token first($columns = ['*'])
*/
class TokenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'uid',
        'token',
        'os'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Token::class;
    }

    public function getId($uid)
    {
        return $this->model->where('uid',$uid)->value('id');

    }
}
