<?php

use App\Models\Hint;
use App\Repositories\HintRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HintRepositoryTest extends TestCase
{
    use MakeHintTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var HintRepository
     */
    protected $hintRepo;

    public function setUp()
    {
        parent::setUp();
        $this->hintRepo = App::make(HintRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateHint()
    {
        $hint = $this->fakeHintData();
        $createdHint = $this->hintRepo->create($hint);
        $createdHint = $createdHint->toArray();
        $this->assertArrayHasKey('id', $createdHint);
        $this->assertNotNull($createdHint['id'], 'Created Hint must have id specified');
        $this->assertNotNull(Hint::find($createdHint['id']), 'Hint with given id must be in DB');
        $this->assertModelData($hint, $createdHint);
    }

    /**
     * @test read
     */
    public function testReadHint()
    {
        $hint = $this->makeHint();
        $dbHint = $this->hintRepo->find($hint->id);
        $dbHint = $dbHint->toArray();
        $this->assertModelData($hint->toArray(), $dbHint);
    }

    /**
     * @test update
     */
    public function testUpdateHint()
    {
        $hint = $this->makeHint();
        $fakeHint = $this->fakeHintData();
        $updatedHint = $this->hintRepo->update($fakeHint, $hint->id);
        $this->assertModelData($fakeHint, $updatedHint->toArray());
        $dbHint = $this->hintRepo->find($hint->id);
        $this->assertModelData($fakeHint, $dbHint->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteHint()
    {
        $hint = $this->makeHint();
        $resp = $this->hintRepo->delete($hint->id);
        $this->assertTrue($resp);
        $this->assertNull(Hint::find($hint->id), 'Hint should not exist in DB');
    }
}
