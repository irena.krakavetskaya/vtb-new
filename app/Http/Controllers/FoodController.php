<?php

namespace App\Http\Controllers;

use App\DataTables\FoodDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFoodRequest;
use App\Http\Requests\UpdateFoodRequest;
use App\Repositories\FoodRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FoodController extends AppBaseController
{
    /** @var  FoodRepository */
    private $foodRepository;

    public function __construct(FoodRepository $foodRepo)
    {
        $this->foodRepository = $foodRepo;
    }

    /**
     * Display a listing of the Food.
     *
     * @param FoodDataTable $foodDataTable
     * @return Response
     */
    public function index(FoodDataTable $foodDataTable)
    {
        return $foodDataTable->render('foods.index');
    }

    /**
     * Show the form for creating a new Food.
     *
     * @return Response
     */
    public function create()
    {
        return view('foods.create');
    }

    /**
     * Store a newly created Food in storage.
     *
     * @param CreateFoodRequest $request
     *
     * @return Response
     */
    public function store(CreateFoodRequest $request)
    {
        $input = $request->all();

        $file = $request->file('img');
        $file1 = $request->input('img1');
        if(isset($file)) {
            $img = $file->store('food');
        }
        else{
            $img =$file1?$file1:'';
        }
        $input=$request->all();
        $input['img'] = $img;
        $input['page_id'] = 20;

        $food = $this->foodRepository->create($input);

        Flash::success('Food saved successfully.');

        //return redirect(route('foods.index'));
        return redirect(route('pages.edit',20));
    }

    /**
     * Display the specified Food.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $food = $this->foodRepository->findWithoutFail($id);

        if (empty($food)) {
            Flash::error('Food not found');

            return redirect(route('foods.index'));
        }

        return view('foods.show')->with('food', $food);
    }

    /**
     * Show the form for editing the specified Food.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $food = $this->foodRepository->findWithoutFail($id);

        if (empty($food)) {
            Flash::error('Food not found');

            return redirect(route('foods.index'));
        }

        return view('foods.edit')->with('food', $food);
    }

    /**
     * Update the specified Food in storage.
     *
     * @param  int              $id
     * @param UpdateFoodRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFoodRequest $request)
    {
        $food = $this->foodRepository->findWithoutFail($id);

        if (empty($food)) {
            Flash::error('Food not found');
            return redirect(route('foods.index'));
        }

        $file = $request->file('img');
        $file1 = $request->input('img1');
        if(isset($file)) {
            $img = $file->store('food');
        }
        else{
            $img =$file1?$file1:'';
        }
        $requestData=$request->all();
        $requestData['img'] = $img;

        $food = $this->foodRepository->update($requestData, $id);

        Flash::success('Food updated successfully.');

        //return redirect(route('foods.index'));
        return redirect(route('pages.edit',20));
    }

    /**
     * Remove the specified Food from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $food = $this->foodRepository->findWithoutFail($id);

        if (empty($food)) {
            Flash::error('Food not found');

            return redirect(route('foods.index'));
        }

        $this->foodRepository->delete($id);

        return $id;

        //Flash::success('Food deleted successfully.');

        //return redirect(route('foods.index'));
    }
}
