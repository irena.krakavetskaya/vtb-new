<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateObjectAPIRequest;
use App\Http\Requests\API\UpdateObjectAPIRequest;
use App\Models\Doc;
use App\Models\Event;
use App\Models\Object;
use App\Models\Page;
use App\Models\RinkEvent;
use App\Models\RinkSchedule;
use App\Models\Schedule;
use App\Models\Service;
use App\Models\Skybox;
use App\Repositories\ObjectRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use App\Models\Translate;
/**
 * Class ObjectController
 * @package App\Http\Controllers\API
 */

class ObjectAPIController extends AppBaseController
{
    /** @var  ObjectRepository */
    private $objectRepository;

    public function __construct(ObjectRepository $objectRepo)
    {
        $this->objectRepository = $objectRepo;
    }

    /**
     * Display a listing of the Object.
     * GET|HEAD /objects
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->objectRepository->pushCriteria(new RequestCriteria($request));
        $this->objectRepository->pushCriteria(new LimitOffsetCriteria($request));
        $objects = $this->objectRepository->all();

        return $this->sendResponse($objects->toArray(), 'Objects retrieved successfully');
    }

    /**
     * Store a newly created Object in storage.
     * POST /objects
     *
     * @param CreateObjectAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateObjectAPIRequest $request)
    {
        $input = $request->all();

        $objects = $this->objectRepository->create($input);

        return $this->sendResponse($objects->toArray(), 'Object saved successfully');
    }

    public function getContacts($request)
    {



        //old version
        /*
        $object = new Object();
        $res= $object->getContacts();
        return $res;
        */
        //end old version
    }


    /**
     * Display the specified Object.
     * GET|HEAD /objects/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        /** @var Object $object */
        //only for id=1
        $object = $this->objectRepository->findWithoutFail($id);

        //return $object;

        /*

        if (empty($object)) {
            return $this->sendError('Object not found');
        }

        if($object->is_active==='true'){
            $usefull=$object->docs_api;
            $arr_usefull=[
                'title'=>Translate::where('front','usefull')->value('ru'),
                'pdf'=>$usefull
            ];
        }
        else{
            $arr_usefull=[];
        }
        */

        //if($id==1){
            /*
            $obj = $object->select('id','text','fb','inst','vk','youtube')->first(); //'img', 'phone','email','name','introtext',

            $ev = new Event();
            $evs= $object->events;
            $events=$ev->formatEvent($evs);
            $obj['useful'] =$arr_usefull;
            $obj['events'] =$events;

            //$services=Service::whereIn('id', [1,3])->select('id','name')->get();
            //$obj['services']=$services;
            */

       // }

        if($id==1){
            $page = Page::find(35);
            $contacts =$page->contacts()->get();
            //$phone =  $contacts->where('contact_type_id',1)->pluck('value');


            $palace = Page::find(9);
            $imgs=$palace->imgs->pluck('link');

            $lang = $request->header('X-Language');
            if($lang=='en'){
                $valPdf='link_en';
                $title =     $palace->pagetitle_en;
                $introtext =   $palace->introtext_en;
            }
            else{
                $valPdf='link';
                $title =     $palace->pagetitle;
                $introtext =   $palace->introtext;
            }


            $doc= new Doc();
            $pdf=$doc->getDocFields($request);
            $usefull=$pdf->where('page_id',9)->whereNotNull($valPdf)->get();//
            $arr_usefull=[
                'title'=>Translate::where('front','usefull')->value($lang),
                'pdf'=>$usefull
            ];

            $date_now= date('Y-m-d');
            $ev= new Event();
            $events=$ev->getEventFields($request);
            $evs= $events
                ->whereDate('date','>=', $date_now)
                ->orderBy('date','asc')
                ->limit(3)
                ->get();

            $arr =[
                'id'=>39, //?
                'pagetitle'=>$title,
                'text'=>$introtext,
                'imgs' => $imgs,
                'phone' => $contacts->where('contact_type_id',1)->pluck('value'),
                'fb'=> $contacts->where('contact_type_id',5)->pluck('value')->first(),
                'inst'=> $contacts->where('contact_type_id',6)->pluck('value')->first(),
                'vk'=> $contacts->where('contact_type_id',7)->pluck('value')->first(),
                'youtube'=> $contacts->where('contact_type_id',8)->pluck('value')->first(),
                'useful' => $arr_usefull,
                'events'=>$ev->formatEvent($evs,$lang)
            ];
        }
        elseif($id==4){
            $page = Page::find(16);
            $contacts =$page->contacts()->get();
            $phone =  $contacts->where('contact_type_id',1)->pluck('value');


            $imgs=$page->imgs->pluck('link');

            $arr =[
                'id'=>16, //?
                'pagetitle'=>$page->pagetitle,
                'imgs' => $imgs,
                'phone' => $phone,
                'adv' => Skybox::select('name', 'img')->get()
            ];
        }


        return $arr;//$obj;
        //return $this->sendResponse($object->toArray(), 'Object retrieved successfully');
    }

    /**
     * Update the specified Object in storage.
     * PUT/PATCH /objects/{id}
     *
     * @param  int $id
     * @param UpdateObjectAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjectAPIRequest $request)
    {
        $input = $request->all();

        /** @var Object $object */
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            return $this->sendError('Object not found');
        }

        $object = $this->objectRepository->update($input, $id);

        return $this->sendResponse($object->toArray(), 'Object updated successfully');
    }

    /**
     * Remove the specified Object from storage.
     * DELETE /objects/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Object $object */
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            return $this->sendError('Object not found');
        }

        $object->delete();

        return $this->sendResponse($id, 'Object deleted successfully');
    }
}
