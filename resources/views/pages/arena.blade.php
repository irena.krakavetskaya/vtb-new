<div class="form-group col-sm-12">
    @foreach($children as $child)
        <a href="{!! route('pages.edit',$child->id) !!}" class="btn btn-primary" target="_blank">{!!$child->pagetitle!!}</a>
    @endforeach
</div>

@include('pages.doc')