<?php

namespace App\Http\Controllers;

use App\DataTables\TeamDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTeamRequest;
use App\Http\Requests\UpdateTeamRequest;
use App\Models\Team;
use App\Repositories\TeamRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Response;

use Image;

class TeamController extends AppBaseController
{
    /** @var  TeamRepository */
    private $teamRepository;

    public function __construct(TeamRepository $teamRepo)
    {
        $this->teamRepository = $teamRepo;
    }

    /**
     * Display a listing of the Team.
     *
     * @param TeamDataTable $teamDataTable
     * @return Response
     */
    public function index(TeamDataTable $teamDataTable)
    {
        return $teamDataTable->render('teams.index');
    }

    /**
     * Show the form for creating a new Team.
     *
     * @return Response
     */
    public function create()
    {
        return view('teams.create');
    }

    /**
     * Store a newly created Team in storage.
     *
     * @param CreateTeamRequest $request
     *
     * @return Response
     */
    public function store(CreateTeamRequest $request)
    {
        $requestData = $request->all();

        $file = $request->file('logo');

        if(isset($file)) {
            $img = $file->store('logo');
            $team = new Team();
            //$img = $team->formatImg($file, 'logo', 50, 50);
            //$img = $team->formatImg($file, 'logo', 66, 66);
        }
        else{
            $img ='';
        }
        $requestData['logo'] = $img;

        $team = $this->teamRepository->create($requestData);

        Flash::success('Команда успешно добавлена.');

        return redirect(route('teams.index'));
    }

    /**
     * Display the specified Team.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $team = $this->teamRepository->findWithoutFail($id);



        if (empty($team)) {
            Flash::error('Команда не найдена');

            return redirect(route('teams.index'));
        }

        return view('teams.show')->with('team', $team);
    }

    /**
     * Show the form for editing the specified Team.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $team = $this->teamRepository->findWithoutFail($id);


        if (empty($team)) {
            Flash::error('Команда не найдена');

            return redirect(route('teams.index'));
        }

        return view('teams.edit')->with('team', $team);
    }

    /**
     * Update the specified Team in storage.
     *
     * @param  int              $id
     * @param UpdateTeamRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTeamRequest $request)
    {
        $team = $this->teamRepository->findWithoutFail($id);

        if (empty($team)) {
            Flash::error('Команда не найдена');
            return redirect(route('teams.index'));
        }


        $file = $request->file('logo');
        $file1 = $request->input('logo1');
        if(isset($file)) {
            $img = $file->store('logo');
            //$img = $team->formatImg($file, 'logo', 50, 50);
            //$img = $team->formatImg($file, 'logo', 66, 66);
        }
        else{
            $img =$file1?$file1:'';
        }


        $requestData = $request->all();
        $requestData['logo'] = $img;


        $team = $this->teamRepository->update($requestData, $id);
        Flash::success('Команда успешно изменена.');

        return redirect(route('teams.index'));
    }

    /**
     * Remove the specified Team from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $team = $this->teamRepository->findWithoutFail($id);

        if (empty($team)) {
            Flash::error('Команда не найдена');
            return redirect(route('teams.index'));
        }

        $ev1 = $team->events2->count('id');
        $ev2 = $team->events2->count('id');
        if($ev1>0 || $ev2>0){
           Flash::error('Удалить данную команду нельзя, т.к. она участвует в матче. Удалите сначала матчи, затем команду.');
        }
        else{
            $this->teamRepository->delete($id);
            Flash::success('Команда успешно удалена.');

        }
        return redirect(route('teams.index'));
    }

    public function getLogo(Request $request)
    {
        $id = $request->id;
        $team = $this->teamRepository->findWithoutFail($id);
        $logo = $team->logo;
        return $logo;

    }
}
