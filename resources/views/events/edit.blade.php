@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            События
        </h1>
        <div class="pull-left teams-btn">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('teams.index') !!}" target="_blank">Команды</a>
        </div>
   </section>
   <div class="content">
       <div class="clearfix"></div>
       @include('adminlte-templates::common.errors')

       <!--
       <ul class="nav nav-tabs" id="myTab" role="tablist">
           <li class="nav-item active">
               <a href="#events" class="nav-link active in"  data-toggle="tab"  role="tab" aria-controls="events" aria-selected="true">Событие</a>
           </li>
           <li class="nav-item">
               <a href="{!! route('teams.index') !!}"  role="tab" class="nav-link" aria-controls="teams" aria-selected="true"  >Команды</a></li>
           </li>
       </ul>
       <div class="tab-content" id="myTabContent">
           <div class="tab-pane fade show active in" id="events" role="tabpanel" aria-labelledby="events-tab">-->

               <div class="box box-primary">
                   <div class="box-body edit_event">
                       <div class="row">
                           {!! Form::model($event, ['route' => ['events.update', $event->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                           @if($event->type=='match')
                               @include('events.fields_match')
                           @else
                               @include('events.fields_show')
                           @endif

                           {!! Form::close() !!}
                       </div>
                   </div>
               </div>

           <!--</div>
           <div class="tab-pane fade" id="teams" role="tabpanel" aria-labelledby="teams-tab">
           </div>
       </div>-->
   </div>


@endsection