<?php

namespace App\Repositories;

use App\Models\Object;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ObjectRepository
 * @package App\Repositories
 * @version December 28, 2017, 11:12 am UTC
 *
 * @method Object findWithoutFail($id, $columns = ['*'])
 * @method Object find($id, $columns = ['*'])
 * @method Object first($columns = ['*'])
*/
class ObjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en',
        'introtext',
        'introtext_en',
        'text',
        'text_en',
        'img',
        'fb',
        'inst',
        'vk',
        'youtube'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Object::class;
    }
}
