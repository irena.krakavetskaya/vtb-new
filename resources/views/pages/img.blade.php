<fieldset>
    <legend>Фото</legend>
    <div class="form-group col-md-12 images">
        @isset($imgs)
            @foreach($imgs as $img)
                <div class="col-sm-2">
                    <p>
                        <input name="img_museum[]" type="hidden" value="{!!  $img->link  !!}">
                        <a href="/{!! $img->link !!}" data-toggle="lightbox">
                            <img src="/{!! $img->link !!}" class="img-responsive">
                        </a>
                    </p>
                    <p class="text-center">
                        <input name="del_img[]" type="hidden" value="{!! $img->id !!}" class="del_img">
                        <a href="#" class='btn btn-danger btn-sm delete-img' id="{!! $img->id !!}">
                            Удалить
                        </a>
                    </p>
                </div>
            @endforeach
        @endisset
        <div class="col-sm-2">
            <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
            <img id="uploadedimage_museum"  class="uploadedimage_museum"><!--height="160px"-->
            <span id="imageerror_museum" style="font-weight: bold; color: red"></span>
            {!! Form::file('img_museum[]', ['id'=>'img_museum', 'accept'=>'image/jpg,image/jpeg,image/png', 'class'=>'img_museum']) !!}
        </div>
    </div>
</fieldset>