<?php

namespace App\Http\Controllers;

use App\DataTables\HintDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateHintRequest;
use App\Http\Requests\UpdateHintRequest;
use App\Models\Hint;
use App\Models\Object;
use App\Repositories\HintRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class HintController extends AppBaseController
{
    /** @var  HintRepository */
    private $hintRepository;

    public function __construct(HintRepository $hintRepo)
    {
        $this->hintRepository = $hintRepo;
    }

    /**
     * Display a listing of the Hint.
     *
     * @param HintDataTable $hintDataTable
     * @return Response
     */
    public function index(HintDataTable $hintDataTable)
    {
        return $hintDataTable->render('hints.index');
    }

    /**
     * Show the form for creating a new Hint.
     *
     * @return Response
     */
    public function create()
    {
        $objects = Object::whereIn('id',[1,4,7])->get();
        return view('hints.create')->with('objects', $objects);
    }

    /**
     * Store a newly created Hint in storage.
     *
     * @param CreateHintRequest $request
     *
     * @return Response
     */
    public function store(CreateHintRequest $request)
    {
        $input = $request->all();

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');
        if(isset($file)) {
            $img = $file->store('img_navigation/hints');//'/'.
        }
        else{
            $img =$file1?$file1:'';
        }
        $input['img'] = $img;
        //end of add preview

        $hint = $this->hintRepository->create($input);

        Flash::success('Подсказка успешно создана.');

        return redirect(route('hints.index'));
    }

    /**
     * Display the specified Hint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $hint = $this->hintRepository->findWithoutFail($id);

        if (empty($hint)) {
            Flash::error('Подсказка не найдена');

            return redirect(route('hints.index'));
        }

        return view('hints.show')->with('hint', $hint);
    }

    /**
     * Show the form for editing the specified Hint.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $hint = $this->hintRepository->findWithoutFail($id);

        $objects = Object::whereIn('id',[1,4,7])->get();

        $arr_web=['http','https', 'www'];
        if(!empty($hint->img)){
            if(!str_contains($hint->img, $arr_web)){
                $hint->link=false;
                $hint->img=''.$hint->img;
            }
            else{
                $hint->link=true;
            }
        }



        if (empty($hint)) {
            Flash::error('Подсказка не найдена');

            return redirect(route('hints.index'));
        }

        return view('hints.edit')->with('hint', $hint)->with('objects', $objects);
    }

    /**
     * Update the specified Hint in storage.
     *
     * @param  int              $id
     * @param UpdateHintRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHintRequest $request)
    {
        $hint = $this->hintRepository->findWithoutFail($id);

        if (empty($hint)) {
            Flash::error('Подсказка не найдена');

            return redirect(route('hints.index'));
        }

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');
        if(isset($file)) {
            $img = $file->store('img_navigation/hints');
        }
        else{
            $img =$file1?$file1:'';
        }
        $requestData=$request->all();
        $requestData['img'] = $img;
        //end of add preview


        $hint = $this->hintRepository->update($requestData, $id);

        Flash::success('Подсказка успешно обновлена.');

        return redirect(route('hints.index'));
    }

    /**
     * Remove the specified Hint from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $hint = $this->hintRepository->findWithoutFail($id);

        if (empty($hint)) {
            Flash::error('Подсказка не найдена');

            return redirect(route('hints.index'));
        }

        $this->hintRepository->delete($id);

        Flash::success('Подсказка успешно удалена.');

        return redirect(route('hints.index'));
    }
}
