<input type="hidden" name="page_id" value="{{$page_id}}">

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_type', 'Тип контакта:') !!}

    <select id="contact_type" name="contact_type_id" class="form-control">
        @if(isset($contact->id))
            @foreach ($contact_types as $types)
                @if($types->id == $contact->contact_type_id)
                    <option value="{!! $types->id !!}" selected>{!! $types->name !!}</option>
                @else
                    <option value="{!! $types->id !!}">{!! $types->name !!}</option>
                @endif
            @endforeach
        @else
            @foreach ($contact_types as $types)
                <option value="{!! $types->id !!}">{!! $types->name !!}</option>
            @endforeach
        @endif
    </select>
</div>
<div class="clearfix"></div>

<!-- Value Field -->
<div class="form-group col-sm-6">
    {!! Form::label('value', 'Контакт:') !!}
    {!! Form::text('value', null, ['class' => 'form-control']) !!}
</div>

    @if(isset($contact) && ($contact->contact_type_id==3 || $contact->contact_type_id==4) || !isset($contact))
        <div class="form-group col-sm-6">
            {!! Form::label('value_en', 'Контакт (English):') !!}
            {!! Form::text('value_en', null, ['class' => 'form-control']) !!}
        </div>
    @endif
<div class="clearfix"></div>


<div class="form-group col-sm-6">
    {!! Form::label('description', 'Пояснение:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('description_en', 'Пояснение (English):') !!}
    {!! Form::text('description_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pages.edit', $page_id) !!}" class="btn btn-default">Отмена</a>
</div>
