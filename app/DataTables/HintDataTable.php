<?php

namespace App\DataTables;

use App\Models\Hint;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class HintDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'hints.datatables_actions')
            ->editColumn('img', function(Hint $hint) {
                $str ='<img src="'.$hint->img.'" class="img-responsive">';
                return $str;
            })
            ->editColumn('object_id', function(Hint $hint) {
                return $hint->object->name;
            })
            ->editColumn('text', function(Hint $hint) {
                $len=100;
                $what = $hint->text;
                if (strlen($what) > $len) {
                    $what = preg_replace('/^(.{' . $len. ',}? ).*$/is', '$1', $what) . '...';
                }
                return '<div class="text">'.$what.'<div>';
            })
            ->editColumn('text_en', function(Hint $hint) {
                $len=100;
                $what = $hint->text_en;
                if (strlen($what) > $len) {
                    $what = preg_replace('/^(.{' . $len. ',}? ).*$/is', '$1', $what) . '...';
                }
                return '<div class="text">'.$what.'<div>';
            })
            ->rawColumns(['text','text_en','img', 'action']);

        //return $dataTable->addColumn('action', 'hints.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Hint $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'pageLength' => 50,
                'searching'=> false,
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'buttons' => [
                    'create'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id'=>['name' => 'id', 'data' => 'id','title'=>'№', 'width'=>10],
            'name'=>['name' => 'name', 'data' => 'name','title'=>'Название', 'width'=>20],
            'name_en'=>['name' => 'name_en', 'data' => 'name_en','title'=>'Название (English)', 'width'=>20],
            'text'=>['name' => 'text', 'data' => 'text','title'=>'Текст', 'width'=>40],
            'text_en'=>['name' => 'text_en', 'data' => 'text_en','title'=>'Текст (English)', 'width'=>40],
            //'img'=>['name' => 'img', 'data' => 'img','title'=>'Изображение','searchable' => false,'orderable'=> false, 'width'=>20],
            'object_id'=>['name' => 'object_id', 'data' => 'object_id','title'=>'Объект', 'width'=>20],

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'hintsdatatable_' . time();
    }
}