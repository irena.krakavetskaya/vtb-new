<?php

namespace App\Repositories;

use App\Models\Floor;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class FloorRepository
 * @package App\Repositories
 * @version January 10, 2018, 12:28 pm UTC
 *
 * @method Floor findWithoutFail($id, $columns = ['*'])
 * @method Floor find($id, $columns = ['*'])
 * @method Floor first($columns = ['*'])
*/
class FloorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en',
        'place_id',
        'img'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Floor::class;
    }
}
