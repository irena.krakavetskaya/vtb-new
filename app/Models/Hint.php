<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Hint
 * @package App\Models
 * @version January 22, 2018, 9:37 am UTC
 *
 * @property string name
 * @property string text
 * @property string img
 * @property integer floor_id
 */
class Hint extends Model
{
    //use SoftDeletes;

    public $table = 'hints';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'text',
        'img',
        'object_id',
        'name_en',
        'text_en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'text' => 'string',
        'img' => 'string',
        'object_id' => 'integer',
        'name_en'=> 'string',
        'text_en' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public $timestamps = false;

    public function object()
    {
        return $this->belongsTo('App\Models\Object');
    }
}
