<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSkyboxAPIRequest;
use App\Http\Requests\API\UpdateSkyboxAPIRequest;
use App\Models\Skybox;
use App\Repositories\SkyboxRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class SkyboxController
 * @package App\Http\Controllers\API
 */

class SkyboxAPIController extends AppBaseController
{
    /** @var  SkyboxRepository */
    private $skyboxRepository;

    public function __construct(SkyboxRepository $skyboxRepo)
    {
        $this->skyboxRepository = $skyboxRepo;
    }

    /**
     * Display a listing of the Skybox.
     * GET|HEAD /skyboxes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->skyboxRepository->pushCriteria(new RequestCriteria($request));
        $this->skyboxRepository->pushCriteria(new LimitOffsetCriteria($request));
        $skyboxes = $this->skyboxRepository->all();

        return $this->sendResponse($skyboxes->toArray(), 'Skyboxes retrieved successfully');
    }

    /**
     * Store a newly created Skybox in storage.
     * POST /skyboxes
     *
     * @param CreateSkyboxAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSkyboxAPIRequest $request)
    {
        $input = $request->all();

        $skyboxes = $this->skyboxRepository->create($input);

        return $this->sendResponse($skyboxes->toArray(), 'Skybox saved successfully');
    }

    /**
     * Display the specified Skybox.
     * GET|HEAD /skyboxes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Skybox $skybox */
        $skybox = $this->skyboxRepository->findWithoutFail($id);

        if (empty($skybox)) {
            return $this->sendError('Skybox not found');
        }

        return $this->sendResponse($skybox->toArray(), 'Skybox retrieved successfully');
    }

    /**
     * Update the specified Skybox in storage.
     * PUT/PATCH /skyboxes/{id}
     *
     * @param  int $id
     * @param UpdateSkyboxAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSkyboxAPIRequest $request)
    {
        $input = $request->all();

        /** @var Skybox $skybox */
        $skybox = $this->skyboxRepository->findWithoutFail($id);

        if (empty($skybox)) {
            return $this->sendError('Skybox not found');
        }

        $skybox = $this->skyboxRepository->update($input, $id);

        return $this->sendResponse($skybox->toArray(), 'Skybox updated successfully');
    }

    /**
     * Remove the specified Skybox from storage.
     * DELETE /skyboxes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Skybox $skybox */
        $skybox = $this->skyboxRepository->findWithoutFail($id);

        if (empty($skybox)) {
            return $this->sendError('Skybox not found');
        }

        $skybox->delete();

        return $this->sendResponse($id, 'Skybox deleted successfully');
    }
}
