<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTranslateAPIRequest;
use App\Http\Requests\API\UpdateTranslateAPIRequest;
use App\Models\Translate;
use App\Repositories\TranslateRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use Illuminate\Support\Facades\Schema;
/**
 * Class TranslateController
 * @package App\Http\Controllers\API
 */

class TranslateAPIController extends AppBaseController
{
    /** @var  TranslateRepository */
    private $translateRepository;

    public function __construct(TranslateRepository $translateRepo)
    {
        $this->translateRepository = $translateRepo;
    }

    /**
     * Display a listing of the Translate.
     * GET|HEAD /translates
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->translateRepository->pushCriteria(new RequestCriteria($request));
        //$this->translateRepository->pushCriteria(new LimitOffsetCriteria($request));
        $translates = $this->translateRepository->all();

        $arr=array();
        foreach ($translates as $key=>$val){
            if($val->front!=''){
                $arr[$val->front] = $val->ru;
            }
        }
        return $arr;
        //return $this->sendResponse($translates->toArray(), 'Translates retrieved successfully');
    }

    public function showLang()
    {
        $columns = Schema::getColumnListing('translates');

        $lang = [];
        foreach ($columns as $column) {
            if (!in_array($column, [
                'id',
                'description',
                'front'
            ])) {
                $lang[] = $column;
            }
        }

        return $lang;
    }

    public function showMenu()
    {
        $menu= Translate::whereIn('id',[22,23,24,25,26])->get();
        $arr =[];

        foreach ($menu as $key=>$val){
            array_push($arr,$val->ru);
        }

        return $arr;
    }

    /**
     * Store a newly created Translate in storage.
     * POST /translates
     *
     * @param CreateTranslateAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTranslateAPIRequest $request)
    {
        $input = $request->all();

        $translates = $this->translateRepository->create($input);

        return $this->sendResponse($translates->toArray(), 'Translate saved successfully');
    }

    /**
     * Display the specified Translate.
     * GET|HEAD /translates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Translate $translate */
        $translate = $this->translateRepository->findWithoutFail($id);

        if (empty($translate)) {
            return $this->sendError('Translate not found');
        }

        return $this->sendResponse($translate->toArray(), 'Translate retrieved successfully');
    }

    /**
     * Update the specified Translate in storage.
     * PUT/PATCH /translates/{id}
     *
     * @param  int $id
     * @param UpdateTranslateAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTranslateAPIRequest $request)
    {
        $input = $request->all();

        /** @var Translate $translate */
        $translate = $this->translateRepository->findWithoutFail($id);

        if (empty($translate)) {
            return $this->sendError('Translate not found');
        }

        $translate = $this->translateRepository->update($input, $id);

        return $this->sendResponse($translate->toArray(), 'Translate updated successfully');
    }

    /**
     * Remove the specified Translate from storage.
     * DELETE /translates/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Translate $translate */
        $translate = $this->translateRepository->findWithoutFail($id);

        if (empty($translate)) {
            return $this->sendError('Translate not found');
        }

        $translate->delete();

        return $this->sendResponse($id, 'Translate deleted successfully');
    }
}
