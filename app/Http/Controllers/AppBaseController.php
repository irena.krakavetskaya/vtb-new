<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Token;
use App\Models\Variable;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;
use Unisender\ApiWrapper\UnisenderApi;

use Illuminate\Http\Request;
/**
 * @SWG\Swagger(
 *   basePath="/api/v1",
 *   @SWG\Info(
 *     title="Laravel Generator APIs",
 *     version="1.0.0",
 *   )
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function connectUnisender()
    {
        $platform = 'VTB Ice Palace';
        $uni = new UnisenderApi('api key here', 'UTF-8', 4, null, false, $platform);

        return $uni;
    }

    public function getList($uni)
    {
        $result=$uni->getLists(); //{"result":[{"id":353358}]}
        $list = json_decode($result);
        //var_dump($list);
        return $list->id;
    }




    public function makeCurl($post, $url)
    {
        // Устанавливаем соединение
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_URL,
            'https://api.unisender.com/ru/api/'.$url.'?format=json');
        $result = curl_exec($ch);

        if ($result) {
            // Раскодируем ответ API-сервера
            $jsonObj = json_decode($result);

            if(null===$jsonObj) {
                // Ошибка в полученном ответе
                //echo "Invalid JSON";

            }
            elseif(!empty($jsonObj->error)) {
                // Ошибка создания сообщения
                //echo "An error occured: " . $jsonObj->error . "(code: " . $jsonObj->code . ")";

            } else {
                // Success
                return  $jsonObj;
            }
        } else {
            // Ошибка соединения с API-сервером
            //echo "API access error";
        }

    }


    public function sendPush(Request $request){

        $title = $request->title;
        $body = $request->body;
        $data = $request->data;
        //$tokens = $request->tokens_arr;

        //for test
        /*
        $tokens_arr = array();
        $token1 = \DB::table('customer2device')
            ->where('customer_id', 1026)
            ->value('token');
        $tokens_arr = [$token1];
        */


        //for all users
        $tokens_arr=[];
        $tokens = Token::select('token')
            ->get();
        foreach($tokens as  $val){
            array_push($tokens_arr,$val->token);
        }

        $serverKey = '';//'AAAAeWv_tDo:APA91bFr2rT9jh2iBzxghaqIzzi8X5ObKy3bi2p3abudjBYCGBEmuNys-a9mgIYMJRc6ztlr2hXnEdE8LlzY__cVlr1PLL91WthtJiRVWcnfAjQyx1cngFE6qAxFghUmRpKnHqq0kTgv';

        $headers = array(
            'Authorization:key=' . $serverKey,
            'Content-Type:application/json'
        );
        $regIdChunk=array_chunk($tokens_arr,1000);


        foreach($regIdChunk as $token){

            $fields = array(
                "to" => "/topics/all",
                "registration_tokens" =>$token
            );
            //return  $fields;

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://iid.googleapis.com/iid/v1:batchAdd');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            //return $result;//{"results":[{"error":"INVALID_ARGUMENT"}]}  Empty results indicate successful subscription for the token

            /*
             *
             * ! NOT_FOUND — The registration token has been deleted or the app has been uninstalled.
                INVALID_ARGUMENT — The registration token provided is not valid for the Sender ID.
                INTERNAL — The backend server failed for unknown reasons. Retry the request.
                TOO_MANY_TOPICS — Excessive number of topics per app instance.
             */

            //second curl
            $fields = array(
                "to" => "/topics/all",

                'notification' => [
                    'title' => $title,
                    'body' => $body
                ],
                'data' => $data
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            curl_exec($ch);
            $result1 = curl_getinfo($ch, CURLINFO_HTTP_CODE); //200
            curl_close($ch);
            //$mes=json_decode($result1);
            //return $result1;//200

            //third curl - delete tokens
            $fields = array(
                "to" => "/topics/all",
                "registration_tokens" => $token
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://iid.googleapis.com/iid/v1:batchRemove');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result2 = curl_exec($ch);
            curl_close($ch);
            //return $result2;
            //$mes=json_decode($result2);
        }


        if(isset($result1) && $result1==200){
            return json_encode(['response'=>'success']);//{"response":"success"}
        }
        else{
            return json_encode(['response'=>'error']);
        }

    }


    //???
    public function pushInterval()
    {

        $minutes = 10;
        $dataTask = Notification::whereRaw('push_datetime > DATE_SUB(NOW(), INTERVAL ' . $minutes . ' MINUTE) AND push_datetime < DATE_SUB(NOW(), INTERVAL ' . ($minutes - 10) . ' MINUTE)')
            ->get();


        if (!$dataTask->isEmpty()){
            foreach ($dataTask as $task) {
                $request = new Request();
                //$request->tokens_arr = $tokens_arr;
                $request->title = $task->title;
                $request->body = $task->introtext;
                $request->data = ['id_event' => $task->id];
                $result = $this->sendPush($request);//!!!
            }
        }


        if (!$dataTask->isEmpty()){
            return 'Пуш-уведомление успешно отправлено.';
        }

    }

}
