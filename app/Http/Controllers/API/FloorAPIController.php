<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFloorAPIRequest;
use App\Http\Requests\API\UpdateFloorAPIRequest;
use App\Models\Floor;
use App\Repositories\FloorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class FloorController
 * @package App\Http\Controllers\API
 */

class FloorAPIController extends AppBaseController
{
    /** @var  FloorRepository */
    private $floorRepository;

    public function __construct(FloorRepository $floorRepo)
    {
        $this->floorRepository = $floorRepo;
    }

    /**
     * Display a listing of the Floor.
     * GET|HEAD /floors
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->floorRepository->pushCriteria(new RequestCriteria($request));
        $this->floorRepository->pushCriteria(new LimitOffsetCriteria($request));
        $floors = $this->floorRepository->all();

        return $this->sendResponse($floors->toArray(), 'Floors retrieved successfully');
    }

    /**
     * Store a newly created Floor in storage.
     * POST /floors
     *
     * @param CreateFloorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFloorAPIRequest $request)
    {
        $input = $request->all();

        $floors = $this->floorRepository->create($input);

        return $this->sendResponse($floors->toArray(), 'Floor saved successfully');
    }

    /**
     * Display the specified Floor.
     * GET|HEAD /floors/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Floor $floor */
        $floor = $this->floorRepository->findWithoutFail($id);

        if (empty($floor)) {
            return $this->sendError('Floor not found');
        }

        return $this->sendResponse($floor->toArray(), 'Floor retrieved successfully');
    }

    /**
     * Update the specified Floor in storage.
     * PUT/PATCH /floors/{id}
     *
     * @param  int $id
     * @param UpdateFloorAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFloorAPIRequest $request)
    {
        $input = $request->all();

        /** @var Floor $floor */
        $floor = $this->floorRepository->findWithoutFail($id);

        if (empty($floor)) {
            return $this->sendError('Floor not found');
        }

        $floor = $this->floorRepository->update($input, $id);

        return $this->sendResponse($floor->toArray(), 'Floor updated successfully');
    }

    /**
     * Remove the specified Floor from storage.
     * DELETE /floors/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Floor $floor */
        $floor = $this->floorRepository->findWithoutFail($id);

        if (empty($floor)) {
            return $this->sendError('Floor not found');
        }

        $floor->delete();

        return $this->sendResponse($id, 'Floor deleted successfully');
    }
}
