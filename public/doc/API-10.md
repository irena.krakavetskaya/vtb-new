* Бюро находок

===================================


Запрос:

    GET /api/v1/services/3


Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 {
                 "id": 3,
                 "name": "Бюро находок",
                 "email": "mailbox@mailbox.ru",
                 "phone": "+7 909 689-07-50",
                 "additional": "График работы: будние дни с 9:00 до 18:00."
             }