* Телефоны и адреса

===================================

Запрос:

    GET /api/v1/get-contacts
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 {
                 "contacts": [
                     {
                         "name": "Парк Легенд",
                         "phone": "+7 495 796-86-80",
                         "email": "info@parklegends.ru",
                         "address": "Москва, ул. Автозаводская, вл. 23",
                         "openHours": null
                     },
                     {
                         "name": "ВТБ Ледовый дворец",
                         "phone": "+7 495 139-12-02",
                         "email": "ld@parklegends.ru",
                         "address": "Москва, ул. Автозаводская, вл. 23А",
                         "openHours": null
                     },
                     {
                         "name": "Музей Хоккея",
                         "phone": "+7 495 223-01-37",
                         "email": "info@hockeymuseum.ru",
                         "address": "Москва, ул. Автозаводская, вл. 23, к. 3",
                         "openHours": "График работы: вторник–воскресенье с 12 до 20 часов."
                     },
                     {
                         "name": "Кассы",
                         "phone": "+7 905 530-38-98",
                         "email": null,
                         "address": "ул. Автозаводская, вл. 23, напротив ВТБ Ледового Дворца",
                         "openHours": "График работы: ежедневно с 10 до 20 часов."
                     }
                 ],
                 "socialNetworks": {
                     "fb": "https://www.facebook.com/ParkIegend?ref=hl",
                     "inst": "https://www.instagram.com/park_legend/",
                     "vk": "https://vk.com/park_legend",
                     "youtube": "http://www.youtube.com/user/ParkIegend",
                     "name": "Парк Легенд в соцсетях"
                 }
             }