@component('mail::message')
    {{-- Greeting --}}
    @if (! empty($greeting))
        # {{ $greeting }}
    @else
        @if ($level == 'error')
            # Whoops!
        @else
            Добрый день!
        @endif
    @endif

    {{-- Intro Lines --}}
    @foreach ($introLines as $line)
        <!--{ $line }}-->

    @endforeach

    {{-- Action Button --}}
    @if (isset($actionText))
        <?php
        switch ($level) {
            case 'success':
                $color = 'green';
                break;
            case 'error':
                $color = 'red';
                break;
            default:
                $color = 'blue';
        }
        ?>
        @component('mail::button', ['url' => $actionUrl, 'color' => $color])
            <!--{ $actionText }}-->
            Сброс пароля
        @endcomponent
    @endif

    {{-- Outro Lines --}}
    <!--foreach ($outroLines as $line)
{ $line }}
endforeach-->

    Вы получили это письмо, потому что запросили сброс пароля со своего аккаунта VTB Admin.
    <!-- Salutation -->
    @if (! empty($salutation))
        {{ $salutation }}
    @else
        <br>С уважением,<br>Команда VTB<!--{ config('app.name') }}-->
    @endif

    <!-- Subcopy -->
    <!--if (isset($actionText))
    component('mail::subcopy')
    If you’re having trouble clicking the "{ $actionText }}" button, copy and paste the URL below
    into your web browser: [{ $actionUrl }}]({ $actionUrl }})
    endcomponent
    endif
    endcomponent
    -->