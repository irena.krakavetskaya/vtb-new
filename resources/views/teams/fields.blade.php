<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название команды:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Название команды (English):') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-12">
    <!--!! Form::label('logo', 'Logo:') !!}
    !! Form::file('logo', null, ['class' => 'form-control']) !!}-->



<p>
    {!! Form::label('logo', 'Лого команды (рекоменд. 150*150 px):') !!}
    <input type="hidden" name="MAX_UPLOAD_SIZE" value="10000000">

    @if(!empty($team->logo))
        <img src="{!! url("/") !!}/{!! $team->logo !!}" class="img-responsive" id="uploadedimage">
        <input name="logo1" type="hidden" value="{!! $team->logo  !!}">
    @endif

    {!! Form::file('logo', null, ['class' => 'form-control','id'=>'logo', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
    <!--<img id="uploadedimage" width="500px" />-->
    <img id="uploadedimage" width="100px" />
</p>
<p>
    <span id="imageerror" style="font-weight: bold; color: red"></span>
</p>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('teams.index') !!}" class="btn btn-default">Отмена</a>
</div>
