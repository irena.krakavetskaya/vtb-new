<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DocApiTest extends TestCase
{
    use MakeDocTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateDoc()
    {
        $doc = $this->fakeDocData();
        $this->json('POST', '/api/v1/docs', $doc);

        $this->assertApiResponse($doc);
    }

    /**
     * @test
     */
    public function testReadDoc()
    {
        $doc = $this->makeDoc();
        $this->json('GET', '/api/v1/docs/'.$doc->id);

        $this->assertApiResponse($doc->toArray());
    }

    /**
     * @test
     */
    public function testUpdateDoc()
    {
        $doc = $this->makeDoc();
        $editedDoc = $this->fakeDocData();

        $this->json('PUT', '/api/v1/docs/'.$doc->id, $editedDoc);

        $this->assertApiResponse($editedDoc);
    }

    /**
     * @test
     */
    public function testDeleteDoc()
    {
        $doc = $this->makeDoc();
        $this->json('DELETE', '/api/v1/docs/'.$doc->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/docs/'.$doc->id);

        $this->assertResponseStatus(404);
    }
}
