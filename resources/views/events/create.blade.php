@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">
            События
        </h1>
        <div class="pull-left teams-btn">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('teams.index') !!}" target="_blank">Команды</a>
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body create_event">
                <div class="row">
                    {!! Form::open(['route' => 'events.store','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('events.fields_create')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
