<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDocAPIRequest;
use App\Http\Requests\API\UpdateDocAPIRequest;
use App\Models\Doc;
use App\Repositories\DocRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class DocController
 * @package App\Http\Controllers\API
 */

class DocAPIController extends AppBaseController
{
    /** @var  DocRepository */
    private $docRepository;

    public function __construct(DocRepository $docRepo)
    {
        $this->docRepository = $docRepo;
    }

    /**
     * Display a listing of the Doc.
     * GET|HEAD /docs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->docRepository->pushCriteria(new RequestCriteria($request));
        $this->docRepository->pushCriteria(new LimitOffsetCriteria($request));
        $docs = $this->docRepository->all();

        return $this->sendResponse($docs->toArray(), 'Docs retrieved successfully');
    }

    /**
     * Store a newly created Doc in storage.
     * POST /docs
     *
     * @param CreateDocAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDocAPIRequest $request)
    {
        $input = $request->all();

        $docs = $this->docRepository->create($input);

        return $this->sendResponse($docs->toArray(), 'Doc saved successfully');
    }

    /**
     * Display the specified Doc.
     * GET|HEAD /docs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Doc $doc */
        $doc = $this->docRepository->findWithoutFail($id);

        if (empty($doc)) {
            return $this->sendError('Doc not found');
        }

        return $this->sendResponse($doc->toArray(), 'Doc retrieved successfully');
    }

    /**
     * Update the specified Doc in storage.
     * PUT/PATCH /docs/{id}
     *
     * @param  int $id
     * @param UpdateDocAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDocAPIRequest $request)
    {
        $input = $request->all();

        /** @var Doc $doc */
        $doc = $this->docRepository->findWithoutFail($id);

        if (empty($doc)) {
            return $this->sendError('Doc not found');
        }

        $doc = $this->docRepository->update($input, $id);

        return $this->sendResponse($doc->toArray(), 'Doc updated successfully');
    }

    /**
     * Remove the specified Doc from storage.
     * DELETE /docs/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Doc $doc */
        $doc = $this->docRepository->findWithoutFail($id);

        if (empty($doc)) {
            return $this->sendError('Doc not found');
        }

        $doc->delete();

        return $this->sendResponse($id, 'Doc deleted successfully');
    }
}
