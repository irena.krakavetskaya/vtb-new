<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $token->id !!}</p>
</div>

<!-- Uid Field -->
<div class="form-group">
    {!! Form::label('uid', 'Uid:') !!}
    <p>{!! $token->uid !!}</p>
</div>

<!-- Token Field -->
<div class="form-group">
    {!! Form::label('token', 'Token:') !!}
    <p>{!! $token->token !!}</p>
</div>

<!-- Os Field -->
<div class="form-group">
    {!! Form::label('os', 'Os:') !!}
    <p>{!! $token->os !!}</p>
</div>

