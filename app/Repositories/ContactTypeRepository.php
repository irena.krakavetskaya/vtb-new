<?php

namespace App\Repositories;

use App\Models\ContactType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ContactTypeRepository
 * @package App\Repositories
 * @version July 10, 2018, 12:09 pm UTC
 *
 * @method ContactType findWithoutFail($id, $columns = ['*'])
 * @method ContactType find($id, $columns = ['*'])
 * @method ContactType first($columns = ['*'])
*/
class ContactTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'type',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ContactType::class;
    }
}
