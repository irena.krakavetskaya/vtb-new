<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PointApiTest extends TestCase
{
    use MakePointTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePoint()
    {
        $point = $this->fakePointData();
        $this->json('POST', '/api/v1/points', $point);

        $this->assertApiResponse($point);
    }

    /**
     * @test
     */
    public function testReadPoint()
    {
        $point = $this->makePoint();
        $this->json('GET', '/api/v1/points/'.$point->id);

        $this->assertApiResponse($point->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePoint()
    {
        $point = $this->makePoint();
        $editedPoint = $this->fakePointData();

        $this->json('PUT', '/api/v1/points/'.$point->id, $editedPoint);

        $this->assertApiResponse($editedPoint);
    }

    /**
     * @test
     */
    public function testDeletePoint()
    {
        $point = $this->makePoint();
        $this->json('DELETE', '/api/v1/points/'.$point->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/points/'.$point->id);

        $this->assertResponseStatus(404);
    }
}
