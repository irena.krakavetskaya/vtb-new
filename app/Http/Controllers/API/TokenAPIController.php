<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTokenAPIRequest;
use App\Http\Requests\API\UpdateTokenAPIRequest;
use App\Models\Token;
use App\Repositories\TokenRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TokenController
 * @package App\Http\Controllers\API
 */

class TokenAPIController extends AppBaseController
{
    /** @var  TokenRepository */
    private $tokenRepository;

    public function __construct(TokenRepository $tokenRepo)
    {
        $this->tokenRepository = $tokenRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/tokens",
     *      summary="Get a listing of the Tokens.",
     *      tags={"Token"},
     *      description="Get all Tokens",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Token")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->tokenRepository->pushCriteria(new RequestCriteria($request));
        $this->tokenRepository->pushCriteria(new LimitOffsetCriteria($request));
        $tokens = $this->tokenRepository->all();

        return $this->sendResponse($tokens->toArray(), 'Tokens retrieved successfully');
    }

    /**
     * @param CreateTokenAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/tokens",
     *      summary="Store a newly created Token in storage",
     *      tags={"Token"},
     *      description="Store Token",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Token that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Token")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Token"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTokenAPIRequest $request)
    {
        $input = $request->all();

        $id= $this->tokenRepository->getId($request->uid); //Token::where('uid',$request->uid)->value('id');
        if(isset($id)){
            $tokens = $this->tokenRepository->update($input, $id);
        }
        else{
            $tokens = $this->tokenRepository->create($input);
        }

        //return $this->sendResponse($tokens->toArray(), 'Token saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/tokens/{id}",
     *      summary="Display the specified Token",
     *      tags={"Token"},
     *      description="Get Token",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Token",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Token"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Token $token */
        $token = $this->tokenRepository->findWithoutFail($id);

        if (empty($token)) {
            return $this->sendError('Token not found');
        }

        return $this->sendResponse($token->toArray(), 'Token retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTokenAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/tokens/{id}",
     *      summary="Update the specified Token in storage",
     *      tags={"Token"},
     *      description="Update Token",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Token",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Token that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Token")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Token"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTokenAPIRequest $request)
    {
        $input = $request->all();

        /** @var Token $token */
        $token = $this->tokenRepository->findWithoutFail($id);

        if (empty($token)) {
            return $this->sendError('Token not found');
        }

        $token = $this->tokenRepository->update($input, $id);

        return $this->sendResponse($token->toArray(), 'Token updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/tokens/{id}",
     *      summary="Remove the specified Token from storage",
     *      tags={"Token"},
     *      description="Delete Token",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Token",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Token $token */
        $token = $this->tokenRepository->findWithoutFail($id);

        if (empty($token)) {
            return $this->sendError('Token not found');
        }

        $token->delete();

        return $this->sendResponse($id, 'Token deleted successfully');
    }
}
