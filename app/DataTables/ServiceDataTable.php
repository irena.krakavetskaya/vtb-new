<?php

namespace App\DataTables;

use App\Models\Service;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ServiceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'services.datatables_actions')
            ->editColumn('img', function(Service $service) {
                $str ='<img src="'.$service->img.'" class="img-responsive">';
                return $str;
            })
            ->editColumn('text', function(Service $service) {
                if(!empty($service->text)){
                    $len=540;
                    $what = $service->text;
                    if (strlen($what) > $len) {
                        $what = preg_replace('/^(.{' . $len. ',}? ).*$/is', '$1', $what) . '...';
                    }
                    return '<div class="text">'.$what.'<div>';
                }
                else{
                    return "-";
                }
            })
            ->rawColumns(['text','img', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Service $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'pageLength' => 50,
                'searching'=> false,
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'buttons' => [
                    'create',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name' => 'id', 'data' => 'id','title'=>'ID', 'width'=>20],
            'name' => ['name' => 'name', 'data' => 'name','title'=>'Название', 'width'=>20],
            //'img'=> ['name' => 'img', 'data' => 'img','searchable' => false,'orderable'=> false,'title'=>'Превью', 'width'=>20],
            'email'=> ['name' => 'email', 'data' => 'email','title'=>'E-mail', 'width'=>20],
            'phone'=> ['name' => 'phone', 'data' => 'phone','title'=>'Телефон', 'width'=>20],
            'additional'=> ['name' => 'additional', 'data' => 'additional','title'=>'Доп. текст', 'width'=>20],
            'text'=> ['name' => 'text', 'data' => 'text','title'=>'Описание', 'width'=>20],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'servicesdatatable_' . time();
    }
}