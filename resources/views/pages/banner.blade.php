<fieldset class="skybox-adv">
    <legend>Баннеры</legend>
        @foreach($banners as $banner)
            <div class="banner">
                <div class="col-md-3">
                    <a href="/{!! $banner->img !!}" data-toggle="lightbox">
                        <img src="/{!! $banner->img !!}" class="img-responsive">
                    </a>
                </div>

                <div class="col-md-3 text">
                    {!! $banner->name !!}
                </div>

                <div class="col-md-3 text">
                    <a href="{!! $banner->link !!}" target="_blank">{!! $banner->link !!}</a>
                </div>

                <div class="btn-group col-md-3">
                    <a href="{{ route('banners.edit', $banner->id) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs delete_item',
                        'id'=>$banner->id
                    ]) !!}
                </div>
                <div class="clearfix"></div>
            </div>
        @endforeach
    <div class="form-group col-sm-6">
        <a href="{!! route('banners.create') !!}" class='btn btn-primary btn-sm'>
            Добавить баннер
        </a>
    </div>
</fieldset>