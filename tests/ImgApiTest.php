<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImgApiTest extends TestCase
{
    use MakeImgTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateImg()
    {
        $img = $this->fakeImgData();
        $this->json('POST', '/api/v1/imgs', $img);

        $this->assertApiResponse($img);
    }

    /**
     * @test
     */
    public function testReadImg()
    {
        $img = $this->makeImg();
        $this->json('GET', '/api/v1/imgs/'.$img->id);

        $this->assertApiResponse($img->toArray());
    }

    /**
     * @test
     */
    public function testUpdateImg()
    {
        $img = $this->makeImg();
        $editedImg = $this->fakeImgData();

        $this->json('PUT', '/api/v1/imgs/'.$img->id, $editedImg);

        $this->assertApiResponse($editedImg);
    }

    /**
     * @test
     */
    public function testDeleteImg()
    {
        $img = $this->makeImg();
        $this->json('DELETE', '/api/v1/imgs/'.$img->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/imgs/'.$img->id);

        $this->assertResponseStatus(404);
    }
}
