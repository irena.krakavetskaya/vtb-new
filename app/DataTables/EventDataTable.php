<?php

namespace App\DataTables;

use App\Models\Event;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class EventDataTable extends DataTable
{


    protected $type;
    protected $past;
    protected $date_from;
    protected $date_till;

    public function forCategory1($type, $past,$date_from,$date_till) {
        $this->type = $type;
        $this->past = $past;
        $this->date_from = $date_from;
        $this->date_till = $date_till;
        return $this;
    }
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {

        $dataTable = new EloquentDataTable($query);
        return $dataTable
            ->editColumn('date', function (Event $event) {
                return date('d.m.Y',strtotime($event->date));
            })
            ->editColumn('time', function (Event $event) {
                return date('H:i',strtotime($event->time));
            })
            ->editColumn('type', function(Event $event) {
                $status="";
                switch ($event->type){
                    case 'match':$status='Матч';break;
                    case 'show':$status='Мероприятие';break;
                    case 'excursion':$status='Экскурсия';break;
                }
                return $status;
            })
            ->editColumn('place_id', function(Event $event) {
                return $event->places['name'];
            })
            ->editColumn('title', function(Event $event) {
                if($event->type=='match'){
                    $name1 = $event->team1['name'];
                    $name2 = $event->team2['name'];
                    $logo1 = $event->team1['logo'];
                    $logo2 = $event->team2['logo'];
                    $str ='<span class="command"><img src="'.$logo1.'" class="img-responsive"><span>'.$name1.'</span></span><span><img src="'.$logo2.'" class="img-responsive"><span>'.$name2.'</span></span>';
                }
                else{
                    $str =$event->name;
                }
                return $str;
            })
            ->editColumn('kind', function(Event $event) {
                return $event->kind;
            })
            ->editColumn('is_important', function(Event $event) {
                $is_important = $event->is_important==='true'?'+':'';
                return $is_important;
            })
            ->editColumn('is_adv', function(Event $event) {
                $is_adv = $event->is_adv==='true'?'+':'';
                return $is_adv;
            })
            ->editColumn('img', function(Event $event) {
                return '<a href="'.$event->img.'" data-toggle="lightbox"><img src="'.$event->img.'" class="img-responsive"></a>';
            })
            ->addColumn('action', 'events.datatables_actions')
            ->rawColumns(['date','title', 'action','img']);

        //return $dataTable->addColumn('action', 'events.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Event $model)
    {
        //return $model->newQuery();


        $now = date('Y-m-d');

        if($this->past=='hide'){
            $events = Event::where('date','>=',$now);
                //->select('id','date','time','name','type','kind','place_id','team1_id','team2_id','is_important','is_adv','img');
        }
        else{
            $events = Event::select('id','date','time','name','type','kind','place_id','team1_id','team2_id','is_important','is_adv','img');
        }
        if($this->type!='all'){
            $events = $events->where('type',$this->type);
        }
        if($this->date_from!=''){
            $events = $events->where('date','>=',$this->date_from);
        }
        if($this->date_till!=''){
            $events = $events->where('date','<=',$this->date_till);
        }


        return $this->applyScopes($events);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '60px','title'  => 'Действия'])

            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'pageLength' => 50,
                'searching'=> false,
               'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],

                'buttons' => [
                'create',
                    //'export',
                   // 'print',
                   // 'reset',
                    //'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'date' => ['name' => 'date', 'data' => 'date','searchable' => false,'title'=>'Дата', 'width'=>10],
            'time' => ['name' => 'time', 'data' => 'time','searchable' => false,'title'=>'Время', 'width'=>10],
            'type' => ['name' => 'type', 'data' => 'type','searchable' => false,'title'=>'Тип события', 'width'=>20],
            'place_id' => ['name' => 'place_id', 'data' => 'place_id','searchable' => false,'title'=>'Место проведения', 'width'=>20],
            'title' => ['name' => 'title', 'data' => 'title','searchable' => false,'orderable' => false,'title'=>'Команды / Название', 'width'=>100],
            'kind' => ['name' => 'kind', 'data' => 'kind','searchable' => false,'title'=>'Лига / Вид мероприятия', 'width'=>20],
            'is_important' => ['name' => 'is_important', 'data' => 'is_important','title'=>'Показывать на главной', 'width'=>20],
            'is_adv' => ['name' => 'is_adv', 'data' => 'is_adv','title'=>'Баннер', 'width'=>20],
            'img' => ['name' => 'img', 'data' => 'img','title'=>'Изображение', 'width'=>20],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'eventsdatatable_' . time();
    }
}