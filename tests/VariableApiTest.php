<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VariableApiTest extends TestCase
{
    use MakeVariableTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateVariable()
    {
        $variable = $this->fakeVariableData();
        $this->json('POST', '/api/v1/variables', $variable);

        $this->assertApiResponse($variable);
    }

    /**
     * @test
     */
    public function testReadVariable()
    {
        $variable = $this->makeVariable();
        $this->json('GET', '/api/v1/variables/'.$variable->id);

        $this->assertApiResponse($variable->toArray());
    }

    /**
     * @test
     */
    public function testUpdateVariable()
    {
        $variable = $this->makeVariable();
        $editedVariable = $this->fakeVariableData();

        $this->json('PUT', '/api/v1/variables/'.$variable->id, $editedVariable);

        $this->assertApiResponse($editedVariable);
    }

    /**
     * @test
     */
    public function testDeleteVariable()
    {
        $variable = $this->makeVariable();
        $this->json('DELETE', '/api/v1/variables/'.$variable->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/variables/'.$variable->id);

        $this->assertResponseStatus(404);
    }
}
