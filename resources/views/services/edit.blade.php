@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Услуги Парка Легенд
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($service, ['route' => ['services.update', $service->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('services.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection