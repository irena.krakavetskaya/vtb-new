<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateVariableAPIRequest;
use App\Http\Requests\API\UpdateVariableAPIRequest;
use App\Models\Variable;
use App\Repositories\VariableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class VariableController
 * @package App\Http\Controllers\API
 */

class VariableAPIController extends AppBaseController
{
    /** @var  VariableRepository */
    private $variableRepository;

    public function __construct(VariableRepository $variableRepo)
    {
        $this->variableRepository = $variableRepo;
    }

    /**
     * Display a listing of the Variable.
     * GET|HEAD /variables
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->variableRepository->pushCriteria(new RequestCriteria($request));
        //$this->variableRepository->pushCriteria(new LimitOffsetCriteria($request));
        $variables = $this->variableRepository->all();

        $arr=array();
        foreach ($variables as $key=>$val){
            $arr[$val->key] = $val->value;
        }
        return $arr;

        //return $this->sendResponse($variables->toArray(), 'Variables retrieved successfully');
    }

    /**
     * Store a newly created Variable in storage.
     * POST /variables
     *
     * @param CreateVariableAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateVariableAPIRequest $request)
    {
        $input = $request->all();

        $variables = $this->variableRepository->create($input);

        return $this->sendResponse($variables->toArray(), 'Variable saved successfully');
    }

    /**
     * Display the specified Variable.
     * GET|HEAD /variables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Variable $variable */
        $variable = $this->variableRepository->findWithoutFail($id);

        if (empty($variable)) {
            return $this->sendError('Variable not found');
        }

        return $this->sendResponse($variable->toArray(), 'Variable retrieved successfully');
    }

    /**
     * Update the specified Variable in storage.
     * PUT/PATCH /variables/{id}
     *
     * @param  int $id
     * @param UpdateVariableAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateVariableAPIRequest $request)
    {
        $input = $request->all();

        /** @var Variable $variable */
        $variable = $this->variableRepository->findWithoutFail($id);

        if (empty($variable)) {
            return $this->sendError('Variable not found');
        }

        $variable = $this->variableRepository->update($input, $id);

        return $this->sendResponse($variable->toArray(), 'Variable updated successfully');
    }

    /**
     * Remove the specified Variable from storage.
     * DELETE /variables/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Variable $variable */
        $variable = $this->variableRepository->findWithoutFail($id);

        if (empty($variable)) {
            return $this->sendError('Variable not found');
        }

        $variable->delete();

        return $this->sendResponse($id, 'Variable deleted successfully');
    }
}
