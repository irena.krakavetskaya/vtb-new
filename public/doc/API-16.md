* Токены

===================================


Запрос:

     POST /api/v1/tokens
        
     {
         "uid":"123589641235893",
         "os": "{ios|android}",
         "token": "1234567890"
     }

Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01    
   
    
Ответ:

    HTTP/1.1 422 Unprocessable Entity

    {
             "message": "The given data was invalid.",
             "errors": {
                 "uid": [
                     "Поле uid обязательно для заполнения."
                 ],
                 "token": [
                     "Поле token обязательно для заполнения."
                 ],
                 "os": [
                     "Выбранное значение для os ошибочно."
                 ]
             }
    }

	    