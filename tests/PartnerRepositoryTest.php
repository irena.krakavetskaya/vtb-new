<?php

use App\Models\Partner;
use App\Repositories\PartnerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartnerRepositoryTest extends TestCase
{
    use MakePartnerTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PartnerRepository
     */
    protected $partnerRepo;

    public function setUp()
    {
        parent::setUp();
        $this->partnerRepo = App::make(PartnerRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePartner()
    {
        $partner = $this->fakePartnerData();
        $createdPartner = $this->partnerRepo->create($partner);
        $createdPartner = $createdPartner->toArray();
        $this->assertArrayHasKey('id', $createdPartner);
        $this->assertNotNull($createdPartner['id'], 'Created Partner must have id specified');
        $this->assertNotNull(Partner::find($createdPartner['id']), 'Partner with given id must be in DB');
        $this->assertModelData($partner, $createdPartner);
    }

    /**
     * @test read
     */
    public function testReadPartner()
    {
        $partner = $this->makePartner();
        $dbPartner = $this->partnerRepo->find($partner->id);
        $dbPartner = $dbPartner->toArray();
        $this->assertModelData($partner->toArray(), $dbPartner);
    }

    /**
     * @test update
     */
    public function testUpdatePartner()
    {
        $partner = $this->makePartner();
        $fakePartner = $this->fakePartnerData();
        $updatedPartner = $this->partnerRepo->update($fakePartner, $partner->id);
        $this->assertModelData($fakePartner, $updatedPartner->toArray());
        $dbPartner = $this->partnerRepo->find($partner->id);
        $this->assertModelData($fakePartner, $dbPartner->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePartner()
    {
        $partner = $this->makePartner();
        $resp = $this->partnerRepo->delete($partner->id);
        $this->assertTrue($resp);
        $this->assertNull(Partner::find($partner->id), 'Partner should not exist in DB');
    }
}
