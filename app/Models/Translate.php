<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Translate
 * @package App\Models
 * @version December 26, 2017, 2:05 pm UTC
 *
 * @property string description
 * @property string front
 * @property string ru
 * @property string en
 */
class Translate extends Model
{
    //use SoftDeletes;

    public $table = 'translates';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'description',
        'front',
        'ru',
        'en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'description' => 'string',
        'front' => 'string',
        'ru' => 'string',
        'en' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'ru' => 'required'
    ];

    public $timestamps = false;

    public function getRu($front)
    {
        return $this->where('front', $front)->first()->ru;
    }
}
