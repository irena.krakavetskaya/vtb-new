<?php

use Faker\Factory as Faker;
use App\Models\Award;
use App\Repositories\AwardRepository;

trait MakeAwardTrait
{
    /**
     * Create fake instance of Award and save it in database
     *
     * @param array $awardFields
     * @return Award
     */
    public function makeAward($awardFields = [])
    {
        /** @var AwardRepository $awardRepo */
        $awardRepo = App::make(AwardRepository::class);
        $theme = $this->fakeAwardData($awardFields);
        return $awardRepo->create($theme);
    }

    /**
     * Get fake instance of Award
     *
     * @param array $awardFields
     * @return Award
     */
    public function fakeAward($awardFields = [])
    {
        return new Award($this->fakeAwardData($awardFields));
    }

    /**
     * Get fake data of Award
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAwardData($awardFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->text,
            'img' => $fake->text,
            'text' => $fake->text
        ], $awardFields);
    }
}
