<?php

namespace App\Repositories;

use App\Models\Point;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PointRepository
 * @package App\Repositories
 * @version January 9, 2018, 9:53 am UTC
 *
 * @method Point findWithoutFail($id, $columns = ['*'])
 * @method Point find($id, $columns = ['*'])
 * @method Point first($columns = ['*'])
*/
class PointRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'position',
        'name',
        'name_en',
        'text',
        'text_en',
        'place_id',
        'floor',
        'is_active'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Point::class;
    }
}
