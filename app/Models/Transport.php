<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Transport
 * @package App\Models
 * @version January 24, 2018, 9:50 am UTC
 *
 * @property string name
 * @property string name_en
 * @property string text
 * @property string text_en
 * @property string img
 */
class Transport extends Model
{
    //use SoftDeletes;

    public $table = 'transports';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'name_en',
        'text',
        'text_en',
        'img'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_en' => 'string',
        'text' => 'string',
        'text_en' => 'string',
        'img' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public $timestamps = false;

    /*
    public function getSchema($id, $lang)
    {
        if($lang=='ru'){
            $res = $this->select('id','name','text','img')->where('id',$id)->first();
        }
        else{
            $res = $this->select('id','name_en','text_en','img')->where('id',$id)->first();
        }
        return  $res;
    }
    */
}
