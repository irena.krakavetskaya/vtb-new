<?php

namespace App\Repositories;

use App\Models\Doc;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DocRepository
 * @package App\Repositories
 * @version December 28, 2017, 1:06 pm UTC
 *
 * @method Doc findWithoutFail($id, $columns = ['*'])
 * @method Doc find($id, $columns = ['*'])
 * @method Doc first($columns = ['*'])
*/
class DocRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'link',
        'name_en',
        'object_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Doc::class;
    }
}
