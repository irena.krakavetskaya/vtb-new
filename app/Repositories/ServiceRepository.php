<?php

namespace App\Repositories;

use App\Models\Doc;
use App\Models\Page;
use App\Models\Service;
use InfyOm\Generator\Common\BaseRepository;


/**
 * Class ServiceRepository
 * @package App\Repositories
 * @version January 17, 2018, 9:11 am UTC
 *
 * @method Service findWithoutFail($id, $columns = ['*'])
 * @method Service find($id, $columns = ['*'])
 * @method Service first($columns = ['*'])
*/
class ServiceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en',
        'text',
        'text_en',
        'img',
        'email',
        'phone'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Service::class;
    }

    public function getFields($request, $model=null)
    {
        $lang = $request->header('X-Language');
        $model = $model?$model:new Page();
        if($lang=='en'){
            return $model->select('id','pagetitle_en as name','introtext_en AS additional','content_en as text');
        }
        else{
            return $model->select('id','pagetitle as name','introtext AS additional','content as text');
        }
    }




}
