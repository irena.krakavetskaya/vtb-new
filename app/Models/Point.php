<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Point
 * @package App\Models
 * @version January 9, 2018, 9:53 am UTC
 *
 * @property integer position
 * @property string name
 * @property string name_en
 * @property string text
 * @property string text_en
 * @property integer place_id
 * @property integer floor
 * @property string is_active
 */
class Point extends Model
{
    //use SoftDeletes;

    public $table = 'points';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'position',
        'name',
        'name_en',
        'text',
        'text_en',
        'place_id',
        'floor',
        'is_active',
        'img',
        'img_thumb'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'position' => 'integer',
        'name' => 'string',
        'name_en' => 'string',
        'text' => 'string',
        'text_en' => 'string',
        'place_id' => 'integer',
        'floor' => 'integer',
        'is_active' => 'string',
        'img' => 'string',
        'img_thumb' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        //'place_id' => 'required'
    ];

    public function getPoints($id) //api
    {

        $arr=array();

        $floors=Place::find($id)->floors;
        foreach($floors as $key=>$floor) {
            $points= Point::select('id','name','img')
                ->where([
                    ['is_active', '=', 'true'],
                    ['place_id', '=', $id],
                    ['floor_id', '=', $floor->id],
                ])
                ->get();


            $arr['floors'][$floor->id]['sectors'] = $floor->img1;
            $arr['floors'][$floor->id]['schema'] = $floor->img2;
            $arr['floors'][$floor->id]['points'] = $points;
        }
        return $arr;

        //old version
        /*
        $points_place= Point::select(array('floor_id'))
            ->where([
                ['is_active', '=', 'true'],
                ['place_id', '=', $id],
            ])
            ->groupBy('floor_id')
            ->get();

          foreach($points_place as $key=>$val) {
                $points = Point::select(array('id', 'name', 'img'))//'text', $arr[$val->floor_id]
                ->where('floor_id', '=', $val->floor_id)
                    ->get();
                $arr[$val->floor_id]['points'] = $points;
            }

        return $arr;
       */
    }

    public function getPointsOnFloor($id,$floor) //api
    {
        $points = Point::select(array('id','name', 'img'))//, 'floor', 'place_id', 'is_active' 'text',
            ->where([
                ['is_active', '=', 'true'],
                ['place_id', '=', $id],
                ['floor_id', '=', $floor],
            ])
            ->get();

        $floor=Floor::where('id',$floor)->first();
        $arr=array();
        $arr['sectors'] = $floor->img1;
        $arr['schema'] = $floor->img2;
        $arr['points'] = $points;

        return $arr;
    }


    public function getPoints1($id) //admin
    {
        $evs= Point::select('id', 'name', 'is_active', 'text','floor_id','place_id', 'img','img_thumb')
            ->where('place_id',$id)
            ->get();

        $evs = $evs->each(function ($item) {
            $len=450;
            $what = $item->text;
            if (strlen($what) > $len) {
                $what = preg_replace('/^(.{' . $len. ',}? ).*$/is', '$1', $what) . '...';
            }
            $item['text'] = $what;
        });
        return $evs;

    }


    public function place()
    {
        return $this->belongsTo('App\Models\Place', 'place_id');
    }

    public function floor()
    {
        return $this->belongsTo('App\Models\Floor', 'floor_id');
    }


    public $timestamps = false;
}
