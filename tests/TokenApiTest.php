<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TokenApiTest extends TestCase
{
    use MakeTokenTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateToken()
    {
        $token = $this->fakeTokenData();
        $this->json('POST', '/api/v1/tokens', $token);

        $this->assertApiResponse($token);
    }

    /**
     * @test
     */
    public function testReadToken()
    {
        $token = $this->makeToken();
        $this->json('GET', '/api/v1/tokens/'.$token->id);

        $this->assertApiResponse($token->toArray());
    }

    /**
     * @test
     */
    public function testUpdateToken()
    {
        $token = $this->makeToken();
        $editedToken = $this->fakeTokenData();

        $this->json('PUT', '/api/v1/tokens/'.$token->id, $editedToken);

        $this->assertApiResponse($editedToken);
    }

    /**
     * @test
     */
    public function testDeleteToken()
    {
        $token = $this->makeToken();
        $this->json('DELETE', '/api/v1/tokens/'.$token->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/tokens/'.$token->id);

        $this->assertResponseStatus(404);
    }
}
