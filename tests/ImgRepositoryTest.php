<?php

use App\Models\Img;
use App\Repositories\ImgRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImgRepositoryTest extends TestCase
{
    use MakeImgTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ImgRepository
     */
    protected $imgRepo;

    public function setUp()
    {
        parent::setUp();
        $this->imgRepo = App::make(ImgRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateImg()
    {
        $img = $this->fakeImgData();
        $createdImg = $this->imgRepo->create($img);
        $createdImg = $createdImg->toArray();
        $this->assertArrayHasKey('id', $createdImg);
        $this->assertNotNull($createdImg['id'], 'Created Img must have id specified');
        $this->assertNotNull(Img::find($createdImg['id']), 'Img with given id must be in DB');
        $this->assertModelData($img, $createdImg);
    }

    /**
     * @test read
     */
    public function testReadImg()
    {
        $img = $this->makeImg();
        $dbImg = $this->imgRepo->find($img->id);
        $dbImg = $dbImg->toArray();
        $this->assertModelData($img->toArray(), $dbImg);
    }

    /**
     * @test update
     */
    public function testUpdateImg()
    {
        $img = $this->makeImg();
        $fakeImg = $this->fakeImgData();
        $updatedImg = $this->imgRepo->update($fakeImg, $img->id);
        $this->assertModelData($fakeImg, $updatedImg->toArray());
        $dbImg = $this->imgRepo->find($img->id);
        $this->assertModelData($fakeImg, $dbImg->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteImg()
    {
        $img = $this->makeImg();
        $resp = $this->imgRepo->delete($img->id);
        $this->assertTrue($resp);
        $this->assertNull(Img::find($img->id), 'Img should not exist in DB');
    }
}
