<fieldset class="skybox-adv">
    <legend>Новости</legend>
    @foreach($children as $child)
        <div class="page">
            <div class="col-md-3">
                {!! $child->pagetitle !!}
            </div>
            <div class="col-md-2">
                @foreach($children_img as $key=>$val)
                    @if($key == $child->id && !empty($val))
                            <a href="/{!! $val[0] !!}" data-toggle="lightbox">
                                <img src="/{!! $val[0] !!}" class="img-responsive">
                            </a>
                    @endif
                @endforeach
            </div>
            <div class='col-md-3 text'>
                {!! $child->content !!}
            </div>
            <div class='col-md-2'>
                {!! date('d-m-Y', strtotime($child->created_at)) !!}
            </div>

            <div class='btn-group col-md-2'>
                <a href="{{ route('pages.edit', $child->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs delete_item',
                    'id'=>$child->id
                ]) !!}
            </div>
            <div class="clearfix"></div>
        </div>
    @endforeach
    <div class="form-group col-sm-6">
        <a href="{!! route('pages.create', ['parent_id'=>$page->id]) !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
            Добавить новость
        </a>
    </div>
</fieldset>