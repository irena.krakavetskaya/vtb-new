<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Название (English):') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Logo Field -->
<div class="form-group col-sm-12 col-lg-12">
    <p>
        {!! Form::label('img', 'Изображение (рекоменд. 150*150 px):') !!}
        <input type="hidden" name="MAX_UPLOAD_SIZE" value="10000000">

        @if(!empty($partner->img))
            <img src="{!! url("/") !!}/{!! $partner->img !!}" class="img-responsive" id="uploadedimage1">
            <input name="img1" type="hidden" value="{!! $partner->img  !!}">
        @endif

        {!! Form::file('img', null, ['class' => 'form-control','id'=>'img', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
        <img id="uploadedimage1" width="100px" />
    </p>
    <p>
        <span id="imageerror1" style="font-weight: bold; color: red"></span>
    </p>
</div>

<!-- Site Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('site', 'Сайт:') !!}
    {!! Form::text('site', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pages.edit', 13) !!}" class="btn btn-default">Отмена</a>
</div>
