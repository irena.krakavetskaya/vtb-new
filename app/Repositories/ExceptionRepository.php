<?php

namespace App\Repositories;

use App\Models\Exception;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ExceptionRepository
 * @package App\Repositories
 * @version January 4, 2018, 2:32 pm UTC
 *
 * @method Exception findWithoutFail($id, $columns = ['*'])
 * @method Exception find($id, $columns = ['*'])
 * @method Exception first($columns = ['*'])
*/
class ExceptionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'date_from',
        'date_till',
        'time_from',
        'time_till',
        'all_day',
        'work'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Exception::class;
    }
}
