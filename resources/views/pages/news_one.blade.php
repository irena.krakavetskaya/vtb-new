<fieldset class="gallery">
    <legend>Галерея</legend>
    <div class="form-group col-md-12">
            <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
            <img id="uploadedimage_museum" height="160px" class="uploadedimage_museum">
            <span id="imageerror_museum" style="font-weight: bold; color: red"></span>
            {!! Form::file('img_museum[]', ['id'=>'img_museum', 'accept'=>'image/jpg,image/jpeg,image/png', 'class'=>'img_museum']) !!}
    </div>
</fieldset>