
    <!-- Sidebar Menu
    <ul class="sidebar-menu" data-widget="tree" data-api="tree" data-accordion=0>-->
        <!--<li class="header">HEADER</li>-->
        <li class="treeview">
            <a href="#"><i class="fa fa-edit"></i> <span>Главная</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::is('objects/1/edit') ? 'active' : '' }}"><!--objects*-->
                    <a href="{{ route('objects.edit', 1) }}"><span>Ледовая арена</span></a>
                </li>



                <li class="{{ Request::is('objects/4/edit') ? 'active' : '' }}">
                    <a href="{{ route('objects.edit', 4) }}"><span>VIP-ложи</span></a>
                </li>

            </ul>
        </li>


        <li class="{{ Request::is('events*') ? 'active' : '' }}">
            <a href="{!! route('events.index') !!}"><i class="fa fa-edit"></i><span>Афиша</span></a>
        </li>


        <li class="{{ Request::is('services*') ? 'active' : '' }}">
            <a href="{!! route('services.index') !!}"><i class="fa fa-edit"></i><span>Услуги</span></a>
        </li>

        <li class="treeview">
            <a href="#"><i class="fa fa-edit"></i><span>Контакты</span>
                <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                <li class="{{ Request::is('objects/edit-contacts*') ? 'active' : '' }}">
                    <a href="{!! route('objects.index') !!}"><span>Телефоны и адреса</span></a>
                </li>

                <li class="{{Request::is('transports*') ? 'active' : '' }}">
                    <a href="{{ route('transports.index') }}"><span>Как добраться</span></a>
                </li>

                <li class="{{ Request::is('places/5/edit') ? 'active' : '' }}">
                    <a href="{{ route('places.edit', 5) }}"><span>Паркинг</span></a>
                </li>

            </ul>

    </li>

    <li class="{{ Request::is('hints*') ? 'active' : '' }}">
        <a href="{!! route('hints.index') !!}"><i class="fa fa-edit"></i><span>Подсказки</span></a>
    </li>


    <br>
    @foreach ($firstLevel as $link)
        @if($link->isfolder==1)
            <li class=""><!--treeview-->
                <a href="{{ route('pages.edit', $link->id) }}" class="dropdown-toggle"><i class="fa fa-edit"></i> <span>{{ $link->pagetitle }}</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!--foreach (\App\Models\Page::with('items')->where('parent_id',$link->id)->get()  as $sublink)-->
                     @foreach ($page->getChildren($link->id)  as $sublink)
                        <li class="{{ Request::is($sublink->alias) ? 'active' : '' }}">
                            <a href="{{ route('pages.edit', $sublink->id) }}"><span>{{ $sublink->pagetitle }}</span></a>
                        </li>
                    @endforeach
                </ul>
            </li>
       @else
            <li class="{{ Request::is($link->alias) ? 'active' : '' }}">
                <a href="{{ route('pages.edit', $link->id) }}"><i class="fa fa-edit"></i><span>{{ $link->pagetitle }}</span></a>
            </li>
       @endif
    @endforeach


    <br>
    <li class="{{ Request::is('emails*') ? 'active' : '' }}">
        <a href="{!! route('emails.index') !!}"><i class="fa fa-edit"></i><span>Emails для рассылки</span></a>
    </li>

    <li class="{{ Request::is('notifications*') ? 'active' : '' }}">
        <a href="{!! route('notifications.index') !!}"><i class="fa fa-edit"></i><span>Текст для рассылок</span></a>
    </li>

