<?php

namespace App\Repositories;

use App\Models\Hint;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class HintRepository
 * @package App\Repositories
 * @version January 22, 2018, 9:37 am UTC
 *
 * @method Hint findWithoutFail($id, $columns = ['*'])
 * @method Hint find($id, $columns = ['*'])
 * @method Hint first($columns = ['*'])
*/
class HintRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'text',
        'img',
        'floor_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Hint::class;
    }

    public function getFields($request, $model=null)
    {
        $lang = $request->header('X-Language');
        $model = $model?$model:new Hint();
        if($lang=='en'){
                return $model->select('id','name_en as name','text_en  as text','img','object_id');
        }
        else{
                return $model->select('id','name','text','img','object_id');
        }
    }
}
