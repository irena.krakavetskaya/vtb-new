<?php

namespace App\Http\Controllers;

use App\DataTables\NotificationDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateNotificationRequest;
use App\Http\Requests\UpdateNotificationRequest;
use App\Models\Event;
use App\Models\Notification;
use App\Models\Variable;
use App\Repositories\NotificationRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use Illuminate\Http\Request;
class NotificationController extends AppBaseController
{
    /** @var  NotificationRepository */
    private $notificationRepository;

    public function __construct(NotificationRepository $notificationRepo)
    {
        $this->notificationRepository = $notificationRepo;
    }

    /**
     * Display a listing of the Notification.
     *
     * @param NotificationDataTable $notificationDataTable
     * @return Response
     */
    public function index(NotificationDataTable $notificationDataTable)
    {
        return $notificationDataTable->render('notifications.index');
    }

    /**
     * Show the form for creating a new Notification.
     *
     * @return Response
     */
    public function create()
    {
        $events = Event::select('id', 'name','team1_id','team2_id', 'date', 'type')->get();

        $types= ['push'=>'Пуш-уведомление', 'email'=>'Email-сообщение'];

        return view('notifications.create')->with(['events'=>$events,'types'=>$types]);
    }

    /**
     * Store a newly created Notification in storage.
     *
     * @param CreateNotificationRequest $request
     *
     * @return Response
     */
    public function store(CreateNotificationRequest $request)
    {
        $input = $request->all();

        $datetime =$request->push_date.' '.$request->push_time;
        $datetime = date('Y-m-d H:i:s', strtotime($datetime));
        $input['push_datetime'] = $datetime;

        $notification = $this->notificationRepository->create($input);

        //unisender
        /*
        $uni = $this->connectUnisender();
        $email_id=$this->createEmailMessage($notification->id);
        $this->createCampaign($email_id, $notification->push_datetime);
        */

        Flash::success('Notification saved successfully.');
        return redirect(route('notifications.index'));
    }

    /**
     * Display the specified Notification.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $notification = $this->notificationRepository->findWithoutFail($id);

        if (empty($notification)) {
            Flash::error('Notification not found');

            return redirect(route('notifications.index'));
        }

        return view('notifications.show')->with('notification', $notification);
    }

    /**
     * Show the form for editing the specified Notification.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $notification = $this->notificationRepository->findWithoutFail($id);

        $events = Event::select('id', 'name','team1_id','team2_id', 'date', 'type')->get();
        $types = ['push'=>'Пуш-уведомление', 'email'=>'Email-сообщение'];
        $date = date('Y-m-d',strtotime($notification->push_datetime));
        $time = date('H:i',strtotime($notification->push_datetime));



        if (empty($notification)) {
            Flash::error('Notification not found');
            return redirect(route('notifications.index'));
        }

        return view('notifications.edit')->with([
            'notification'=>$notification,
            'events'=>$events,
            'types'=>$types,
            'date'=>$date,
            'time'=>$time
        ]);
    }

    /**
     * Update the specified Notification in storage.
     *
     * @param  int              $id
     * @param UpdateNotificationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNotificationRequest $request)
    {
        $notification = $this->notificationRepository->findWithoutFail($id);

        if (empty($notification)) {
            Flash::error('Notification not found');
            return redirect(route('notifications.index'));
        }

        $input = $request->all();
        $datetime =$request->push_date.' '.$request->push_time;
        $datetime = date('Y-m-d H:i:s', strtotime($datetime));
        $input['push_datetime'] = $datetime;


        $notification = $this->notificationRepository->update($input, $id);

        //unisender
        /*
        $uni = $this->connectUnisender();
        $email_id=$this->createEmailMessage($id);
        $this->createCampaign($email_id, $notification->push_datetime);
        */


        Flash::success('Notification updated successfully.');
        return redirect(route('notifications.index'));
    }

    /**
     * Remove the specified Notification from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $notification = $this->notificationRepository->findWithoutFail($id);

        if (empty($notification)) {
            Flash::error('Notification not found');

            return redirect(route('notifications.index'));
        }

        $this->notificationRepository->delete($id);

        Flash::success('Notification deleted successfully.');

        return redirect(route('notifications.index'));
    }


    public function createEmailMessage($id)
    {
        $uni = $this->connectUnisender();
        $email_from_name = Variable::where('key','sender_name')->value('value');
        $email_from_name = iconv('cp1251', 'utf-8', $email_from_name);
        $email_from_email = Variable::where('key','sender_email')->value('value');
        $email_subject = Notification::where('id',$id)->value('name');
        $email_subject = iconv('cp1251', 'utf-8', $email_subject);
        $email_to = $this->getList($uni); // код списка, по которому делать рассылку
        $email_text = Notification::where('id',$id)->value('text');
        $email_text = iconv('cp1251', 'utf-8', $email_text);
        $email_wrap = 'center';
        $email_categories = 'letter list, unisender';//?

        // Прикрепим простой текстовый файл:
        //$email_attach_file_name = iconv('cp1251', 'utf-8', "текстовый файл.txt");
        //$email_attach_file_content = iconv('cp1251', 'utf-8', "Содержимое файла");

        // Создаём POST-запрос
        $post = array (
            'api_key' =>  env('UNISENDER_KEY', ''),
            'sender_name' => $email_from_name,
            'sender_email' => $email_from_email,
            'subject' => $email_subject,
            'list_id' => $email_to,
            'wrap_type' => $email_wrap,
            'categories' => $email_categories,
            'body' => $email_text,
            //'attachments[' . $email_attach_file_name . ']' => $email_attach_file_content
        );

        $jsonObj= $this->makeCurl($post, 'createEmailMessage');
        $result = $jsonObj->result->message_id;
        return $result;

        // Новое сообщение успешно создано
        //echo "Success. Message ID is " . $jsonObj->result->message_id;
    }


    public function createCampaign($email_id, $email_starttime)
    {
        // Параметры для отправки сообщения
        //$email_id = "361988";

        // Запланируем отправку на определённое время  (закомментируйте, чтобы отправить немедленно)
        $email_starttime = urlencode($email_starttime);

        // Параметры сбора статистики    // Если 1, собираем, если 0 - нет
        $email_stats_links = "0";
        $email_stats_read = "0";

        // Создаём POST-запрос
        $post = array(
            'api_key' => env('UNISENDER_KEY', ''),
            'message_id' => $email_id,
            'start_time' => $email_starttime,
            'track_read' => $email_stats_read,
            'track_links' => $email_stats_links
        );

        $this->makeCurl($post, 'createCampaign');
        // Рассылка успешно отправлена (или запланирована к отправке)
        //echo "Success. Newsletter delivery status is " . $jsonObj->result->status;
    }

    public function push($id)
    {
        $notification=Notification::where('id',$id)
            ->first();

        $request = new Request();
        $request->title = $notification->name;
        $request->body = $notification->text;
        $request->data =  ['id_event' => $notification->event_id];
        $result = $this->sendPush($request);//!!!

        return $result; //?
    }
}
