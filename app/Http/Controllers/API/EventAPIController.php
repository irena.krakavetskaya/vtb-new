<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEventAPIRequest;
use App\Http\Requests\API\UpdateEventAPIRequest;
use App\Models\Event;
use App\Models\Object;
use App\Repositories\EventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EventController
 * @package App\Http\Controllers\API
 */

class EventAPIController extends AppBaseController
{
    /** @var  EventRepository */
    private $eventRepository;

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepository = $eventRepo;
    }

    /**
     * Display a listing of the Event.
     * GET|HEAD /events
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->eventRepository->pushCriteria(new RequestCriteria($request));
        //$this->eventRepository->pushCriteria(new LimitOffsetCriteria($request));
        //$event = $this->eventRepository->all();

        $event = new Event();
        $total = $event->count();

        $startIndex = $request->startIndex?($request->startIndex)-1:0;
        $endIndex = $request->endIndex?$request->endIndex:0;
        $limit= $endIndex-$startIndex<=0?$total:$endIndex-$startIndex;

        $date_now= date('Y-m-d');
        $time_now =date('H:i');
        $month_now =date('n');

        //without months
        $evs = Event::select('id','type','date', 'time', 'kind','place_id','team1_id','team2_id','name','img')//,'text'  $val->ru
            ->whereDate('date','>=', $date_now)
            //->whereTime('time','>=', $time_now)
            ->orderBy('date', 'asc')
            ->offset($startIndex)
            ->limit($limit)
            ->get();
        $event->formatEvent($evs);
        //end new

        $data=[
            'events'=>$evs
        ];

        return $evs;

    }

    /**
     * Store a newly created Event in storage.
     * POST /events
     *
     * @param CreateEventAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEventAPIRequest $request)
    {
        $input = $request->all();

        $events = $this->eventRepository->create($input);

        return $this->sendResponse($events->toArray(), 'Event saved successfully');
    }

    public function showAd()//$id
    {
        $event = new Event();
        $ad=$event->getAd();

        return $ad;
    }
    /**
     * Display the specified Event.
     * GET|HEAD /events/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function showPlace(Request $request)//,$place,$year=null,$month=null
    {
        /*
         * place:
         * 1 - лед дворец
         * 3 - музей
         * all
         */

        $place = $request->place;
        $year = $request->year;
        $month = $request->month;
        $day = $request->day;


        $type = $request->category;//all, sport, entertainment, excursion
        if(!isset($type) || $type=='all'){
            $type='all';
        }
        else{
            if($type=='sport'){
                $type='match';
            }
            elseif($type=='entertainment'){
                $type='show';
            }
            else{
                $type='excursion';
            }
        }

        $date_now = date('Y-m-d');
        $time_now = date('H:i');
        $month_now = date('n');
        $year_now = date('Y');

        $event = new Event();
        $total = $event->count();

        $startIndex = $request->startIndex?($request->startIndex)-1:0;
        $endIndex = $request->endIndex?$request->endIndex:1000;
        $limit= $endIndex-$startIndex<0?$total:$endIndex-$startIndex;


        $month_names=$event->getMonths($month_now);
        $place=$event->getPlace($place);

        $year=isset($year)?$year:$year_now;

        $evs = Event::get();//whereIn('place_id', $place)->
        if(isset($month)){
            $evs1 = Event::whereYear('date', $year)
                //->whereIn('place_id', $place)
                ->whereMonth('date','=', $month)//$month1
                ->get();
        }
        else{
            $evs1 = Event::whereYear('date', $year)
                //->whereIn('place_id', $place)
                ->get();
        }

        if($evs->toArray()==[]){
            return [];
        }
        if($evs1->toArray()==[]){
            return [];
        }

        $query = Event::select('id','type','date', 'time', 'kind','place_id','team1_id','team2_id','name','img')//,'text'
           // ->whereIn('place_id', $place)//
            ->whereYear('date','=', $year)
            ->whereDate('date','>=', $date_now)
            ->orderBy('date', 'asc');


        if($month!=null){
            if($day!=null){
                $evs =$query
                    ->whereMonth('date','=', $month)
                    ->whereDay('date', $day)
                    ->get();

            }
            else{
                $evs =$query
                    ->whereMonth('date','=', $month)
                    ->get();
            }
        }
        else{
            $evs=$query->get();
        }

        $collection=collect($evs);
        if($type!='all')  {
            $evs = $collection->where('type', $type)
                ->slice($startIndex)
                ->take($limit)
                ->values();
        }
        else{
            $evs = $collection
                ->slice($startIndex)
                ->take($limit)
                ->values();
        }

        $event->formatEvent($evs);

        return $evs;
    }

    public function show($id){

        $event = $this->eventRepository->findWithoutFail($id);
        if (empty($event)) {
           return $this->sendError('Event not found');
        }

        $ev = new Event();
        $evs = Event::where('id',$id)
            ->select('id','type','date', 'time', 'kind','place_id','team1_id','team2_id','name','img','text')->first();

        $res=$ev->formatEvent($evs);

        $res['phoneVip']=Object::where('id',4)->value('phone');
        $res['phoneBoxOffice']=Object::where('id',6)->value('phone');
        $res['openHours']=Object::where('id',6)->value('open_hours');

        return  $res;
        //return $this->sendResponse($event->toArray(), 'Event retrieved successfully');
    }



    /**
     * Update the specified Event in storage.
     * PUT/PATCH /events/{id}
     *
     * @param  int $id
     * @param UpdateEventAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEventAPIRequest $request)
    {
        $input = $request->all();

        /** @var Event $event */
        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event = $this->eventRepository->update($input, $id);

        return $this->sendResponse($event->toArray(), 'Event updated successfully');
    }

    /**
     * Remove the specified Event from storage.
     * DELETE /events/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Event $event */
        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            return $this->sendError('Event not found');
        }

        $event->delete();

        return $this->sendResponse($id, 'Event deleted successfully');
    }
}
