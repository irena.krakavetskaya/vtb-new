<?php

namespace App\Http\Controllers;

use App\DataTables\SkyboxDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateSkyboxRequest;
use App\Http\Requests\UpdateSkyboxRequest;
use App\Models\Skybox;
use App\Repositories\SkyboxRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class SkyboxController extends AppBaseController
{
    /** @var  SkyboxRepository */
    private $skyboxRepository;

    public function __construct(SkyboxRepository $skyboxRepo)
    {
        $this->skyboxRepository = $skyboxRepo;
    }

    /**
     * Display a listing of the Skybox.
     *
     * @param SkyboxDataTable $skyboxDataTable
     * @return Response
     */
    public function index(SkyboxDataTable $skyboxDataTable)
    {
        return $skyboxDataTable->render('skyboxes.index');
    }

    /**
     * Show the form for creating a new Skybox.
     *
     * @return Response
     */
    public function create()
    {
        return view('skyboxes.create');
    }

    /**
     * Store a newly created Skybox in storage.
     *
     * @param CreateSkyboxRequest $request
     *
     * @return Response
     */
    public function store(CreateSkyboxRequest $request)
    {

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');

        if(isset($file)) {
            $skybox = new Skybox();
            $img = $skybox->formatImg($file, 'img_skybox', 240, 240);
            //$img = $file->store('img_skybox');
        }
        else{
            $img =$file1?$file1:'';
        }
        $input=$request->all();
        $input['img'] = $img;
        //end of add preview


        $skybox = $this->skyboxRepository->create($input);

        Flash::success('Привилегия успешно сохранена');

        //return redirect(route('objects.edit',4));
        return redirect(route('pages.edit',16));
    }

    /**
     * Display the specified Skybox.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $skybox = $this->skyboxRepository->findWithoutFail($id);

        if (empty($skybox)) {
            Flash::error('Skybox not found');

            return redirect(route('skyboxes.index'));
        }

        return view('skyboxes.show')->with('skybox', $skybox);
    }

    /**
     * Show the form for editing the specified Skybox.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $skybox = $this->skyboxRepository->findWithoutFail($id);

        if (empty($skybox)) {
            Flash::error('Привилегия не найдена');

            return redirect(route('skyboxes.index'));
        }

        return view('skyboxes.edit')->with('skybox', $skybox);
    }

    /**
     * Update the specified Skybox in storage.
     *
     * @param  int              $id
     * @param UpdateSkyboxRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSkyboxRequest $request)
    {
        $skybox = $this->skyboxRepository->findWithoutFail($id);

        if (empty($skybox)) {
            Flash::error('Привилегия не найдена');

            return redirect(route('skyboxes.index'));
        }

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');
        if(isset($file)) {
            //$img = $file->store('img_skybox');
            $img = $skybox->formatImg($file, 'img_skybox', 240, 240);
        }
        else{
            $img =$file1?$file1:'';
        }
        //end of add preview
        $requestData=$request->all();
        $requestData['img'] = $img;


        $skybox = $this->skyboxRepository->update($requestData, $id);

        Flash::success('Привилегия успешно изменена');

        //return redirect(route('objects.edit',4));
        return redirect(route('pages.edit',16));
    }

    /**
     * Remove the specified Skybox from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $skybox = $this->skyboxRepository->findWithoutFail($id);

        if (empty($skybox)) {
            Flash::error('Skybox not found');

            return redirect(route('skyboxes.index'));
        }

        $this->skyboxRepository->delete($id);

        return $id;

        /*
        $this->skyboxRepository->delete($id);
        Flash::success('Skybox deleted successfully.');
        return redirect(route('skyboxes.index'));
        */
    }
}
