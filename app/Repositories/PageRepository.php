<?php

namespace App\Repositories;

use App\Models\Album;
use App\Models\Img;
use App\Models\Page;
use App\Models\Photo;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PageRepository
 * @package App\Repositories
 * @version July 9, 2018, 12:33 pm UTC
 *
 * @method Page findWithoutFail($id, $columns = ['*'])
 * @method Page find($id, $columns = ['*'])
 * @method Page first($columns = ['*'])
*/
class PageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pagetitle',
        'longtitle',
        'introtext',
        'description',
        'content',
        'menuindex',
        'menutitle',
        'alias',
        'parent',
        'isfolder',
        'template_id',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'pagetitle_en',
        'longtitle_en',
        'introtext_en',
        'description_en',
        'content_en',
        'menutitle_en'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Page::class;
    }

    public function getAlbums($id)
    {
        if($id==32){
            $type='img';
        }
        if($id==33){
            $type='video';
        }
        $albums=isset($type)?Album::where('type',$type)->get():null;
        return $albums;
    }

    public function getVideoAlbums()
    {
        $albums_video=Album::where('type','video')->get();
        $video =[];
        $video_img=[];
        foreach($albums_video as $album){
            $link = Photo::where('album_id',$album->id)->value('link');
            $video[$album->id]= $link;
            $substr = str_replace('http://www.youtube.com/embed/', '', $link);
            $substr = str_replace('?autoplay=1', '', $substr);
            $video_img[$album->id]= $substr;
        }
        return $data=[$video,$video_img];
    }

    public function getChildrenImg($children)
    {
        $children_img =[];
        foreach($children as $child){
            $children_img[$child->id]= Img::where('page_id',$child->id)->pluck('link')->toArray();
        }
        return $children_img;
    }
}
