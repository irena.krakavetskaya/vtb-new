<div class="col-md-12">


    <fieldset class="gallery">
        <legend>Галерея</legend>
        <div class="form-group col-md-12">

            @foreach($imgs as $img)
                <div class="col-sm-2">
                    <p>
                        <input name="img_skybox[]" type="hidden" value="{!!  $img->link  !!}">
                        <a href="/{!! $img->link !!}" data-toggle="lightbox">
                            <img src="/{!! $img->link !!}" class="img-responsive">
                        </a>
                    </p>
                    <p class="text-center">
                        <input name="del_img[]" type="hidden" value="{!! $img->id !!}" class="del_img">
                        <a href="#" class='btn btn-danger btn-sm delete-img' id="{!! $img->id !!}">
                            Удалить
                        </a>
                    </p>
                </div>
            @endforeach

            <div class="col-sm-2">

                <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
                <img id="uploadedimage_museum" height="160px" class="uploadedimage_museum">
                <span id="imageerror_museum" style="font-weight: bold; color: red"></span>
                {!! Form::file('img_museum[]', ['id'=>'img_museum', 'accept'=>'image/jpg,image/jpeg,image/png', 'class'=>'img_museum']) !!}

            </div>
        </div>
    </fieldset>




    <fieldset class="skybox-adv">
        <legend>Привилегии VIP-лож</legend>

        @foreach($skyboxes as $skybox)
            <div>
                <div class="col-md-2">
                    <input name="id_skybox[]" type="hidden" value="{!!$skybox->id  !!}" class="id_skybox">
                    <img src="/{!! $skybox->img !!}" class="img-responsive">
                </div>
                <div class="col-md-6 text">
                    {!! $skybox->name !!}
                </div>

                <div class='btn-group col-md-2'>
                    <a href="{{ route('skyboxes.edit', $skybox->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs delete_skyboxe',
                        'id'=>$skybox->id
                    ]) !!}
                </div>

                <div class="clearfix"></div>
            </div>
        @endforeach


        <div class="form-group col-sm-6">
            <a href="{!! route('skyboxes.create') !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
                Добавить привилегию
            </a>
        </div>
    </fieldset>


    <fieldset>
            <legend>Контакты</legend>
            <div class="form-group col-sm-6">
                {!! Form::label('phone', 'Номер телефона:') !!}
                {!! Form::text('phone', $object->phone,  ['class' => 'form-control']) !!}
            </div>

    </fieldset>


    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('objects.edit',2) !!}" class="btn btn-default">Отмена</a>
    </div>


</div>



