<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название услуги:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Img Field -->
<!--<div class="form-group col-sm-6">
    !! Form::label('img', 'Превью:') !!}<br>
    <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">

    if(!empty($service->img))
        <input name="img3" type="hidden" value="!! $service->img  !!}">
        <img src="!! url("/") !!}/!! $service->img !!}" class="img-responsive" id="uploadedimage1">
    endif

    <img id="uploadedimage1" width="200px" />
    <span id="imageerror1" style="font-weight: bold; color: red"></span>

    !! Form::file('img', ['id'=>'img_nav', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
</div>-->
<div class="clearfix"></div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Телефон:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

@if($service->id==3 || $service->id==4 || $service->id==1)
    <!-- Email Field -->
    <div class="form-group col-sm-6">
        {!! Form::label('email', 'E-mail:') !!}
        {!! Form::email('email', null, ['class' => 'form-control']) !!}
    </div>
    <div class="clearfix"></div>

    <div class="form-group col-sm-12">
        {!! Form::label('additional', 'Доп. текст:') !!}
        {!! Form::text('additional', null, ['class' => 'form-control']) !!}
    </div>
@endif

@if($service->id==1 || $service->id==4)
    <div class="form-group col-sm-12 col-lg-12">
        {!! Form::label('text', 'Описание услуги:') !!}
        {!! Form::textarea('text', null, ['class' => 'form-control','id'=>'ckeditor1','maxlength'=>500]) !!}
    </div>
@endif






<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('services.index') !!}" class="btn btn-default">Отмена</a>
</div>

<!--
<div class="form-group col-sm-6">
    !! Form::label('name_en', 'Name En:') !!}
!! Form::text('name_en', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-12 col-lg-12">
    !! Form::label('text_en', 'Text En:') !!}
!! Form::textarea('text_en', null, ['class' => 'form-control']) !!}
        </div>
-->