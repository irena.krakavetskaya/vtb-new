* Ледовый дворец

===================================

Запрос:

    GET /api/v1/objects/1
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 {
                 "id": 1,
                 "text": "Мобильное приложение Парк легенд предназначено для посетителей комплекса Парк легенд, состоящего из Ледовой арены, Музея хоккея, Открытого катка. Использование приложения должно облегчить навигацию посетителей внутри комплекса, совершать покупки билетов и заказ дополнительных услуг как в самом комплексе, так и удаленно, без необхо</em>димости посещения касс на территории комплекса.",
                 "fb": "https://www.facebook.com/ParkIegend?ref=hl",
                 "inst": "https://www.facebook.com/ParkIegend?ref=hl",
                 "vk": "https://www.facebook.com/ParkIegend?ref=hl",
                 "youtube": "https://www.facebook.com/ParkIegend?ref=hl",
                 "useful": {
                     "title": "Полезное",
                     "pdf": [                        
                         {
                             "name": "Правила посещения ВТБ Ледового дворца",
                             "link": "pdf/Правила посещения ВТБ Ледового дворца.pdf"
                         }
                     ]
                 },
                 "events": [
                     {
                         "id": 3,
                         "type": "match",
                         "time": "19:00",
                         "dateEvent": "2018-01-08",                        
                         "place": "Большая арена",
                         "league": "КХЛ",
                         "team2": {
                             "name": "ЦСКА",
                             "logo": "logo/Q9R5gkBaoe4x5WHceP2RvAJJYD3YRsWZYsTfpB4y.jpeg"
                         },
                         "team1": {
                             "id": 1,
                             "name": "Спартак",
                             "logo": "logo/muPgF8fGOjanlNeBKQS0kWde3rAwCmXMz8YjTZf3.jpeg"
                         }
                     },
                     {
                         "id": 1,
                         "type": "show",
                         "time": "17:00",
                         "kind": "Шоу",
                         "name": "БОЛЬШОЙ ТЕСТ-ДРАЙВ SUBARU XV 1",
                         "img": "img_show/83iagllAiuNHahrXW1ZL5fotry58Fga15puIJxTm.jpeg",
                         "dateEvent": "2018-01-09",                         
                         "place": "Малая арена"
                     },
                     {
                         "id": 10,
                         "type": "match",
                         "time": "18:00",
                         "dateEvent": "2018-01-26",                         
                         "place": "Большая арена",
                         "league": null,
                         "team2": {
                             "name": "Слован",
                             "logo": "logo/LNCgp1k3uFZ2uwyW0eqyw27anlXVc404U7upRCIc.jpeg"
                         },
                         "team1": {
                             "id": 1,
                             "name": "Спартак",
                             "logo": "logo/muPgF8fGOjanlNeBKQS0kWde3rAwCmXMz8YjTZf3.jpeg"
                         }
                     }
                 ],
                 "services": [
                          {
                              "id": 1,
                              "name": "Аренда льда"
                          },                          
                          {
                              "id": 3,
                              "name": "Бюро находок"
                          }
                 ]
             }