<div class="form-group col-sm-12 no-padding">
    <!-- Type Field -->
    <div class="form-group col-md-4">
        {!! Form::label('type', 'Тип события:') !!}
        @isset($event->type)
            {!! Form::select('type', array('match' =>'Матч','show'=>'Мероприятие'),$event->type, ['class' => 'form-control', 'disabled']) !!}
        @else
            {!! Form::select('type', array('match' =>'Матч','show'=>'Мероприятие'),null, ['class' => 'form-control']) !!}
        @endisset
    </div>

    <!-- Place Id Field -->
    <div class="form-group col-md-4">
        {!! Form::label('place_id', 'Место проведения:') !!}
        <select id="place_id" name="place_id" class="form-control">
            @if(isset($event->place_id))
                @foreach ($places as $key=>$value)
                    @if($value->id == $event->place_id)
                        <option value="{!! $value->id !!}" selected>{!! $value->name !!}</option>
                    @else
                        <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                    @endif
                @endforeach
            @else
                @foreach ($places as $key=>$value)
                    <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="clearfix"></div>


    <div class="form-group col-md-4 league">
        {!! Form::label('kind', 'Лига:') !!}
        {!! Form::text('kind', null, ['class' => 'form-control','maxlength'=>50]) !!}
    </div>
    <div class="form-group col-md-4 league">
        {!! Form::label('kind_en', 'Лига (En):') !!}
        {!! Form::text('kind_en', null, ['class' => 'form-control','maxlength'=>50]) !!}
    </div>
    <div class="clearfix"></div>

</div>

<div class="form-group col-md-12 no-padding">
    <div class="form-group col-md-4">
    {!! Form::label('date', 'Дата:') !!}
    @isset($event->date)
        {!! Form::date('date', date('Y-m-d', strtotime($event->date)), ['class' => 'form-control']) !!}
    @else
        {!! Form::date('date', date('Y-m-d'), ['class' => 'form-control']) !!}
    @endisset <!--'id'=> 'datepicker', 'data-date-format'=>'dd/mm/yyyy','data-provide'=>'datepicker'-->
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('time', 'Время:') !!}
        @isset($event->time)
            {!! Form::time('time', null, ['class' => 'form-control']) !!}
        @else
            {!! Form::time('time', '00:00', ['class' => 'form-control']) !!}
        @endisset
    </div>

    @if($event->place_id==1 || $event->place_id==2 || $event->place_id==3)
        <div class="form-group col-md-4 is_important">
                @isset($event->is_important)
                    @if($event->is_important=='true')
                        {!! Form::checkbox('is_important', $event->is_important, true,['class' => 'field', 'checked'=>true]) !!}
                    @else
                        {!! Form::checkbox('is_important', $event->is_important, false, ['class' => 'field', 'checked'=>false]) !!}
                    @endif
                @else
                    {!! Form::checkbox('is_important', null, null, ['class' => 'field', 'checked'=>false]) !!}
                @endisset
                {!! Form::label('is_important', 'Показывать на главной') !!}
        </div>
    @endif

</div>

<div class="form-group col-md-12 no-padding teams">
    <div class="form-group col-md-4">
        {!! Form::label('team1_id', 'Команда 1:') !!}
        <select id="team1_id" name="team1_id" class="form-control">
            @if(isset($event->team1_id))
                @foreach ($teams as $key=>$value)
                    @if($value->id == $event->team1_id)
                        <option value="{!! $value->id !!}" selected>{!! $value->name !!}</option>
                    @else
                        <option value="{!! $value->id !!}" @if(old('team1_id') == $value->id) {{ 'selected' }} @endif>{!! $value->name !!}</option>
                    @endif
                @endforeach
            @else
                <option value="">Не выбрано</option>
                @foreach ($teams as $key=>$value)
                    <option value="{!! $value->id !!}"  @if(old('team1_id') == $value->id) {{ 'selected' }} @endif>
                        {!! $value->name !!}
                    </option>
                @endforeach
            @endif
        </select>

        @isset($event)
            <img src="/{!! $event->team1->logo !!}">
        @else
            <img src="">
        @endisset

    </div>

    <div class="form-group col-md-4">
        {!! Form::label('team1_id', 'Команда 2:') !!}
        <select id="team2_id" name="team2_id" class="form-control">
            @if(isset($event->team2_id))
                @foreach ($teams as $key=>$value)
                    @if($value->id == $event->team2_id)
                        <option value="{!! $value->id !!}" selected>{!! $value->name !!}</option>
                    @else
                        <option value="{!! $value->id !!}" @if(old('team2_id') == $value->id) {{ 'selected' }} @endif>{!! $value->name !!}</option>
                    @endif
                @endforeach
            @else
                <option value="">Не выбрано</option>
                @foreach ($teams as $key=>$value)
                    <option value="{!! $value->id !!}" @if(old('team2_id') == $value->id) {{ 'selected' }} @endif>{!! $value->name !!}</option>
                @endforeach
            @endif
        </select>

        @isset($event)
            <img src="/{!! $event->team2->logo !!}">
        @else
            <img src="">
        @endisset

    </div>
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('events.index') !!}" class="btn btn-default">Отмена</a>
</div>
