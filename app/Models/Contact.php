<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Contact",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="page_id",
 *          description="page_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="value",
 *          description="value",
 *          type="string"
 *      )
 * )
 */
class Contact extends Model
{
    //use SoftDeletes;

    public $table = 'contacts';
    
    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'page_id',
        'value',
        'value_en',
        'description',
        'description_en',
        'contact_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'page_id' => 'integer',
        'value' => 'string',
        'value_en'=> 'string',
        'description'=>'string',
        'description_en'=>'string',
        'contact_type_id'=> 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    public $timestamps = false;

    public function contactType()
    {
        return $this->belongsTo('App\Models\ContactType', 'contact_type_id');
    }

    public function saveContacts($request, $page_id){
        $phone = $request->phone;
        $email = $request->email;
        $open_hours = $request->open_hours;
        $address = $request->address;
        $open_hours_en = $request->open_hours_en;
        $address_en = $request->address_en;
        if(isset($phone)){
            Contact::where(['page_id'=>$page_id,'contact_type_id' => 1])
                ->update(['value' => $phone]);
        }
        if(isset($email)){
            Contact::where(['page_id'=>$page_id,'contact_type_id' => 2])
                ->update(['value' => $email]);
        }
        if(isset($open_hours)){
            Contact::where(['page_id'=>$page_id,'contact_type_id' => 4])
                ->update(['value' => $open_hours]);
        }
        if(isset($open_hours_en)){
            Contact::where(['page_id'=>$page_id,'contact_type_id' => 4])
                ->update(['value_en' => $open_hours_en]);
        }
        if(isset($address)){
            Contact::where(['page_id'=>$page_id,'contact_type_id' =>3])
                ->update(['value' => $address]);
        }
        if(isset($address_en)){
            Contact::where(['page_id'=>$page_id,'contact_type_id' =>3])
                ->update(['value_en' => $address_en]);
        }
    }
}
