<?php

namespace App\Http\Controllers;

use App\DataTables\PointDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePointRequest;
use App\Http\Requests\UpdatePointRequest;
use App\Models\Point;
use App\Repositories\PointRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;
use Image;


class PointController extends AppBaseController
{
    /** @var  PointRepository */
    private $pointRepository;

    public function __construct(PointRepository $pointRepo)
    {
        $this->pointRepository = $pointRepo;
    }

    /**
     * Display a listing of the Point.
     *
     * @param PointDataTable $pointDataTable
     * @return Response
     */
    public function index(PointDataTable $pointDataTable)
    {
        return $pointDataTable->render('points.index');
    }

    /**
     * Show the form for creating a new Point.
     *
     * @return Response
     */
    public function create()
    {
        return view('points.create');
    }

    /**
     * Store a newly created Point in storage.
     *
     * @param CreatePointRequest $request
     *
     * @return Response
     */
    public function store(CreatePointRequest $request)
    {
        $input = $request->all();

        $point = $this->pointRepository->create($input);

        Flash::success('Point saved successfully.');

        return redirect(route('points.index'));
    }

    /**
     * Display the specified Point.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $point = $this->pointRepository->findWithoutFail($id);

        if (empty($point)) {
            Flash::error('Point not found');

            return redirect(route('points.index'));
        }

        return view('points.show')->with('point', $point);
    }

    /**
     * Show the form for editing the specified Point.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $point = $this->pointRepository->findWithoutFail($id);

        if (empty($point)) {
            Flash::error('Point not found');

            return redirect(route('points.index'));
        }

        return view('points.edit')->with('point', $point);
    }

    /**
     * Update the specified Point in storage.
     *
     * @param  int              $id
     * @param UpdatePointRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePointRequest $request)
    {
        $point = $this->pointRepository->findWithoutFail($id);


        if (empty($point)) {
            Flash::error('Point not found');

            return redirect(route('points.index'));
        }

        $name=$point->name;
        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');
        $img_thumb1 = $request->input('img_thumb');
        $place=$point->place->id;
        $floor=$point->floor->id;
        if(isset($file)) {
            $img = $file->store('img_navigation/'.$place.'/'.$floor);

            //Image::make($file->getRealPath())->resize(200, 200)->save($img);
            $image = Image::make($file->getRealPath());
            $image->fit(300, 200)->save(public_path('img_navigation/'.$place.'/'.$floor .'/'.$name.'-thumbs.jpg'));
            $img_thumb='img_navigation/'.$place.'/'.$floor .'/'.$name.'-thumbs.jpg';
        }
        else{
            $img =$file1?$file1:'';
            $img_thumb= $img_thumb1? $img_thumb1:'';
        }

        //end of add preview


        $requestData=$request->all();
        $requestData['img'] = $img;
        $requestData['img_thumb'] = $img_thumb;

        $point = $this->pointRepository->update($requestData, $id);

        Flash::success('Информация о точке успешно сохранена.');
        //return redirect(route('places.edit',$point->place_id,$floor));
        return redirect(route('places.edit',[$point->place_id, 'floor'=>$floor]));

        //Flash::success('Point updated successfully.');
        //return redirect(route('points.index'));
    }

    /**
     * Remove the specified Point from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $point = $this->pointRepository->findWithoutFail($id);

        if (empty($point)) {
            return 'Point not found';
        }

        Point::where('id',$id)->update([
            'is_active'=>'false',
            'text'=>'',
            'img'=>''
        ]);

        return $id;
    }


    public function changeStatus(Request $request)
    {
        $is_active = $request->is_active;
        $id = $request->id;

        $point = $this->pointRepository->findWithoutFail($id);

        if (empty($point)) {
            return 'Point not found';
        }

        $point = $this->pointRepository->update($request->all(), $id);
        return 'true';
    }
}
