<?php

use Faker\Factory as Faker;
use App\Models\Img;
use App\Repositories\ImgRepository;

trait MakeImgTrait
{
    /**
     * Create fake instance of Img and save it in database
     *
     * @param array $imgFields
     * @return Img
     */
    public function makeImg($imgFields = [])
    {
        /** @var ImgRepository $imgRepo */
        $imgRepo = App::make(ImgRepository::class);
        $theme = $this->fakeImgData($imgFields);
        return $imgRepo->create($theme);
    }

    /**
     * Get fake instance of Img
     *
     * @param array $imgFields
     * @return Img
     */
    public function fakeImg($imgFields = [])
    {
        return new Img($this->fakeImgData($imgFields));
    }

    /**
     * Get fake data of Img
     *
     * @param array $postFields
     * @return array
     */
    public function fakeImgData($imgFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'link' => $fake->word,
            'object_id' => $fake->randomDigitNotNull
        ], $imgFields);
    }
}
