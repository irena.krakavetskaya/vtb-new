<?php

use App\Models\Variable;
use App\Repositories\VariableRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class VariableRepositoryTest extends TestCase
{
    use MakeVariableTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var VariableRepository
     */
    protected $variableRepo;

    public function setUp()
    {
        parent::setUp();
        $this->variableRepo = App::make(VariableRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateVariable()
    {
        $variable = $this->fakeVariableData();
        $createdVariable = $this->variableRepo->create($variable);
        $createdVariable = $createdVariable->toArray();
        $this->assertArrayHasKey('id', $createdVariable);
        $this->assertNotNull($createdVariable['id'], 'Created Variable must have id specified');
        $this->assertNotNull(Variable::find($createdVariable['id']), 'Variable with given id must be in DB');
        $this->assertModelData($variable, $createdVariable);
    }

    /**
     * @test read
     */
    public function testReadVariable()
    {
        $variable = $this->makeVariable();
        $dbVariable = $this->variableRepo->find($variable->id);
        $dbVariable = $dbVariable->toArray();
        $this->assertModelData($variable->toArray(), $dbVariable);
    }

    /**
     * @test update
     */
    public function testUpdateVariable()
    {
        $variable = $this->makeVariable();
        $fakeVariable = $this->fakeVariableData();
        $updatedVariable = $this->variableRepo->update($fakeVariable, $variable->id);
        $this->assertModelData($fakeVariable, $updatedVariable->toArray());
        $dbVariable = $this->variableRepo->find($variable->id);
        $this->assertModelData($fakeVariable, $dbVariable->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteVariable()
    {
        $variable = $this->makeVariable();
        $resp = $this->variableRepo->delete($variable->id);
        $this->assertTrue($resp);
        $this->assertNull(Variable::find($variable->id), 'Variable should not exist in DB');
    }
}
