<?php

namespace App\Http\Controllers;

use App\DataTables\TransportDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTransportRequest;
use App\Http\Requests\UpdateTransportRequest;
use App\Repositories\TransportRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TransportController extends AppBaseController
{
    /** @var  TransportRepository */
    private $transportRepository;

    public function __construct(TransportRepository $transportRepo)
    {
        $this->transportRepository = $transportRepo;
    }

    /**
     * Display a listing of the Transport.
     *
     * @param TransportDataTable $transportDataTable
     * @return Response
     */
    public function index(TransportDataTable $transportDataTable)
    {
        return $transportDataTable->render('transports.index');
    }

    /**
     * Show the form for creating a new Transport.
     *
     * @return Response
     */
    public function create()
    {
        return view('transports.create');
    }

    /**
     * Store a newly created Transport in storage.
     *
     * @param CreateTransportRequest $request
     *
     * @return Response
     */
    public function store(CreateTransportRequest $request)
    {
        $input = $request->all();

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');
        if(isset($file)) {
            $img = $file->store('img_contacts/');
        }
        else{
            $img =$file1?$file1:'';
        }
        $input['img'] = $img;
        //end of add preview


        $transport = $this->transportRepository->create($input);

        Flash::success('Transport saved successfully.');

        //return redirect(route('transports.index'));
        return redirect(route('pages.edit',21));
    }

    /**
     * Display the specified Transport.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transport = $this->transportRepository->findWithoutFail($id);

        if (empty($transport)) {
            Flash::error('Transport not found');

            return redirect(route('transports.index'));
        }

        return view('transports.show')->with('transport', $transport);
    }

    /**
     * Show the form for editing the specified Transport.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $transport = $this->transportRepository->findWithoutFail($id);

        if (empty($transport)) {
            Flash::error('Схема проезда не найдена');
            return redirect(route('transports.index'));
        }

        return view('transports.edit')->with('transport', $transport);
    }

    /**
     * Update the specified Transport in storage.
     *
     * @param  int              $id
     * @param UpdateTransportRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTransportRequest $request)
    {
        $transport = $this->transportRepository->findWithoutFail($id);

        if (empty($transport)) {
            Flash::error('Схема проезда не найдена');
            return redirect(route('transports.index'));
        }

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');
        if(isset($file)) {
            $img = $file->store('img_contacts');
        }
        else{
            $img =$file1?$file1:'';
        }
        $requestData=$request->all();
        $requestData['img'] = $img;
        //end of add preview

        $transport = $this->transportRepository->update($requestData, $id);

        Flash::success('Схема проезда успешно изменена.');
        //return redirect(route('transports.index'));

        return redirect(route('pages.edit',21));
    }

    /**
     * Remove the specified Transport from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $transport = $this->transportRepository->findWithoutFail($id);

        if (empty($transport)) {
            Flash::error('Схема проезда не найдена');

            return redirect(route('transports.index'));
        }

        $this->transportRepository->delete($id);

        return $id;
        //Flash::success('Схема проезда успешно удалена.');
        //return redirect(route('transports.index'));
    }
}
