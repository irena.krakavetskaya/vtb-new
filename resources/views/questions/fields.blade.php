<!-- Datetime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Дата поступления вопроса:') !!}
    {!! Form::date('date', date('Y-m-d', strtotime($question->date)),  ['class' => 'form-control']) !!}
</div>

<!-- Question Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('question', 'Вопрос:') !!}
    {!! Form::text('question', null, ['class' => 'form-control']) !!}
</div>

<!-- Answer Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('answer', 'Ответ:') !!}
    {!! Form::textarea('answer', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 toggle-modern">
    <div class="formrow clearfix">
        <label for="is_active">Опубликован</label>
        <div id="is_active" class="toggle floatright"></div>
        @if(isset($question->status))
            @if($question->status=='1')
                <input type="text"  class="checkbx" name="status" value="{!! $question->status !!}">
            @else
                <input type="text"  class="checkbx" name="status"  value="0">
            @endif
        @else
            <input   class="text" name="status"  value="0">
        @endif
    </div>
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pages.edit',38) !!}" class="btn btn-default">Отмена</a>
</div>
