<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEmailAPIRequest;
use App\Http\Requests\API\UpdateEmailAPIRequest;
use App\Models\Email;
use App\Repositories\EmailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EmailController
 * @package App\Http\Controllers\API
 */

class EmailAPIController extends AppBaseController
{
    /** @var  EmailRepository */
    private $emailRepository;

    public function __construct(EmailRepository $emailRepo)
    {
        $this->emailRepository = $emailRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/emails",
     *      summary="Get a listing of the Emails.",
     *      tags={"Email"},
     *      description="Get all Emails",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Email")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->emailRepository->pushCriteria(new RequestCriteria($request));
        $this->emailRepository->pushCriteria(new LimitOffsetCriteria($request));
        $emails = $this->emailRepository->all();

        return $this->sendResponse($emails->toArray(), 'Emails retrieved successfully');
    }

    /**
     * @param CreateEmailAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/emails",
     *      summary="Store a newly created Email in storage",
     *      tags={"Email"},
     *      description="Store Email",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Email that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Email")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Email"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEmailAPIRequest $request)
    {
        $input = $request->all();

        $emails = $this->emailRepository->create($input);

        return $this->sendResponse($emails->toArray(), 'Email saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/emails/{id}",
     *      summary="Display the specified Email",
     *      tags={"Email"},
     *      description="Get Email",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Email",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Email"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Email $email */
        $email = $this->emailRepository->findWithoutFail($id);

        if (empty($email)) {
            return $this->sendError('Email not found');
        }

        return $this->sendResponse($email->toArray(), 'Email retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEmailAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/emails/{id}",
     *      summary="Update the specified Email in storage",
     *      tags={"Email"},
     *      description="Update Email",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Email",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Email that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Email")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Email"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEmailAPIRequest $request)
    {
        $input = $request->all();

        /** @var Email $email */
        $email = $this->emailRepository->findWithoutFail($id);

        if (empty($email)) {
            return $this->sendError('Email not found');
        }

        $email = $this->emailRepository->update($input, $id);

        return $this->sendResponse($email->toArray(), 'Email updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/emails/{id}",
     *      summary="Remove the specified Email from storage",
     *      tags={"Email"},
     *      description="Delete Email",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Email",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Email $email */
        $email = $this->emailRepository->findWithoutFail($id);

        if (empty($email)) {
            return $this->sendError('Email not found');
        }

        $email->delete();

        return $this->sendResponse($id, 'Email deleted successfully');
    }
}
