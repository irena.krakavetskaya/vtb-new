$(document).ready(function () {

    /*slider for questions*/
    if($('input[name="status"]').length>0 || $('input[name="status"]').length>0){
        var input = $('input[name="status"]');
        var pos="";
        if(input.val()=='1'){
            pos=true;
        }
        else{
            pos=false;
        }
        $(function(){
            $('.toggle').toggles({
                drag: false,
                text:{
                    on:"ДА",
                    off:"НЕТ"
                },
                checkbox:input,
                on: pos,
                width: 90,
                height: 30
            });
        });
        $('.toggle').on('toggle', function(e, active) {
            if (active) {
                $('input[name="status"]').val('1');
            } else {
                //console.log(false);
                $('input[name="status"]').val('0');
            }
        });
    }
    /*end of slider for questions*/

    //ice palace editor, museum, prices, halls
    if($('#ckeditor1').length>0){
        //CKEDITOR.replace( 'ckeditor1' );

        CKEDITOR.replace( 'ckeditor1', {
            filebrowserWindowWidth: '640',
            filebrowserWindowHeight: '480',
            disallowedContent : 'img{width, height}[width, height]; table{width, border}[width,  border]'//elements [attributes]{styles}(classes)
        });
    }

    if($('#ckeditor2').length>0){
        CKEDITOR.replace( 'ckeditor2', {
            filebrowserWindowWidth: '640',
            filebrowserWindowHeight: '480',
            disallowedContent : 'img{width, height}[width, height]; table{width, border}[width,  border]'//elements [attributes]{styles}(classes)
        });

    }

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });


    //placeholder for login
    $('.login-box-body input').focus(function(){
        $(this).data('placeholder',$(this).attr('placeholder'))
        $(this).attr('placeholder','');
    });
    $('.login-box-body input').blur(function(){
        $(this).attr('placeholder',$(this).data('placeholder'));
    });

    //placeholder for search
    $(".teams input[type='search']").attr('placeholder','Введите название команды ...')


    //preview in file upload in team
    if($("#logo").length>0) {
        document.getElementById("logo").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (e.total > 10000000) {
                    $('#imageerror').text('Размер изображения не должен превышать 10 мб');
                    $jimage = $("#logo");
                    $jimage.val("");
                    $jimage.wrap('<form>').closest('form').get(0).reset();
                    $jimage.unwrap();
                    $('#uploadedimage').removeAttr('src');
                    return;
                }
                $('#imageerror').text('');
                document.getElementById("uploadedimage").src = e.target.result;
            };
            reader.readAsDataURL(this.files[0]);
        };
    }

    //preview in file upload in event
    if($("#img").length>0) {
        document.getElementById("img").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (e.total > 10000000) {//1mb - 1000 kb - 1 000 000 byte
                    $('#imageerror1').text('Размер изображения не должен превышать 10 мб');
                    $jimage = $("#img");
                    $jimage.val("");
                    $jimage.wrap('<form>').closest('form').get(0).reset();
                    $jimage.unwrap();
                    $('#uploadedimage1').removeAttr('src');
                    return;
                }
                $('#imageerror1').text('');
                document.getElementById("uploadedimage1").src = e.target.result;
            };
            reader.readAsDataURL(this.files[0]);
        };
    }

    //wysiwyg editor for shows
    //if($("#editor").length>0){
       /* CKEDITOR.replace( 'editor' , {
            //extraPlugins: 'imageuploader'
            //extraPlugins: 'filebrowser',
            filebrowserBrowseUrl: '../browser/browse.php',
            filebrowserUploadUrl: '../uploader/upload.php'
        });*/
        //CKEDITOR.replace( 'editor', {
           //extraPlugins: 'simpleImageUpload'

            //filebrowserBrowseUrl: '/browser/browse.php?type=Files',
            //filebrowserUploadUrl: '/uploader/upload.php?type=Files'
       // });
   // }

    //preview logo when selected team changes
    $("#team1_id").change(function(){
        var id =  $(this).val();
        var name =  $(this).attr('id');
        console.log(id);
        getLogo(name, id);
    });
    $("#team2_id").change(function(){
        var id =  $(this).val();
        var name =  $(this).attr('id');
        console.log(id);
        getLogo(name, id);
    });

    function getLogo(name, id){
        var url = window.location.origin+'/events/get-logo';
        var obj={
            'id':id
        };
        var data = JSON.stringify(obj);

        $.ajax({
            type:"POST",
            url: url,
            data: data,
            contentType: "application/json",
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            success: function(response){
                console.log(response);
                $('#'+name+ "+img").attr('src','/'+response);

            }
        })
    }

    //filter in events
    $(".filter select#type").change(function(){
        var type =  $(this).val();
        //console.log(type);
        getFiltered('type',type);
    });

    $("input#hidePast").change(function(){
        //console.log(true);
        var hide='hide';
        if($("input:checked").length >0){
            hide = 'hide'
        }
        else{
             hide ='show';
        }
        getFiltered('past',hide);
    });

    $("input#date_from").change(function(){
        var date_from=$(this).val();//2017-12-21
        getFiltered('date_from',date_from);
    });
    $("input#date_till").change(function(){
        var date_till=$(this).val();//2017-12-21
        getFiltered('date_till',date_till);
    });

    function getFiltered(str, filter){
        //console.log(filter);
        search= '';
        if(str !== 'past'  && $("input:checked").length >0){
           search += '&past=hide';
        }
        if(str !== 'type'){
            var val = $("select#type").val();
            search += '&type='+val;
        }
        if(str !== 'date_from'){
            var val = $("input#date_from").val();
            search += '&date_from='+val;
        }
        if(str !== 'date_till'){
            var val = $("input#date_till").val();
            search += '&date_till='+val;
        }

        var url = window.location.origin+'/events';
        $.ajax({
            type:"GET",
            url: url,
            contentType: "application/json",
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            success: function(response){
                window.location.href = 'events?'+str+'='+filter+search;
                //console.log(response);//true
            }
        })
    }

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };

    var past = getUrlParameter('past');
    var type = getUrlParameter('type');
    var date_from = getUrlParameter('date_from');
    var date_till = getUrlParameter('date_till');
    //console.log(type);

    //(past==='hide')?$("#hidePast").prop('checked','checked'):'';
    (past==='show')?$("#hidePast").prop('checked',''):'';
    type?$(".filter  #type").val(type).attr('selected', 'selected'):'';
    date_from?$("#date_from").val(date_from):'';
    date_till?$("#date_till").val(date_till):'';
    //end filter in events

});

//match or show in events
function toggleFields(){
    if($('.create_event #type').val() =='show' || $('.create_event #type').val() =='excursion'){
        $('.create_event').find('.league').hide();
        $('.create_event').find('.teams').hide();
        $('.create_event').find('.kind').show();
        $('.create_event').find('.desc').show();
        $('.create_event').find('.name').show();
        $('.create_event').find('.is_adv').show();
    }
    else{
        $('.create_event').find('.league').show();
        $('.create_event').find('.teams').show();
        $('.create_event').find('.kind').hide();
        $('.create_event').find('.desc').hide();
        $('.create_event').find('.name').hide();
        $('.create_event').find('.is_adv').hide();
    }

    if($('.create_event #type').val() =='excursion'){
        $('.create_event').find('.kind input[name="kind"]').val('Экскурсия');
        $('.create_event').find('.name input[name="name"]').val('Экскурсия'); //Экскурсия по музею
        $('.create_event').find('#time').val('12:00');
        $('.create_event').find('#place_id').val(3);
    }
}


//no important events for museum
function hideImportantField(field){
    if($(field).find('#place_id').val()==3){
        $("input[name='is_important']").parent().hide();
    }
    else{
        $("input[name='is_important']").parent().show();
    }
}

$(function () {
    $('.create_event').find('.league').show();
    $('.create_event').find('.teams').show();
    toggleFields();


    $('.create_event #type').on('change',function(){
        toggleFields();
    });

    //no important events for museum
    $('.create_event #place_id').on('change',function(){
        hideImportantField('.create_event');
    });
    $('.edit_event #place_id').on('change',function() {
        //if ($('.edit_event').find('#place_id').val() == 3) {
            hideImportantField('.edit_event');
    });
    hideImportantField('.edit_event');


    //two-level menu
    if($('.treeview-menu').find('li').hasClass('active')){
        //$('.treeview-menu li.active').parent().parent().addClass('active');
        //$('.treeview-menu').addClass('menu-open').show();
    }
    $('.sidebar ul li').find('a').each(function () {
        if (document.location.href == $(this).attr('href')) {
            //console.log('submenu');
            $(this).parents().addClass("active");
            $(this).addClass("active");
            // add class as you need ul or li or a
        }
    });
    $('.treeview>a').click(function(){
        console.log(true);
        //if($('.treeview').hasClass('menu-open')){
            //$(this).siblings('.treeview-menu').addClass('menu-open').show();
       //}
    });
    if($(".treeview-menu li").hasClass('active')){
        $(".treeview-menu li.active").parent().parent().addClass('active');
    }

    //making parent menu links clickable
    /*
     $(".dropdown-toggle").on('click', function(){
            var url = $(this).attr("href");
            $(location).attr('href', url);
     });
     */


    //tabs
    if( $('.nav-tabs').find('li.active').length == 0){
        $('.nav-tabs').find('li').first().addClass('active');
        $('.tab-content').find('div').first().addClass('active');
    }




    //ice palace image upload
    if($("#img2").length>0) {
        document.getElementById("img2").onchange = function () {
            var reader = new FileReader();
            reader.onload = function (e) {
                if (e.total > 3000000) {//1mb - 1000 kb - 1 000 000 byte
                    $('#imageerror1').text('Размер изображения не должен превышать 3 мб');
                    $jimage = $("#img2");
                    $jimage.val("");
                    $jimage.wrap('<form>').closest('form').get(0).reset();
                    $jimage.unwrap();
                    $('#uploadedimage1').removeAttr('src');
                    return;
                }
                $('#imageerror1').text('');
                document.getElementById("uploadedimage1").src = e.target.result;
                $("#uploadedimage1").css('margin-bottom','10px');
            };
            reader.readAsDataURL(this.files[0]);
        };
    }
    //end of ice palace image upload

    //img_nav upload
    if($("#img_nav").length>0) {
        document.getElementById("img_nav").onchange = function () {
            console.log('1');
            var reader = new FileReader();
            reader.onload = function (e) {
                if (e.total > 15000000) {//1mb - 1000 kb - 1 000 000 byte
                    $('#imageerror1').text('Размер изображения не должен превышать 3 мб');
                    $jimage = $("#img2");
                    $jimage.val("");
                    $jimage.wrap('<form>').closest('form').get(0).reset();
                    $jimage.unwrap();
                    $('#uploadedimage1').removeAttr('src');
                    return;
                }
                $('#imageerror1').text('');
                document.getElementById("uploadedimage1").src = e.target.result;
                $("#uploadedimage1").css('margin-bottom','10px');
            };
            reader.readAsDataURL(this.files[0]);
        };
    }

    //img floors
    if($(".img_nav2").length>0) {
        //document.getElementById("img_nav2").onchange = function () {
        $(".img_nav2").each(function () {
            imgView($(this),2);
        });
    }
    if($(".img_nav1").length>0) {
        //document.getElementById("img_nav2").onchange = function () {
        $(".img_nav1").each(function () {
            imgView($(this),1);
        });
    }
    function imgView(el, num){
        //console.log(el);
        el.change(function(){
            var thisClass  = $(this).parents('.tab-pane').attr('id');
            //console.log(thisClass);
            //console.log($(this).siblings('.uploadedimage2'));
            var reader = new FileReader();
            reader.onload = function (e) {
                var thisClass = $('.tab-pane.active').attr('id');
                if (e.total > 15000000) {//1mb - 1000 kb - 1 000 000 byte
                    $('#'+thisClass).find('.imageerror'+num).text('Размер изображения не должен превышать 3 мб');
                    $jimage = $('#'+thisClass).find(".img_nav2");
                    $jimage.val("");
                    $jimage.wrap('<form>').closest('form').get(0).reset();
                    $jimage.unwrap();
                    $('#'+thisClass).find('.uploadedimage'+num).removeAttr('src');
                    return;
                }
                $('#'+thisClass).find('.imageerror'+num).text('');
                //document.getElementsByClassName("uploadedimage2").src = e.target.result;
                //console.log(e.target.result);
                $('#'+thisClass).find('.uploadedimage'+num).attr('src',e.target.result) ;//'/img_navigation/1/empty-2.jpg'
                console.log($('#'+thisClass).find('.uploadedimage'+num));//$('#tab_1').find(".uploadedimage2").attr('src')
                //console.log($('#tab_1').find(".uploadedimage2").attr('src'));
                $('#'+thisClass).find('.uploadedimage'+num).css('margin-bottom','10px');
            };
            reader.readAsDataURL(this.files[0]);
        });
    }
    //end of img floors



    /*slider for ice palace*/
    if($('input[name="is_active"]').length>0 || $('input[name="is_active"]').length>0){
        var input = $('input[name="is_active"]');
        var pos="";
        if(input.val()=='true'){
            pos=true;
        }
        else{
            pos=false;
        }
        $(function(){
            $('.toggle').toggles({
                drag: false,
                text:{
                    on:"ДА",
                    off:"НЕТ"
                },
                checkbox:input,
                on: pos,
                width: 90,
                height: 30
            });
        });
        $('.toggle').on('toggle', function(e, active) {
            if (active) {
                $('input[name="is_active"]').val('true');
            } else {
                //console.log(false);
                $('input[name="is_active"]').val('false');
            }
        });
    }
    /*end of slider for ice palace*/


    /*pdf download*/
    $('.add-pdf').click(function(){
        event.preventDefault();
        if($('.tr-pdf').length<5) {
            console.log($('.tr-pdf').length);
            var num = $(this).parent().siblings('.tr-pdf').length;
            num++;
            //console.log(num);
            var row = '<div class="tr-pdf">\n' +
                '                <div class="form-group col-sm-6">\n' +
                '                   <input type="text" class="form-control" name="name[]" id="name_' + num + '" placeholder="Имя файла" required="required">\n' +
                '</div>\n' +
                '                <div class="form-group col-sm-3 pdf">\n' +
                '                    <span class="link_name"></span>\n' +
                '                </div>\n' +
                '                <div class="form-group col-sm-3 pdf-btn">\n' +
                '                    <div class=\'btn-group\'>\n' +
                '\n' +
                '                        <div class="file-input">\n' +
                '                           <input name="ids[]" type="hidden" value="' + num + '" class="ids">\n' +
                '                <input type="file" class="inputfile" id="link_' + num + '" name="link[]" accept="application/pdf" data-multiple-caption ="{count} files selected" required="required">\n' +
                '<label for="link_' + num + '">Загрузить файл</label>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                        <a href="#" class=\'btn btn-danger btn-sm\'>\n' +
                '                            Удалить\n' +
                '                        </a>\n' +
                '\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>';

            $(this).parent().before(row);

            pdfDownload();
        }
    });

    pdfDownload();


    function pdfDownload(){
        var inputs = document.querySelectorAll( '.inputfile' );
        //console.log(inputs);
        Array.prototype.forEach.call( inputs, function( input )
        {
            var label	 = input.nextElementSibling,
                labelVal = label.innerHTML;
            input.addEventListener( 'change', function( e )
            {
                console.log($(this).parents('.pdf-btn').prev().find(".link_name").text());
                //console.log(true);
                var fileName = '';
                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $(this).parents('.pdf-btn').prev().find(".link_name").text(fileName); //???
                   //label.querySelector( 'span' ).innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });
        });


        $('.file-input+a').click(function(){
            event.preventDefault();

            var id = $(this).parent().find('.ids').val();
            //console.log(id);
            var html_del = '<input name="del[]" type="hidden" value="'+id+'" class="del">';
            $(this).parents('.tr-pdf').remove();
            $('.add-pdf').after(html_del);

        });


        $('.file-input label').click(function() {
                var id = $(this).siblings('.ids').val();
                var html_del = '<input name="del[]" type="hidden" value="' + id + '" class="del">';
                $('.add-pdf').after(html_del);
        });
    }
    /*end of pdf download*/


    /*pdf download en*/
    $('.add-pdf-en').click(function(){
        event.preventDefault();
        if($('.tr-pdf-en').length<5) {
            console.log($('.tr-pdf-en').length);
            var num = $(this).parent().siblings('.tr-pdf-en').length;
            num++;
            //console.log(num);
            var row = '<div class="tr-pdf-en">\n' +
                '                <div class="form-group col-sm-6">\n' +
                '                   <input type="text" class="form-control" name="name_en[]" id="name_' + num + '" placeholder="Имя файла" required="required">\n' +
                '</div>\n' +
                '                <div class="form-group col-sm-3 pdf">\n' +
                '                    <span class="link_name"></span>\n' +
                '                </div>\n' +
                '                <div class="form-group col-sm-3 pdf-btn">\n' +
                '                    <div class=\'btn-group\'>\n' +
                '\n' +
                '                        <div class="file-input-en">\n' +
                '                           <input name="ids_en[]" type="hidden" value="' + num + '" class="ids">\n' +
                '                <input type="file" class="inputfile" id="link_en_' + num + '" name="link_en[]" accept="application/pdf" data-multiple-caption ="{count} files selected" required="required">\n' +
                '<label for="link_en_' + num + '">Загрузить файл</label>\n' +
                '\n' +
                '                        </div>\n' +
                '\n' +
                '                        <a href="#" class=\'btn btn-danger btn-sm\'>\n' +
                '                            Удалить\n' +
                '                        </a>\n' +
                '\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div>';

            $(this).parent().before(row);

            pdfDownloadEn();
        }
    });
    pdfDownloadEn();
    function pdfDownloadEn(){
        var inputs = document.querySelectorAll( '.inputfile' );
        //console.log(inputs);
        Array.prototype.forEach.call( inputs, function( input )
        {
            var label	 = input.nextElementSibling,
                labelVal = label.innerHTML;
            input.addEventListener( 'change', function( e )
            {
                console.log($(this).parents('.pdf-btn').prev().find(".link_name").text());
                //console.log(true);
                var fileName = '';
                if( this.files && this.files.length > 1 )
                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                else
                    fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    $(this).parents('.pdf-btn').prev().find(".link_name").text(fileName); //???
                //label.querySelector( 'span' ).innerHTML = fileName;
                else
                    label.innerHTML = labelVal;
            });
        });


        $('.file-input-en+a').on('click', function(){
            event.preventDefault();
            var id = $(this).parent().find('.ids').val();
            //console.log(id);
            var html_del = '<input name="del_en[]" type="hidden" value="'+id+'" class="del">';
            $(this).parents('.tr-pdf-en').remove();
            $('.add-pdf-en').after(html_del);
        });


        $('.file-input-en label').click(function() {
            var id = $(this).siblings('.ids').val();
            var html_del = '<input name="del_en[]" type="hidden" value="' + id + '" class="del">';
            $('.add-pdf-en').after(html_del);
        });
    }
    /*end pdf download en*/


    /*1 - slider for museum day_offs nad for  rink schedule template*/
    $.each( $('.day_off'), function(index, value ){
            //console.log($(value).val());
            //console.log($(this));
            var input = $(this);//$('#day_off');
            var pos="";
            if(input.val()==='true'){
                pos=true;
            }
            else{
                pos=false;
            }

            index++;
            //console.log(pos);
            //console.log($('.toggle1_'+index).attr('class'));

            $('.toggle1_'+index).toggles({
                drag: false,
                text:{
                    on:"Выходной",
                    off:"Рабочий"
                },
                checkbox:input,
                on: pos,
                width: 90,
                height: 30
            });

            if($('.toggle1_'+index+' .toggle-on').hasClass('active')){
                $(this).parents('.toggle-modern').siblings('.schedule-block').hide();
            }

            $('.toggle1_'+index).on('toggle', function(e, active) {
                //console.log('toggle1_'+index);
                if (active) {
                    $(this).next('input[name="day_off[]"]').val('true');
                    $(this).parents('.toggle-modern').siblings('.schedule-block').slideUp();//rink schedule template
                } else {
                    $(this).next('input[name="day_off[]"]').val('false');
                    $(this).parents('.toggle-modern').siblings('.schedule-block').slideDown();//rink schedule template
                }
                if($(this).hasClass('schedule-btn')){
                    var id = $(this).attr('id');
                    console.log(id);
                    var url = '/weeks/change-day';
                    var obj={
                        'id':id,
                        'is_active':$(this).next('input[name="day_off[]"]').val()
                    };
                    var data = JSON.stringify(obj);
                    console.log(data);
                    $.ajax({
                        type:"POST",
                        url: url,
                        data: data,
                        contentType: "application/json",
                        headers: {
                            'X-CSRF-Token': $('input[name="_token"]').val()
                        },
                        success: function(response){
                            console.log(response);
                        }
                    })
                }
            });
    });
    /*end of slider for museum day_offs*/

    /*2 - slider for museum price*/
    $.each( $('.price'), function(index, value ){
        //console.log(index);
        var id = $(this).parents('.price-row').find('.id_pr').val();
        //console.log(id);
        index = parseInt(id)-1;
        //console.log(index);
        var input = $(this);//$('#day_off');
        var pos="";
        if(input.val()==='true'){
            pos=true;
        }
        else{
            pos=false;
        }

        index++;
        $('.toggle2_'+index).toggles({
            drag: false,
            text:{
                on:"Вкл",
                off:"Выкл"
            },
            checkbox:input,
            on: pos,
            width: 90,
            height: 30
        });

        $('.toggle2_'+index).on('toggle', function(e, active) {
            if (active) {
                console.log(true);
                $(this).next('input[name="is_shown[]"]').val('true');
            } else {
                $(this).next('input[name="is_shown[]"]').val('false');
            }
        });
    });
    /*end of slider for museum price*/

    /*2 - slider for navigation*/
    $.each( $('.point'), function(index, value ){
        //console.log(index);
        var id = $(this).parents('.point-row').find('.id_point').val();
        //console.log(id);
        index = parseInt(id)-1;
        //console.log(index);
        var input = $(this);//$('#day_off');
        var pos="";
        if(input.val()==='true'){
            pos=true;
        }
        else{
            pos=false;
        }

        index++;
        $('.toggle2_'+index).toggles({
            drag: false,
            text:{
                on:"Вкл",
                off:"Выкл"
            },
            checkbox:input,
            on: pos,
            width: 90,
            height: 30
        });

        $('.toggle2_'+index).on('toggle', function(e, active) {
            if (active) {
                console.log(true);
                $(this).next('input[name="is_shown[]"]').val('true');
            } else {
                $(this).next('input[name="is_shown[]"]').val('false');
            }

            var url = '/points/change-status';
            var obj={
                'id':index,
                'is_active':$(this).next('input[name="is_shown[]"]').val()
            };
            var data = JSON.stringify(obj);
            console.log(data);
            $.ajax({
                type:"POST",
                url: url,
                data: data,
                contentType: "application/json",
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                success: function(response){
                    //console.log(response);
                }
            })

        });
    });
    /*end of slider for navigation*/

    //breaks in museum schedule
    $("input[name='breaks']").click(function () {
        var is_checked=$(this).prop('checked');
        $(this).prev().val(is_checked);
    });

    //preview img gallery museum
    if($("#img_museum").length>0) {
        document.getElementById("img_museum").onchange = function () {
            showPreview(this);
        };
    }

    function showPreview(elem) {
            //console.log(elem);
            var reader = new FileReader();
            reader.onload = function (e) {
                if (e.total > 3000000) {//1mb - 1000 kb - 1 000 000 byte
                    $('#imageerror_museum').text('Размер изображения не должен превышать 3 мб');
                    $jimage = $(".img_museum").last();
                    $jimage.val("");
                    $jimage.wrap('<form>').closest('form').get(0).reset();
                    $jimage.unwrap();
                    $('#uploadedimage_museum').removeAttr('src');
                    return;
                }
                $('#imageerror_museum').text('');
                $('.uploadedimage_museum').last().attr('src',e.target.result);
                $(".uploadedimage_museum").last().css('margin-bottom', '10px');

                /*
                var src=  $('.uploadedimage_museum').last().attr('src');
                var wrapper = '<a href="'+src+'" data-toggle="lightbox" class="preview_timetable"></a>';
                $('.imageerror_museum').last().before(wrapper);
                $('.uploadedimage_museum').last().appendTo($('.preview_timetable').last());
                */
                //new
                var id_img = $('.delete-img').last().attr('id');
                id_img = ++id_img;//parseInt(id_img);
                //console.log(id_img);
                if($(".gallery.timetable").length>0){
                    var classBoot= 'col-sm-4';
                }
                else{
                    var classBoot= 'col-sm-2';
                }
                console.log(classBoot);
                var str='<div class="'+classBoot+'"><input type="hidden" name="MAX_UPLOAD_SIZE" value="3000"><img id="uploadedimage_museum" height="160px" class="uploadedimage_museum"/>' +
                    '<span id="imageerror_museum" style="font-weight: bold; color: red" >' +
                    '</span><input id="img_museum" accept="image/jpg,image/jpeg,image/png" name="img_museum[]" type="file" class="img_museum"></div>';
                var str1='<p class="text-center"><input name="del_img[]" type="hidden" value="' + id_img + '" class="del_img">'+
                    '<a href="#" class="btn btn-danger btn-sm delete-img" id="' + id_img + '">Удалить </a></p>';


                $('.uploadedimage_museum').last().show();
                $('.img_museum').last().hide();
                $('.img_museum').last().parent().after(str);
                //$('.img_museum').after(str1);
                $('.img_museum').eq(-2).after(str1);
                deleteImgMuseum();

                var last = $(".img_museum").last();
                last.on('change', function(){
                    console.log(true);
                    showPreview(last[0]);
                });
                var id = $(this).siblings('.ids').val();
                var html_del = '<input name="del[]" type="hidden" value="' + id + '" class="del">';
                $('.add-pdf').after(html_del);
                //end new
            };

        reader.readAsDataURL(elem.files[0]);
    }
    //preview img gallery museum


    //admin can add only 6 rink events
    is_max_num();
    function is_max_num(){
        if($('.rink_event .rink_event-row').length>=6){
            $('.rink_event .form-group a').attr('disabled','disabled');
        }
        else{
            $('.rink_event .form-group a').attr('disabled',false);
        }
    }
    //end of add only 6 rink events

    //delete img gallery museum
    deleteImgMuseum();
    function deleteImgMuseum(){
        $('.delete-img').click(function () {
            event.preventDefault();
            $(this).parent().parent().remove();
        });
    }
    //end of delete img gallery museum


    //delete price & halls from museum & points from nav & skyboxes
    function deleteItem(item, id){
        var url = '/'+item+'s/destroy/'+id;
        var obj={
            'id':id
        };
        //console.log(url);
        var data = JSON.stringify(obj);
        $.ajax({
            type:"POST",
            url: url,
            data: data,
            contentType: "application/json",
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            success: function(response){
                console.log(response);
                //console.log($('#'+item).find('button').attr('id', response).parent().parent().html());
                $('.'+item).find('button#'+ response).parent().parent().remove();
                /*
                if(item!=='point'){
                    //$('#'+response+'.delete_'+item).parent().parent().remove();
                    $('#'+item).find('button').attr('id', response).parent().parent().remove();
                }
                else{
                    $('#'+response+'.delete_'+item).parent().siblings('.text').empty();
                    //$('#'+response+'.delete_'+item).parent().siblings('.name').empty();
                    $('#'+response+'.delete_'+item).parent().siblings('.img').empty();
                    $('#'+response+'.delete_'+item).parent().siblings('.toggle-modern').find('input[name="is_shown[]"]').val('false');
                    $('.toggle2_'+response).toggles({
                        drag: false,
                        text:{
                            on:"Вкл",
                            off:"Выкл"
                        },
                        checkbox:input,
                        on: pos,
                        width: 90,
                        height: 30
                    });
                }
                is_max_num();
                */
            }
        })
    }

    $('.delete_item').on('click',function(){
        //$('.price-row').one('click', '.delete_price', function (event) {
        event.preventDefault();
        if(confirm('Вы уверены?')===true){
            //console.log($(this).attr('id'));
            var id = $(this).attr('id');
            var name= $(this).parent().parent().attr('class');
            //console.log(name);
            deleteItem(name, id);
            //deleteItem('skyboxe', id);
        }
    });

    /*
   $('.delete_price').on('click',function(){
    //$('.price-row').one('click', '.delete_price', function (event) {
        event.preventDefault();
        if(confirm('Вы уверены?')===true){
            console.log($(this).attr('id'));
            var id = $(this).attr('id');
            deleteItem('price', id);
        }
        //event.stopPropagation();
       //return false;
    });

    $('form').one('click', '.delete_hall', function (event) {
    //$('.delete_hall').click(function(){
        event.preventDefault();
        if(confirm('Вы уверены?')===true){
            var id = $(this).attr('id');
            deleteItem('hall', id);
            return false;
        }
    });


   $('.delete_point').on('click',function(){
       event.preventDefault();
       if(confirm('Вы уверены?')===true){
           console.log($(this).attr('id'));
           var id = $(this).attr('id');
           deleteItem('point', id);
       }
       //event.stopPropagation();
       //return false;
   });

    $('.delete_rink_event').on('click',function(){
        event.preventDefault();
        if(confirm('Вы уверены?')===true){
            console.log($(this).attr('id'));
            var id = $(this).attr('id');
            deleteItem('rink_event', id);

        }
    });
    */
    $('.delete_skyboxe').on('click',function(){
        //$('.price-row').one('click', '.delete_price', function (event) {
        event.preventDefault();
        if(confirm('Вы уверены?')===true){
            console.log($(this).attr('id'));
            var id = $(this).attr('id');
            deleteItem('skyboxe', id);
        }
        //event.stopPropagation();
        //return false;
    });

    /*
    $('.delete_template').on('click',function(){
        //$('.price-row').one('click', '.delete_price', function (event) {
        event.preventDefault();
        if(confirm('Вы уверены?')===true){
            console.log($(this).attr('id'));
            var id = $(this).attr('id');
            deleteItem('template', id);
        }
    });

    $('.delete_rinkSchedule').on('click',function(){
        //$('.price-row').one('click', '.delete_price', function (event) {
        event.preventDefault();
        if(confirm('Вы уверены?')===true){
            console.log($(this).attr('id'));
            var id = $(this).attr('id');
            deleteItem('rinkSchedule', id);
        }
    });
    */
    //end of delete price & halls from museum & points from nav $rink_event & skyboxes

    //show/hide price text
    if($('input[name="is_link"]').length>0){
        $('.desc').hide();
        $('input[name="is_link"]').prop('checked',false);
        //if($('input[name="is_link"]').prop('checked')===true){
        if($('input[name="is_link"]').val()==='true'){
            $('input[name="is_link"]').prop('checked',true);
            $('.desc').show();
        }


        $('input[name="is_link"]').click(function(){
            $('.desc').toggle();
            if($(this).prop('checked')===true){
                $(this).val(true);
            }
            else{
                $(this).val(false);
            }
        })
    }
    //END of show/hide price text

    //check main events
    if( $('input[name="is_important"]').val()==='false'){
        $('input[name="is_important"]').prop('checked','');
    }
    $('input[name="is_important"]').click(function(){
        if($(this).prop('checked')===true){
            $(this).val(true);
        }
        else{
            $(this).val(false);
        }
    });

    if( $('input[name="is_adv"]').val()==='false'){
        $('input[name="is_adv"]').prop('checked','');
    }
    $('input[name="is_adv"]').click(function(){
        if($(this).prop('checked')===true){
            $(this).val(true);
        }
        else{
            $(this).val(false);
        }
    });
    //END of check main events

    //add  rink schedule template - is not used now
    /*
    $('.add-tmpl').click(function(){
        event.preventDefault();

        var day_num = $(this).parents('.schedule-block').siblings('.toggle-modern').find('.floatright').attr('id');
        //console.log(day_num);

        var row = ' <div class="schedule-row">\n' +
                '                <div class="col-md-1">\n' +
                '                   <input type="hidden"   class="form-control" name="day_num[]" value="'+day_num+'">\n'+
                '                </div>\n' +
                '                <div class="col-md-4">\n' +
                '                    <input type="text"   class="form-control" name="name_1[]" value="" required>\n' +
                '                </div>\n' +
                '                <div class="col-md-1">\n' +
                '                   <span>с</span>' +
                '                </div>\n' +
                '                <div class="col-md-2">\n' +
                '                    <input type="time"   class="form-control" name="time_from_1[]" value="" required>\n' +
                '                </div>\n' +
                '                <div class="col-md-1">\n' +
                '                   <span>по</span>' +
                '                </div>\n' +
                '                <div class="col-md-2">\n' +
                '                    <input type="time"   class="form-control" name="time_till_1[]" value="" required>\n' +
                '                </div>\n' +

                '                <div class="col-md-1">\n' +
                '                        <a href="#" class=\'btn btn-danger btn-xs\'>\n' +
                '                            <i class="glyphicon glyphicon-trash"></i>\n' +
                '                        </a>\n' +
                '                    </div>\n' +
                '                </div>\n' +
                '            </div><div class="clearfix"></div>';

        $(this).parent().before(row);

        $(".schedule-row a").click(function(){
                event.preventDefault();
                $(this).parent().parent().remove();
        });
        $('.schedule-page .toggle-modern').attr('active');
    });
    */
    //end of add  rink schedule template

    //upload template
    $('.upload_template').on('click',function(){

        event.preventDefault();
        if(confirm('Вы уверены? Cуществуюшие данные для этой недели будут перезаписаны')===true){
            console.log($(this).attr('id'));

            var id = $(this).attr('id');
            var url = '/weeks/upload-template/'+id;
            var obj={
                'id':id
            };
            console.log(url);
            var data = JSON.stringify(obj);
            $.ajax({
                type:"POST",
                url: url,
                data: data,
                contentType: "application/json",
                headers: {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                success: function(response){
                    //console.log(response);
                    document.location.reload(true);// Перезагрузить текущую страницу, без использования кэша
                },
                error: function(response){
                    alert('Произошел сбой. Попробуйте еще раз');
                }

            })

        }
    });


});
