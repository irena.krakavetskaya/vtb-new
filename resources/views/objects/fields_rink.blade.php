<div class="col-md-12">

    <fieldset  class="pr">
        <legend>Цены на услуги</legend>

        @foreach($prices as $price)
            <div class="price-row">
                <div class="col-md-3">
                    <input name="id_pr[]" type="hidden" value="{!!$price->id  !!}" class="id_pr">
                    {!! $price->name !!}
                </div>
                <div class="col-md-1">
                    {!! $price->price !!} р.
                </div>
                <div class="col-md-4">
                    {!! $price->text !!}
                </div>
                <div class="col-md-2 toggle-modern">
                    <div class="formrow clearfix">
                        <div id="is_active" class="toggle2_{!! $price->id !!} floatright pull-right"></div>
                        @if(isset($price->is_active))
                            @if($price->is_active=='true')
                                <input type="text"  class="checkbx price" name="is_shown[]" value="{!! $price->is_active !!}">
                            @else
                                <input type="text"  class="checkbx price" name="is_shown[]"  value="false">
                            @endif
                        @else
                            <input  type="text" class="checkbx price" name="is_shown[]"  value="true">
                        @endif
                    </div>
                </div>

                <div class='btn-group col-md-2'>
                    <a href="{{ route('prices.edit', $price->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs delete_price',
                        //'onclick' => "return confirm('Вы уверены?')",
                        'id'=>$price->id
                    ]) !!}
                </div>
            </div>
            <div class="clearfix"></div>
        @endforeach


        <div class="form-group col-sm-6">
            <a href="{!! route('prices.create') !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
                Добавить услугу
            </a>
        </div>

    </fieldset>

    <!--
    <fieldset>
        <legend>Схема катка</legend>


        <div class="form-group col-sm-6">
            <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
            if(!empty($object->img))
                <input name="img3" type="hidden" value="!! $object->img  !!}">
                <img src="!! url("/") !!}/!! $object->img !!}" class="img-responsive" id="uploadedimage1">
            endif

            <img id="uploadedimage1" width="200px" />
            <span id="imageerror1" style="font-weight: bold; color: red"></span>

            !! Form::file('img', ['id'=>'img2', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}

        </div>

    </fieldset>

    <fieldset class="rink_event">
        <legend>Мероприятия</legend>

        foreach($rink_events as $ev)
            <div class="rink_event-row">
                <div class="col-md-3">
                    <input name="id_ev[]" type="hidden" value="!!$ev->id  !!}" class="id_ev">
                    !! $ev->name !!}
                </div>
                <div class="col-md-1">
                    <p>
                        <img src="/!! $ev->img !!}" class="img-responsive">
                    </p>
                </div>
                <div class="col-md-6">
                    !! $ev->text !!}
                </div>

                <div class='btn-group col-md-2'>
                    <a href="{ route('rink_events.edit', $ev->id) }}" class='btn btn-default btn-xs'>
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    !! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs delete_rink_event',
                        //'onclick' => "return confirm('Вы уверены?')",
                        'id'=>$ev->id,
                    ]) !!}
                </div>

                <div class="clearfix"></div>
            </div>
        endforeach

        <div class="form-group col-sm-6">
            <a href="!! route('rink_events.create') !!}" class='btn btn-primary btn-sm'>
                Добавить мероприятие
            </a>
        </div>

    </fieldset>


    <fieldset>
            <legend>Контакты</legend>
            <div class="form-group col-sm-6">
                !! Form::label('phone', 'Номер телефона:') !!}
                !! Form::text('phone', $object->phone,  ['class' => 'form-control']) !!}
            </div>

    </fieldset>
    -->

    <fieldset class="gallery timetable">
        <legend>Расписание</legend>
        <div class="form-group col-md-12">
            @foreach($imgs as $img)
                <div class="col-sm-4">
                    <p>
                        <input name="img_rink[]" type="hidden" value="{!!  $img->link  !!}">
                        <a href="/{!! $img->link !!}" data-toggle="lightbox" data-gallery="gallery-timetable">
                            <img src="/{!! $img->link !!}" class="img-responsive">
                        </a>
                    </p>
                    <p class="text-center">
                        <input name="del_img[]" type="hidden" value="{!! $img->id !!}" class="del_img">
                        <a href="#" class='btn btn-danger btn-sm delete-img' id="{!! $img->id !!}">
                            Удалить
                        </a>
                    </p>
                </div>
            @endforeach

            <div class="col-sm-4">
                <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
                <img id="uploadedimage_museum" height="160px" class="uploadedimage_museum">
                <span id="imageerror_museum" style="font-weight: bold; color: red" class="imageerror_museum"></span>
                {!! Form::file('img_museum[]', ['id'=>'img_museum', 'accept'=>'image/jpg,image/jpeg,image/png', 'class'=>'img_museum']) !!}
            </div>
        </div>
    </fieldset>


    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('objects.edit',3) !!}" class="btn btn-default">Отмена</a>
    </div>

</div>

