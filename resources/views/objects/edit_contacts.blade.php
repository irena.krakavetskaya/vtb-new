@extends('layouts.app')

@section('content')
    <section class="content-header">
        <!--<h1>
            Object
        </h1>-->
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="clearfix"></div>

       @include('flash::message')

       <div class="clearfix"></div>
       <div class="box box-primary objects">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($object, ['route' => ['objects.update', $object->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                            @include('objects.fields_contacts')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection