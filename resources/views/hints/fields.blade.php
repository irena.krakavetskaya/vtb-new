<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Название (English):') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<!-- Floor Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('object_id', 'Объект:') !!}
    <select id="object_id" name="object_id" class="form-control">
        @if(isset($hint->object_id))
            @foreach ($objects as $key=>$value)
                @if($value->id == $hint->object_id)
                    <option value="{!! $value->id !!}" selected>{!! $value->name !!}</option>
                @else
                    <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                @endif
            @endforeach
        @else
            @foreach ($objects as $key=>$value)
                <option value="{!! $value->id !!}">{!! $value->name !!}</option>
            @endforeach
        @endif
    </select>
</div>
<div class="clearfix"></div>

<!-- Img Field -->
<div class="form-group col-sm-6">
    {!! Form::label('img', 'Изображение:') !!}<br>

    <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">

    @if(!empty($hint->img)&& ($hint->link===false))
        <img src="/{!! $hint->img !!}" class="img-responsive" id="uploadedimage1"><!--!! url("/") !!}/-->
    @elseif (!empty($hint->img) && ($hint->link===true))
        <img src="{!! $hint->img !!}" class="img-responsive" id="uploadedimage1">
    @endif

    <img id="uploadedimage1" width="200px" />
    <span id="imageerror1" style="font-weight: bold; color: red"></span>

    {!! Form::file('img', ['id'=>'img', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}

    <p>Или укажите ссылку на изображение в интернете:</p>

    @if(!empty($hint->img) && ($hint->link===false))
        <input name="img3" type="text" value="{!! $hint->img !!}" class="form-control">
    @elseif (!empty($hint->img) && ($hint->link===true))
        <input name="img3" type="text" value="{!! $hint->img !!}" class="form-control">
    @elseif (empty($hint->img))
        <input name="img3" type="text" value="" class="form-control">
    @endif

</div>
<div class="clearfix"></div>

<!-- Text Field -->
<div class="form-group col-sm-12">
{!! Form::label('text', 'Текст:') !!}
{!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('text_en', 'Текст (English):') !!}
    {!! Form::textarea('text_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
{!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
<a href="{!! route('hints.index') !!}" class="btn btn-default">Отмена</a>
</div>
