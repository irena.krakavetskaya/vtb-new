<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Variable
 * @package App\Models
 * @version December 26, 2017, 2:23 pm UTC
 *
 * @property string key
 * @property string value
 * @property string description
 * @property string admin
 */
class Variable extends Model
{
    //use SoftDeletes;

    public $table = 'variables';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'key',
        'value',
        'description',
        'admin'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'key' => 'string',
        'value' => 'string',
        'description' => 'string',
        'admin' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'key' => 'required',
        'value' => 'required',
        'admin' => 'required'
    ];

    public $timestamps = false;
}
