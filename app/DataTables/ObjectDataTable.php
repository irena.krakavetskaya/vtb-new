<?php

namespace App\DataTables;

use App\Models\Object;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ObjectDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'objects.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Object $model)
    {
        $objects=$model->whereIn('id',[1,2,5,6]);
        return $this->applyScopes($objects);
        //return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'pageLength' => 50,
                'searching'=> false,
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'buttons' => [
                    'create'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'pos' => ['name' => 'pos', 'data' => 'pos','title'=>'№'],
            'name'=> ['name' => 'name', 'data' => 'name','title'=>'Название'],
            'phone'=> ['name' => 'phone', 'data' => 'phone','title'=>'Телефон'],
            'address'=> ['name' => 'address', 'data' => 'address','title'=>'Адрес'],
            'email' => ['name' => 'email', 'data' => 'email','title'=>'E-mail'],
            'open_hours'=> ['name' => 'open_hours', 'data' => 'open_hours','title'=>'График работы'],
            //'fb',
            //'inst',
            //'vk',
            //'youtube'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'objectsdatatable_' . time();
    }
}