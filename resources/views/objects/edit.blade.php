@extends('layouts.app')

@section('content')
    <section class="content-header">
        <!--<h1>
            Object
        </h1>-->
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="clearfix"></div>

       @include('flash::message')

       <div class="clearfix"></div>
       <div class="box box-primary objects">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($object, ['route' => ['objects.update', $object->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                       @if($object->id===1)
                           @include('objects.fields')
                       @elseif($object->id===2)
                           @include('objects.fields_museum')
                       @elseif($object->id===3)
                            @include('objects.fields_rink')
                       @elseif($object->id===4)
                           @include('objects.fields_skybox')
                       @endif

                   <!--elseif($object->id===5 || $object->id===6)
                       include('objects.fields_contacts')
                        elseif($object->id===1 && $contacts==='true')
                       include('objects.fields_contacts')
                    -->

                   {!! Form::close() !!}

               </div>
           </div>
       </div>
   </div>
@endsection