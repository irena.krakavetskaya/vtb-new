<?php

use App\Models\Skybox;
use App\Repositories\SkyboxRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SkyboxRepositoryTest extends TestCase
{
    use MakeSkyboxTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var SkyboxRepository
     */
    protected $skyboxRepo;

    public function setUp()
    {
        parent::setUp();
        $this->skyboxRepo = App::make(SkyboxRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateSkybox()
    {
        $skybox = $this->fakeSkyboxData();
        $createdSkybox = $this->skyboxRepo->create($skybox);
        $createdSkybox = $createdSkybox->toArray();
        $this->assertArrayHasKey('id', $createdSkybox);
        $this->assertNotNull($createdSkybox['id'], 'Created Skybox must have id specified');
        $this->assertNotNull(Skybox::find($createdSkybox['id']), 'Skybox with given id must be in DB');
        $this->assertModelData($skybox, $createdSkybox);
    }

    /**
     * @test read
     */
    public function testReadSkybox()
    {
        $skybox = $this->makeSkybox();
        $dbSkybox = $this->skyboxRepo->find($skybox->id);
        $dbSkybox = $dbSkybox->toArray();
        $this->assertModelData($skybox->toArray(), $dbSkybox);
    }

    /**
     * @test update
     */
    public function testUpdateSkybox()
    {
        $skybox = $this->makeSkybox();
        $fakeSkybox = $this->fakeSkyboxData();
        $updatedSkybox = $this->skyboxRepo->update($fakeSkybox, $skybox->id);
        $this->assertModelData($fakeSkybox, $updatedSkybox->toArray());
        $dbSkybox = $this->skyboxRepo->find($skybox->id);
        $this->assertModelData($fakeSkybox, $dbSkybox->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteSkybox()
    {
        $skybox = $this->makeSkybox();
        $resp = $this->skyboxRepo->delete($skybox->id);
        $this->assertTrue($resp);
        $this->assertNull(Skybox::find($skybox->id), 'Skybox should not exist in DB');
    }
}
