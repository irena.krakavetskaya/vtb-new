<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Floor
 * @package App\Models
 * @version January 10, 2018, 12:28 pm UTC
 *
 * @property string name
 * @property string name_en
 * @property integer place_id
 * @property string img
 */
class Floor extends Model
{
    //use SoftDeletes;

    public $table = 'floors';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'name_en',
        'place_id',
        'img1',
        'img2'

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_en' => 'string',
        'place_id' => 'integer',
        'img1' => 'string',
        'img2' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'name' => 'required',
        //'place_id' => 'required',
        //'img1' => 'required'
    ];

    public $timestamps = false;

    public function points()
    {
        return $this->hasMany('App\Models\Point', 'floor_id');
    }
}
