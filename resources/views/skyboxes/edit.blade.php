@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Привилегии VIP-лож
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($skybox, ['route' => ['skyboxes.update', $skybox->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('skyboxes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection