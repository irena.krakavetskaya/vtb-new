* Фильтры событий

===================================

Places:
				
				all - все
				1 - ледовый дворец
						   		
				

Categories:
				
				all - все
				sport - спорт
				entertainment - развлечения
				excursion - экскурсия
				
				
Запрос:

    GET /api/v1/events-filter?&year={year?}&month={month?}&day={day?}&category={category?}&startIndex={index?}&endIndex={index?}
    
Пример запроса:
 
    GET /api/v1/events-filter?&year=2018&month=1&category=all&startIndex=1&endIndex=3
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK
    
          [
              {
                  "id": 3,
                  "type": "match",
                  "time": "19:00",
                  "kind": "КХЛ",
                  "dateEvent": "2018-01-09",
                  "place": "Большая арена",
                  "team1": {
                      "name": "Спартак",
                      "logo": "logo/muPgF8fGOjanlNeBKQS0kWde3rAwCmXMz8YjTZf3.jpeg"
                  },
                  "team2": {
                      "name": "ЦСКА",
                      "logo": "logo/Q9R5gkBaoe4x5WHceP2RvAJJYD3YRsWZYsTfpB4y.jpeg"
                  }
              },              
              {
                  "id": 8,
                  "type": "show",
                  "time": "18:00",
                  "kind": null,
                  "name": "Чемпионат России по фехтованию",
                  "img": "img_show/96e1JavC6TyogNNOAurtxMrKHKLViSNdKTQBNIs0.jpeg",
                  "dateEvent": "2018-01-27",
                  "place": "Малая арена"
              },
              {
                  "id": 14,
                  "type": "match",
                  "time": "21:00",
                  "kind": null,
                  "dateEvent": "2018-03-03",
                  "place": "Малая арена",
                  "team1": {
                      "name": "Спартак",
                      "logo": "logo/muPgF8fGOjanlNeBKQS0kWde3rAwCmXMz8YjTZf3.jpeg"
                  },
                  "team2": {
                      "name": "ЦСКА",
                      "logo": "logo/Q9R5gkBaoe4x5WHceP2RvAJJYD3YRsWZYsTfpB4y.jpeg"
                  }
              }
          ]
             
    HTTP/1.1 200 OK
    
          []