<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $translate->id !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $translate->description !!}</p>
</div>

<!-- Front Field -->
<div class="form-group">
    {!! Form::label('front', 'Front:') !!}
    <p>{!! $translate->front !!}</p>
</div>

<!-- Ru Field -->
<div class="form-group">
    {!! Form::label('ru', 'Ru:') !!}
    <p>{!! $translate->ru !!}</p>
</div>

<!-- En Field -->
<div class="form-group">
    {!! Form::label('en', 'En:') !!}
    <p>{!! $translate->en !!}</p>
</div>

