<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AlbumApiTest extends TestCase
{
    use MakeAlbumTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAlbum()
    {
        $album = $this->fakeAlbumData();
        $this->json('POST', '/api/v1/albums', $album);

        $this->assertApiResponse($album);
    }

    /**
     * @test
     */
    public function testReadAlbum()
    {
        $album = $this->makeAlbum();
        $this->json('GET', '/api/v1/albums/'.$album->id);

        $this->assertApiResponse($album->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAlbum()
    {
        $album = $this->makeAlbum();
        $editedAlbum = $this->fakeAlbumData();

        $this->json('PUT', '/api/v1/albums/'.$album->id, $editedAlbum);

        $this->assertApiResponse($editedAlbum);
    }

    /**
     * @test
     */
    public function testDeleteAlbum()
    {
        $album = $this->makeAlbum();
        $this->json('DELETE', '/api/v1/albums/'.$album->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/albums/'.$album->id);

        $this->assertResponseStatus(404);
    }
}
