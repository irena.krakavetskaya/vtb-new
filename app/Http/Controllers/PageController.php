<?php

namespace App\Http\Controllers;

use App\DataTables\PageDataTable;
use App\Http\Requests;
use App\Http\Requests\CreatePageRequest;
use App\Http\Requests\UpdatePageRequest;
use App\Models\Album;
use App\Models\Award;
use App\Models\Banner;
use App\Models\Contact;
use App\Models\ContactType;
use App\Models\Doc;
use App\Models\Food;
use App\Models\Img;
use App\Models\News;
use App\Models\Page;
use App\Models\Partner;
use App\Models\Photo;
use App\Models\Question;
use App\Models\Skybox;
use App\Models\Transport;
use App\Repositories\PageRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class PageController extends AppBaseController
{
    /** @var  PageRepository */
    private $pageRepository;

    public function __construct(PageRepository $pageRepo)
    {
        $this->pageRepository = $pageRepo;
    }

    /**
     * Display a listing of the Page.
     *
     * @param PageDataTable $pageDataTable
     * @return Response
     */
    public function index(PageDataTable $pageDataTable)
    {
        return $pageDataTable->render('pages.index');
    }

    /**
     * Show the form for creating a new Page.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $parent_id=$request->parent_id??0;

        return view('pages.create', ['parent_id'=>$parent_id]);
    }

    /**
     * Store a newly created Page in storage.
     *
     * @param CreatePageRequest $request
     *
     * @return Response
     */
    public function store(CreatePageRequest $request)
    {
        $input = $request->all();

        $parent_id=$request->parent_id??0;

        $page = $this->pageRepository->create($input);

        Flash::success('Page saved successfully.');
        return redirect(route('pages.edit',$parent_id));
    }

    /**
     * Display the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            Flash::error('Page not found');
            return redirect(route('pages.index'));
        }

        return view('pages.show')->with('page', $page);
    }

    /**
     * Show the form for editing the specified Page.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            Flash::error('Page not found');
            return redirect(route('pages.index'));
        }

        $albums=$this->pageRepository->getAlbums($id);
        $data=$this->pageRepository->getVideoAlbums();
        $video = $data[0];
        $video_img = $data[1];

        $imgs = $page->imgs; //Img::where('page_id', $id)->get();
        $contacts = $page->contacts;//Contact::where('page_id', $id)->get();
        $docs=$page->docs;
        $docs_en=$page->docs_en;

        $children = $page->getChildren($id) ;//Page::where('parent_id',$id)->get();
        $children_img=$this->pageRepository->getChildrenImg($children);

        $awards = Award::get();
        $partners = Partner::get();
        $skyboxes=Skybox::get();
        $food = Food::get();
        $transports = Transport::get();
        $banners = Banner::get();
        $questions = Question::get();
        $contact_types=ContactType::select('id','name')->get();

        $data = [
            'page'=> $page,
            'imgs' =>$imgs,
            'docs' =>$docs,
            'docs_en' =>$docs_en,
            'awards' =>$awards,
            'partners' =>$partners,
            'children'=>$children,
            'skyboxes'=>$skyboxes,
            'contact_types'=>$contact_types,
            'contacts'=>$contacts,
            'food'=>$food,
            'transports'=>$transports,
            'albums'=>$albums,
            'video'=>$video,
            'video_img'=>$video_img,
            'banners'=>$banners,
            'children_img'=>$children_img,
            'questions'=>$questions
        ];

        return view('pages.edit')->with($data);
    }

    /**
     * Update the specified Page in storage.
     *
     * @param  int              $id
     * @param UpdatePageRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePageRequest $request)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        //add/delete imgs from galleries
        $imgs = $request->file('img_museum');
        $del_img = $request->del_img;
        $img = new Img();
        $img->uploadImgs($imgs,$del_img,$id);
        //end of add/delete imgs

        //add pdf
        $files = $request->file('link');
        $names=$request->name;
        $ids=$request->ids;
        $del=$request->del;
        $lang = 'ru';
        $doc = new Doc();
        $doc->uploadDocs($files,$names,$ids,$del,$id,$lang);

        //add pdf en
        $files = $request->file('link_en');
        $names=$request->name_en;
        $ids=$request->ids_en;
        $del=$request->del_en;
        $lang = 'en';
        $doc->uploadDocs($files,$names,$ids,$del,$id,$lang);
        //end of add pdf

        //contacts - vip, box-offices, contacts
        $contact = new Contact();
        $contact->saveContacts($request, $id);
        //end of contacts


        if (empty($page)) {
            Flash::error('Page not found');
            return redirect(route('pages.index'));
        }

        $page = $this->pageRepository->update($request->all(), $id);

        Flash::success('Page updated successfully.');
        //return redirect(route('pages.index'));

        if($request->parent_id==31 || $request->parent_id==34){
            return redirect(route('pages.edit',$request->parent_id));
        }
        else{
            return redirect(route('pages.edit',$id));
        }

    }

    /**
     * Remove the specified Page from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $page = $this->pageRepository->findWithoutFail($id);

        if (empty($page)) {
            Flash::error('Page not found');

            return redirect(route('pages.index'));
        }

        $this->pageRepository->delete($id);

        return $id;

        //Flash::success('Page deleted successfully.');
        //return redirect(route('pages.index'));
    }
}
