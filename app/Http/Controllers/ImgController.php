<?php

namespace App\Http\Controllers;

use App\DataTables\ImgDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateImgRequest;
use App\Http\Requests\UpdateImgRequest;
use App\Repositories\ImgRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ImgController extends AppBaseController
{
    /** @var  ImgRepository */
    private $imgRepository;

    public function __construct(ImgRepository $imgRepo)
    {
        $this->imgRepository = $imgRepo;
    }

    /**
     * Display a listing of the Img.
     *
     * @param ImgDataTable $imgDataTable
     * @return Response
     */
    public function index(ImgDataTable $imgDataTable)
    {
        return $imgDataTable->render('imgs.index');
    }

    /**
     * Show the form for creating a new Img.
     *
     * @return Response
     */
    public function create()
    {
        return view('imgs.create');
    }

    /**
     * Store a newly created Img in storage.
     *
     * @param CreateImgRequest $request
     *
     * @return Response
     */
    public function store(CreateImgRequest $request)
    {
        $input = $request->all();

        $img = $this->imgRepository->create($input);

        Flash::success('Img saved successfully.');

        return redirect(route('imgs.index'));
    }

    /**
     * Display the specified Img.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $img = $this->imgRepository->findWithoutFail($id);

        if (empty($img)) {
            Flash::error('Img not found');

            return redirect(route('imgs.index'));
        }

        return view('imgs.show')->with('img', $img);
    }

    /**
     * Show the form for editing the specified Img.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $img = $this->imgRepository->findWithoutFail($id);

        if (empty($img)) {
            Flash::error('Img not found');

            return redirect(route('imgs.index'));
        }

        return view('imgs.edit')->with('img', $img);
    }

    /**
     * Update the specified Img in storage.
     *
     * @param  int              $id
     * @param UpdateImgRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImgRequest $request)
    {
        $img = $this->imgRepository->findWithoutFail($id);

        if (empty($img)) {
            Flash::error('Img not found');

            return redirect(route('imgs.index'));
        }

        $img = $this->imgRepository->update($request->all(), $id);

        Flash::success('Img updated successfully.');

        return redirect(route('imgs.index'));
    }

    /**
     * Remove the specified Img from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $img = $this->imgRepository->findWithoutFail($id);

        if (empty($img)) {
            Flash::error('Img not found');

            return redirect(route('imgs.index'));
        }

        $this->imgRepository->delete($id);

        Flash::success('Img deleted successfully.');

        return redirect(route('imgs.index'));
    }
}
