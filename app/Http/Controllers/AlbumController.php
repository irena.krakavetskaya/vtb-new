<?php

namespace App\Http\Controllers;

use App\DataTables\AlbumDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateAlbumRequest;
use App\Http\Requests\UpdateAlbumRequest;
use App\Models\Photo;
use App\Repositories\AlbumRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Illuminate\Http\Request;

class AlbumController extends AppBaseController
{
    /** @var  AlbumRepository */
    private $albumRepository;

    public function __construct(AlbumRepository $albumRepo)
    {
        $this->albumRepository = $albumRepo;
    }

    /**
     * Display a listing of the Album.
     *
     * @param AlbumDataTable $albumDataTable
     * @return Response
     */
    public function index(AlbumDataTable $albumDataTable)
    {
        return $albumDataTable->render('albums.index');
    }

    /**
     * Show the form for creating a new Album.
     *
     * @return Response
     */
    public function create(Request $request)
    {

        return view('albums.create', ['page_id'=>$request->page_id]);
    }

    /**
     * Store a newly created Album in storage.
     *
     * @param CreateAlbumRequest $request
     *
     * @return Response
     */
    public function store(CreateAlbumRequest $request)
    {
        $input = $request->all();

        $file = $request->file('img');
        $file1 = $request->input('img1');
        if(isset($file)) {
            $img = $file->store('album');
        }
        else{
            $img =$file1?$file1:'';
        }
        $input['img'] = $img;

        if($request->page_id==32){
            $input['type'] = 'img';
        }
        else{
            $input['type'] = 'video';
        }

        $input['date'] = date('Y-m-d H:i:s');

        $album = $this->albumRepository->create($input);

        $id = $album->id;
        //add/delete imgs from galleries
                $imgs = $request->file('img_museum');
                $del_img = $request->del_img;
                $insertedIds=[];
                $last_id = Photo::all()->last()->id;
                if (isset($imgs)) {
                    foreach ($imgs as $key => $val) {
                        $filename = $val->store('image');
                        $insertedId= Photo::insertGetId([ //insert
                            'id' => ++$last_id,
                            'link' => $filename,
                            'album_id' => $id,
                        ]);
                        array_push($insertedIds,$insertedId);
                    }
                }
                if(isset($del_img)){
                    if(!empty($insertedIds)){
                        array_push($del_img,$insertedIds );
                    }
                    Photo::whereNotIn('id', $del_img)
                        ->where('album_id', $id)
                        ->delete();
                }
                else{
                    Photo::where('album_id', $id)
                        ->delete();
                }
         //video
        if(isset($request->link)){
            Photo::insert([
                'id' => ++$last_id,
                'link' => $request->link,
                'album_id' => $id,
            ]);
        }
         //end of add/delete imgs




        Flash::success('Album saved successfully.');
        //return redirect(route('albums.index'));
        return redirect(route('pages.edit',$request->page_id));
    }

    /**
     * Display the specified Album.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $album = $this->albumRepository->findWithoutFail($id);

        if (empty($album)) {
            Flash::error('Album not found');

            return redirect(route('albums.index'));
        }

        return view('albums.show')->with('album', $album);
    }

    /**
     * Show the form for editing the specified Album.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $album = $this->albumRepository->findWithoutFail($id);

        if (empty($album)) {
            Flash::error('Album not found');
            return redirect(route('albums.index'));
        }

        if($album->type=='img'){
            $photo = Photo::where('album_id', $id)->get();
        }
        else{
            $photo = Photo::where('album_id', $id)->first();
        }


        return view('albums.edit')->with(['album'=> $album,'imgs'=> $photo,'page_id'=>$album->page_id]);
    }

    /**
     * Update the specified Album in storage.
     *
     * @param  int              $id
     * @param UpdateAlbumRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAlbumRequest $request)
    {
        $album = $this->albumRepository->findWithoutFail($id);

        if (empty($album)) {
            Flash::error('Album not found');
            return redirect(route('albums.index'));
        }

        $file = $request->file('img');
        $file1 = $request->input('img1');
        if(isset($file)) {
            $img = $file->store('album');
        }
        else{
            $img =$file1?$file1:'';
        }
        $requestData=$request->all();
        $requestData['img'] = $img;


        $album = $this->albumRepository->update($requestData, $id);


        //add/delete imgs from galleries
        $imgs = $request->file('img_museum');
        $del_img = $request->del_img;
        $insertedIds=[];
        $last_id = Photo::all()->last()->id;
        if (isset($imgs)) {
            foreach ($imgs as $key => $val) {
                $filename = $val->store('image');
                $insertedId= Photo::insertGetId([ //insert
                    'id' => ++$last_id,
                    'link' => $filename,
                    'album_id' => $id,
                ]);
                array_push($insertedIds,$insertedId);
            }
        }
        if(isset($del_img)){
            if(!empty($insertedIds)){
                array_push($del_img,$insertedIds );
            }
            Photo::whereNotIn('id', $del_img)
                ->where('album_id', $id)
                ->delete();
        }
        else{
            Photo::where('album_id', $id)
                ->delete();
        }
        if(isset($request->link)){
            $video_link = Photo::where('album_id', $id)->value('id');
            if(isset($video_link)) {
                Photo::where('album_id', $id)
                    ->update(['link' => $request->link]);
            }
            else{
                Photo::insert([ //insert
                    'id' => ++$last_id,
                    'link' => $request->link,
                    'album_id' => $id,
                ]);
            }
        }
        //end of add/delete imgs



        Flash::success('Album updated successfully.');

        //return redirect(route('albums.index'));
        return redirect(route('pages.edit',$request->page_id));
    }

    /**
     * Remove the specified Album from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $album = $this->albumRepository->findWithoutFail($id);

        if (empty($album)) {
            Flash::error('Album not found');
            return redirect(route('albums.index'));
        }


        $album->photos()->delete();
        $this->albumRepository->delete($id);
        return $id;

        //Flash::success('Album deleted successfully.');
        //return redirect(route('albums.index'));
    }
}
