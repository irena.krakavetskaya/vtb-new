<fieldset class="skybox-adv">
    <legend>Партнеры</legend>
    @foreach($partners as $partner)
        <div class="partner">
            <div class="col-md-3">
                <input name="id_hall[]" type="hidden" value="{!!$partner->id  !!}" class="id_hall">
                {!! $partner->name !!}
            </div>
            <div class="col-md-4">
                <a href="/{!! $partner->img !!}" data-toggle="lightbox">
                    <img src="/{!! $partner->img !!}" class="img-responsive">
                </a>
            </div>
            <div class="col-md-3">
                <a href="{!! $partner->site!!}" target="_blank">{!! $partner->site!!}</a>
            </div>
            <div class='btn-group col-md-2'>
                <a href="{{ route('partners.edit', $partner->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs delete_item',
                    'id'=>$partner->id
                ]) !!}
            </div>
            <div class="clearfix"></div>
        </div>
    @endforeach

    <div class="form-group col-sm-6">
        <a href="{!! route('partners.create') !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
            Добавить партнера
        </a>
    </div>
</fieldset>
