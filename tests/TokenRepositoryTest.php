<?php

use App\Models\Token;
use App\Repositories\TokenRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TokenRepositoryTest extends TestCase
{
    use MakeTokenTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var TokenRepository
     */
    protected $tokenRepo;

    public function setUp()
    {
        parent::setUp();
        $this->tokenRepo = App::make(TokenRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateToken()
    {
        $token = $this->fakeTokenData();
        $createdToken = $this->tokenRepo->create($token);
        $createdToken = $createdToken->toArray();
        $this->assertArrayHasKey('id', $createdToken);
        $this->assertNotNull($createdToken['id'], 'Created Token must have id specified');
        $this->assertNotNull(Token::find($createdToken['id']), 'Token with given id must be in DB');
        $this->assertModelData($token, $createdToken);
    }

    /**
     * @test read
     */
    public function testReadToken()
    {
        $token = $this->makeToken();
        $dbToken = $this->tokenRepo->find($token->id);
        $dbToken = $dbToken->toArray();
        $this->assertModelData($token->toArray(), $dbToken);
    }

    /**
     * @test update
     */
    public function testUpdateToken()
    {
        $token = $this->makeToken();
        $fakeToken = $this->fakeTokenData();
        $updatedToken = $this->tokenRepo->update($fakeToken, $token->id);
        $this->assertModelData($fakeToken, $updatedToken->toArray());
        $dbToken = $this->tokenRepo->find($token->id);
        $this->assertModelData($fakeToken, $dbToken->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteToken()
    {
        $token = $this->makeToken();
        $resp = $this->tokenRepo->delete($token->id);
        $this->assertTrue($resp);
        $this->assertNull(Token::find($token->id), 'Token should not exist in DB');
    }
}
