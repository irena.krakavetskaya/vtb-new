<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRinkSchedulesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rink_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->time('time_from');
            $table->time('time_till');
            $table->integer('days_id');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rink_schedules');
    }
}
