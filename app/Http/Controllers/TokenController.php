<?php

namespace App\Http\Controllers;

use App\DataTables\TokenDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTokenRequest;
use App\Http\Requests\UpdateTokenRequest;
use App\Repositories\TokenRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TokenController extends AppBaseController
{
    /** @var  TokenRepository */
    private $tokenRepository;

    public function __construct(TokenRepository $tokenRepo)
    {
        $this->tokenRepository = $tokenRepo;
    }

    /**
     * Display a listing of the Token.
     *
     * @param TokenDataTable $tokenDataTable
     * @return Response
     */
    public function index(TokenDataTable $tokenDataTable)
    {
        return $tokenDataTable->render('tokens.index');
    }

    /**
     * Show the form for creating a new Token.
     *
     * @return Response
     */
    public function create()
    {
        return view('tokens.create');
    }

    /**
     * Store a newly created Token in storage.
     *
     * @param CreateTokenRequest $request
     *
     * @return Response
     */
    public function store(CreateTokenRequest $request)
    {
        $input = $request->all();

        $token = $this->tokenRepository->create($input);

        Flash::success('Token saved successfully.');

        return redirect(route('tokens.index'));
    }

    /**
     * Display the specified Token.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $token = $this->tokenRepository->findWithoutFail($id);

        if (empty($token)) {
            Flash::error('Token not found');

            return redirect(route('tokens.index'));
        }

        return view('tokens.show')->with('token', $token);
    }

    /**
     * Show the form for editing the specified Token.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $token = $this->tokenRepository->findWithoutFail($id);

        if (empty($token)) {
            Flash::error('Token not found');

            return redirect(route('tokens.index'));
        }

        return view('tokens.edit')->with('token', $token);
    }

    /**
     * Update the specified Token in storage.
     *
     * @param  int              $id
     * @param UpdateTokenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTokenRequest $request)
    {
        $token = $this->tokenRepository->findWithoutFail($id);

        if (empty($token)) {
            Flash::error('Token not found');

            return redirect(route('tokens.index'));
        }

        $token = $this->tokenRepository->update($request->all(), $id);

        Flash::success('Token updated successfully.');

        return redirect(route('tokens.index'));
    }

    /**
     * Remove the specified Token from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $token = $this->tokenRepository->findWithoutFail($id);

        if (empty($token)) {
            Flash::error('Token not found');

            return redirect(route('tokens.index'));
        }

        $this->tokenRepository->delete($id);

        Flash::success('Token deleted successfully.');

        return redirect(route('tokens.index'));
    }
}
