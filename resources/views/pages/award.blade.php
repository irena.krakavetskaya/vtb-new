<fieldset class="skybox-adv">
    <legend>Награды</legend>
    @foreach($awards as $award)
        <div class="award">
            <div class="col-md-3 text">
                {!! $award->name !!}
            </div>
            <div class="col-md-4">
                <a href="/{!! $award->img !!}" data-toggle="lightbox">
                    <img src="/{!! $award->img !!}" class="img-responsive">
                </a>
            </div>
            <div class="col-md-3 text">
                {!! $award->text !!}
            </div>
            <div class='btn-group col-md-2'>
                <a href="{{ route('awards.edit', $award->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs delete_item',
                    'id'=>$award->id
                ]) !!}
            </div>


            <div class="clearfix"></div>
        </div>
    @endforeach

    <div class="form-group col-sm-6">
        <a href="{!! route('awards.create') !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
            Добавить награду
        </a>
    </div>
</fieldset>