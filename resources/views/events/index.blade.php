@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">События</h1>

        <div class="pull-left teams-btn">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('teams.index') !!}" target="_blank">Команды</a>
        </div>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body event">
                <div class="pull-left col-md-10  col-sm-6 no-padding">

                    <form class="form-inline filter">
                        <div class="form-group col-md-3 col-sm-12 no-padding">
                            <label>Показать события с</label>
                            <input type="date"  name="date_from" value="" id="date_from" class="form-control">
                        </div>

                        <div class="form-group col-md-3  col-sm-12 no-padding">
                            <label>по</label>
                            <input type="date"  name="date_till" value="" id="date_till" class="form-control">
                        </div>

                        <div class="form-group col-md-3  col-sm-12  no-padding">
                            <label> Тип события</label>
                            <select class="form-control" id="type">
                                <option value="all" selected>Все</option>
                                <option value="match">Матч</option>
                                <option value="show">Мероприятие</option>
                                <option value="excursion">Экскурсия</option>
                            </select>
                        </div>

                        <div class="form-group col-md-3  col-sm-12 no-padding">
                            <label class="label-past"><!-- class="checkbox-inline"-->
                                <input type="checkbox"  name="hidePast" value="hide" id="hidePast" checked>Скрыть прошедшие
                            </label>
                        </div>
                    </form>

                </div>
                <div class="pull-right col-md-2  col-sm-6 no-padding">
                    <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('events.create') !!}">Новое событие</a>
                </div>
                    @include('events.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection

