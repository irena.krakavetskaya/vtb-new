<?php

use Faker\Factory as Faker;
use App\Models\Variable;
use App\Repositories\VariableRepository;

trait MakeVariableTrait
{
    /**
     * Create fake instance of Variable and save it in database
     *
     * @param array $variableFields
     * @return Variable
     */
    public function makeVariable($variableFields = [])
    {
        /** @var VariableRepository $variableRepo */
        $variableRepo = App::make(VariableRepository::class);
        $theme = $this->fakeVariableData($variableFields);
        return $variableRepo->create($theme);
    }

    /**
     * Get fake instance of Variable
     *
     * @param array $variableFields
     * @return Variable
     */
    public function fakeVariable($variableFields = [])
    {
        return new Variable($this->fakeVariableData($variableFields));
    }

    /**
     * Get fake data of Variable
     *
     * @param array $postFields
     * @return array
     */
    public function fakeVariableData($variableFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'key' => $fake->word,
            'value' => $fake->word,
            'description' => $fake->word,
            'admin' => $fake->word
        ], $variableFields);
    }
}
