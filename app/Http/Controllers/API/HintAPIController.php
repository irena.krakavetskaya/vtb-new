<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHintAPIRequest;
use App\Http\Requests\API\UpdateHintAPIRequest;
use App\Models\Hint;
use App\Models\Object;
use App\Models\Variable;
use App\Repositories\HintRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class HintController
 * @package App\Http\Controllers\API
 */

class HintAPIController extends AppBaseController
{
    /** @var  HintRepository */
    private $hintRepository;

    public function __construct(HintRepository $hintRepo)
    {
        $this->hintRepository = $hintRepo;
    }

    /**
     * Display a listing of the Hint.
     * GET|HEAD /hints
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->hintRepository->pushCriteria(new RequestCriteria($request));
        //$this->hintRepository->pushCriteria(new LimitOffsetCriteria($request));
        //$hints = $this->hintRepository->all();

        $hints =$this->hintRepository->getFields($request)->get();

        return $this->sendResponse($hints->toArray(), 'Hints retrieved successfully');
    }

    /**
     * Store a newly created Hint in storage.
     * POST /hints
     *
     * @param CreateHintAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHintAPIRequest $request)
    {
        $input = $request->all();

        $hints = $this->hintRepository->create($input);

        return $this->sendResponse($hints->toArray(), 'Hint saved successfully');
    }

    /**
     * Display the specified Hint.
     * GET|HEAD /hints/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request) //$id = object_id
    {
        /** @var Hint $hint */
        //$hint = $this->hintRepository->findWithoutFail($id);

        /*if (empty($hint)) {
            return $this->sendError('Hint not found');
        }*/

        $object=Object::find($id);

        $hints = $object->getHints();

        $hints=$this->hintRepository->getFields($request,$hints)->get();

        $res = $hints->each(function ($item) {
            $outer = strpos($item->img,'http');
            if($outer===false){
                if($item->img !== null || $item->img != ''){
                    $host = Variable::where('key','dev_host')->value('value');
                    $item->img=$host.'/'.$item->img;
                }
            }
        });

        return $res;
        //return $object->hints;

        //return $this->sendResponse($hint->toArray(), 'Hint retrieved successfully');
    }

    /**
     * Update the specified Hint in storage.
     * PUT/PATCH /hints/{id}
     *
     * @param  int $id
     * @param UpdateHintAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHintAPIRequest $request)
    {
        $input = $request->all();

        /** @var Hint $hint */
        $hint = $this->hintRepository->findWithoutFail($id);

        if (empty($hint)) {
            return $this->sendError('Hint not found');
        }

        $hint = $this->hintRepository->update($input, $id);

        return $this->sendResponse($hint->toArray(), 'Hint updated successfully');
    }

    /**
     * Remove the specified Hint from storage.
     * DELETE /hints/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Hint $hint */
        $hint = $this->hintRepository->findWithoutFail($id);

        if (empty($hint)) {
            return $this->sendError('Hint not found');
        }

        $hint->delete();

        return $this->sendResponse($id, 'Hint deleted successfully');
    }
}
