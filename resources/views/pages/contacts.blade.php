<fieldset class="skybox-adv">
    <legend>Контакты</legend>

    <!--
    foreach($contacts as $contact)
        <div class="form-group col-sm-12">

            <div class="form-group col-sm-4">
                <select id="contact_type" name="contact_type" class="form-control">
                    if(isset($contact->id))
                        foreach ($contact_types as $types)
                            if($types->id == $contact->contact_type_id)
                                <option value="!! $types->id !!}" selected>!! $types->name !!}</option>
                            else
                                <option value="!! $types->id !!}">!! $types->name !!}</option>
                            endif
                        endforeach
                    else
                        foreach ($contact_types as $types)
                            <option value="!! $types->id !!}">!! $types->name !!}</option>
                        endforeach
                    endif
                </select>
            </div>

            <div class="form-group col-sm-4">
                !! Form::text($contact->id, $contact->value,  ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-sm-4">
               !! Form::text($contact->id, $contact->description,  ['class' => 'form-control']) !!}
            </div>
        </div>
    endforeach

    <div class="form-group col-sm-6">
        <a href="#" class='btn btn-primary btn-sm add-pdf'>
            Добавить контакт
        </a>
    </div>
-->
    @foreach($contacts as $item)
        <div class="contact">
            <div class="col-md-3">
                {!! $item->contactType->name !!}
            </div>

            <div class="col-md-3">
                {!! $item->value!!}
            </div>

            <div class="col-md-4 text">
                {!! $item->description !!}
            </div>
            <div class='btn-group col-md-2'>
                <a href="{{ route('contacts.edit', $item->id) }}" class='btn btn-default btn-xs'>
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs delete_item',
                    'id'=>$item->id
                ]) !!}
            </div>
            <div class="clearfix"></div>
        </div>
    @endforeach

    <div class="form-group col-sm-6">
        <a href="{!! route('contacts.create', ['page_id'=>$item->page_id]) !!}" class='btn btn-primary btn-sm'>
            Добавить контакт
        </a>
    </div>

</fieldset>