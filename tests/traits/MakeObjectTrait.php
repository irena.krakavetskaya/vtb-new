<?php

use Faker\Factory as Faker;
use App\Models\Object;
use App\Repositories\ObjectRepository;

trait MakeObjectTrait
{
    /**
     * Create fake instance of Object and save it in database
     *
     * @param array $objectFields
     * @return Object
     */
    public function makeObject($objectFields = [])
    {
        /** @var ObjectRepository $objectRepo */
        $objectRepo = App::make(ObjectRepository::class);
        $theme = $this->fakeObjectData($objectFields);
        return $objectRepo->create($theme);
    }

    /**
     * Get fake instance of Object
     *
     * @param array $objectFields
     * @return Object
     */
    public function fakeObject($objectFields = [])
    {
        return new Object($this->fakeObjectData($objectFields));
    }

    /**
     * Get fake data of Object
     *
     * @param array $postFields
     * @return array
     */
    public function fakeObjectData($objectFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'name_en' => $fake->word,
            'introtext' => $fake->word,
            'introtext_en' => $fake->word,
            'text' => $fake->word,
            'text_en' => $fake->word,
            'img' => $fake->word,
            'fb' => $fake->word,
            'inst' => $fake->word,
            'vk' => $fake->word,
            'youtube' => $fake->word
        ], $objectFields);
    }
}
