<?php

use App\Models\Album;
use App\Repositories\AlbumRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AlbumRepositoryTest extends TestCase
{
    use MakeAlbumTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AlbumRepository
     */
    protected $albumRepo;

    public function setUp()
    {
        parent::setUp();
        $this->albumRepo = App::make(AlbumRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAlbum()
    {
        $album = $this->fakeAlbumData();
        $createdAlbum = $this->albumRepo->create($album);
        $createdAlbum = $createdAlbum->toArray();
        $this->assertArrayHasKey('id', $createdAlbum);
        $this->assertNotNull($createdAlbum['id'], 'Created Album must have id specified');
        $this->assertNotNull(Album::find($createdAlbum['id']), 'Album with given id must be in DB');
        $this->assertModelData($album, $createdAlbum);
    }

    /**
     * @test read
     */
    public function testReadAlbum()
    {
        $album = $this->makeAlbum();
        $dbAlbum = $this->albumRepo->find($album->id);
        $dbAlbum = $dbAlbum->toArray();
        $this->assertModelData($album->toArray(), $dbAlbum);
    }

    /**
     * @test update
     */
    public function testUpdateAlbum()
    {
        $album = $this->makeAlbum();
        $fakeAlbum = $this->fakeAlbumData();
        $updatedAlbum = $this->albumRepo->update($fakeAlbum, $album->id);
        $this->assertModelData($fakeAlbum, $updatedAlbum->toArray());
        $dbAlbum = $this->albumRepo->find($album->id);
        $this->assertModelData($fakeAlbum, $dbAlbum->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAlbum()
    {
        $album = $this->makeAlbum();
        $resp = $this->albumRepo->delete($album->id);
        $this->assertTrue($resp);
        $this->assertNull(Album::find($album->id), 'Album should not exist in DB');
    }
}
