<?php

namespace App\Repositories;

use App\Models\Translate;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TranslateRepository
 * @package App\Repositories
 * @version December 26, 2017, 2:05 pm UTC
 *
 * @method Translate findWithoutFail($id, $columns = ['*'])
 * @method Translate find($id, $columns = ['*'])
 * @method Translate first($columns = ['*'])
*/
class TranslateRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'description',
        'front',
        'ru',
        'en'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Translate::class;
    }
}
