<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FloorApiTest extends TestCase
{
    use MakeFloorTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateFloor()
    {
        $floor = $this->fakeFloorData();
        $this->json('POST', '/api/v1/floors', $floor);

        $this->assertApiResponse($floor);
    }

    /**
     * @test
     */
    public function testReadFloor()
    {
        $floor = $this->makeFloor();
        $this->json('GET', '/api/v1/floors/'.$floor->id);

        $this->assertApiResponse($floor->toArray());
    }

    /**
     * @test
     */
    public function testUpdateFloor()
    {
        $floor = $this->makeFloor();
        $editedFloor = $this->fakeFloorData();

        $this->json('PUT', '/api/v1/floors/'.$floor->id, $editedFloor);

        $this->assertApiResponse($editedFloor);
    }

    /**
     * @test
     */
    public function testDeleteFloor()
    {
        $floor = $this->makeFloor();
        $this->json('DELETE', '/api/v1/floors/'.$floor->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/floors/'.$floor->id);

        $this->assertResponseStatus(404);
    }
}
