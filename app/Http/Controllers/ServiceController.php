<?php

namespace App\Http\Controllers;

use App\DataTables\ServiceDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use App\Repositories\ServiceRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class ServiceController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepo)
    {
        $this->serviceRepository = $serviceRepo;
    }

    /**
     * Display a listing of the Service.
     *
     * @param ServiceDataTable $serviceDataTable
     * @return Response
     */
    public function index(ServiceDataTable $serviceDataTable)
    {
        return $serviceDataTable->render('services.index');
    }

    /**
     * Show the form for creating a new Service.
     *
     * @return Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created Service in storage.
     *
     * @param CreateServiceRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceRequest $request)
    {

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');

        if(isset($file)) {
            $img = $file->store('img_services/');
        }
        else{
            $img =$file1?$file1:'';
        }
        //end of add preview

        $requestData=$request->all();
        $requestData['img'] = $img;

        //$service = $this->serviceRepository->create($input);
        $service = $this->serviceRepository->create($requestData);

        Flash::success('Услуга успешно добавлена.');

        return redirect(route('services.index'));
    }

    /**
     * Display the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            Flash::error('Service not found');

            return redirect(route('services.index'));
        }

        return view('services.show')->with('service', $service);
    }

    /**
     * Show the form for editing the specified Service.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            Flash::error('Услуга на найдена');
            return redirect(route('services.index'));
        }



        return view('services.edit')->with('service', $service);
    }

    /**
     * Update the specified Service in storage.
     *
     * @param  int              $id
     * @param UpdateServiceRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceRequest $request)
    {
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            Flash::error('Услуга на найдена');
            return redirect(route('services.index'));
        }

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');

        if(isset($file)) {
            $img = $file->store('img_services/');
        }
        else{
            $img =$file1?$file1:'';
        }
        //end of add preview

        $requestData=$request->all();
        $requestData['img'] = $img;

        $service = $this->serviceRepository->update($requestData, $id);

        //$service = $this->serviceRepository->update($request->all(), $id);

        Flash::success('Услуга успешно изменена.');
        return redirect(route('services.index'));
    }

    /**
     * Remove the specified Service from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            Flash::error('Услуга на найдена');

            return redirect(route('services.index'));
        }

        $this->serviceRepository->delete($id);

        Flash::success('Услуга успешно удалена.');

        return redirect(route('services.index'));
    }
}
