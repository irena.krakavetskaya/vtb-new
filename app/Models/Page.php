<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Page",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pagetitle",
 *          description="pagetitle",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longtitle",
 *          description="longtitle",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="introtext",
 *          description="introtext",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description",
 *          description="description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="content",
 *          description="content",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="menuindex",
 *          description="menuindex",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="menutitle",
 *          description="menutitle",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="alias",
 *          description="alias",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="parent",
 *          description="parent",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="isfolder",
 *          description="isfolder",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="template_id",
 *          description="template_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="seo_title",
 *          description="seo_title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_keywords",
 *          description="seo_keywords",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="seo_description",
 *          description="seo_description",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pagetitle_en",
 *          description="pagetitle_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="longtitle_en",
 *          description="longtitle_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="introtext_en",
 *          description="introtext_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="description_en",
 *          description="description_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="content_en",
 *          description="content_en",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="menutitle_en",
 *          description="menutitle_en",
 *          type="string"
 *      )
 * )
 */
class Page extends Model
{
    //use SoftDeletes;

    public $table = 'pages';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'pagetitle',
        'longtitle',
        'introtext',
        'description',
        'content',
        'menuindex',
        'menutitle',
        'alias',
        'parent_id',
        'isfolder',
        'template_id',
        'seo_title',
        'seo_keywords',
        'seo_description',
        'pagetitle_en',
        'longtitle_en',
        'introtext_en',
        'description_en',
        'content_en',
        'menutitle_en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pagetitle' => 'string',
        'longtitle' => 'string',
        'introtext' => 'string',
        'description' => 'string',
        'content' => 'string',
        'menuindex' => 'integer',
        'menutitle' => 'string',
        'alias' => 'string',
        'parent_id' => 'integer',
        'isfolder' => 'string',
        'template_id' => 'integer',
        'seo_title' => 'string',
        'seo_keywords' => 'string',
        'seo_description' => 'string',
        'pagetitle_en' => 'string',
        'longtitle_en' => 'string',
        'introtext_en' => 'string',
        'description_en' => 'string',
        'content_en' => 'string',
        'menutitle_en' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pagetitle' => 'required',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\Page', 'parent_id', 'id');
    }

    public function getChildren($id)
    {
        return Page::with('items')->where('parent_id',$id)->get();
    }


    public function imgs()
    {
        return $this->hasMany(\App\Models\Img::class);
    }

    public function docs()
    {
        return $this->hasMany(\App\Models\Doc::class)->whereNull('link_en');

    }

    public function docs_en()
    {
        return $this->hasMany(\App\Models\Doc::class)->whereNotNull('link_en');

    }

    public function contacts()
    {
        return $this->hasMany(\App\Models\Contact::class);

    }

    //for  multilingual api
    public function getPageFields($request)
    {
        $lang = $request->header('X-Language');
        if($lang=='en'){
            return $this->select('id','pagetitle_en as name','introtext_en AS additional','content_en as text');
        }
        else{
            return $this->select('id','pagetitle as name','introtext AS additional','content as text');
        }
    }

    public function modifyContent($item)
    {
                    $host=Variable::where('id',3)->value('value');
                    $item = str_replace('"','\'',$item);
                    $pattern = '/src=\'/';
                    $replacement = 'src=\''.$host; //'src=\'http://dev.vtb.indi.by';
                    $item = preg_replace($pattern, $replacement,$item);
                    return  '<body>'.$item.'</body>';

    }
}
