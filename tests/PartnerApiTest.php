<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PartnerApiTest extends TestCase
{
    use MakePartnerTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatePartner()
    {
        $partner = $this->fakePartnerData();
        $this->json('POST', '/api/v1/partners', $partner);

        $this->assertApiResponse($partner);
    }

    /**
     * @test
     */
    public function testReadPartner()
    {
        $partner = $this->makePartner();
        $this->json('GET', '/api/v1/partners/'.$partner->id);

        $this->assertApiResponse($partner->toArray());
    }

    /**
     * @test
     */
    public function testUpdatePartner()
    {
        $partner = $this->makePartner();
        $editedPartner = $this->fakePartnerData();

        $this->json('PUT', '/api/v1/partners/'.$partner->id, $editedPartner);

        $this->assertApiResponse($editedPartner);
    }

    /**
     * @test
     */
    public function testDeletePartner()
    {
        $partner = $this->makePartner();
        $this->json('DELETE', '/api/v1/partners/'.$partner->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/partners/'.$partner->id);

        $this->assertResponseStatus(404);
    }
}
