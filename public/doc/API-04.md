* Список анонсов

===================================

Запрос:

    GET /api/v1/advertisement
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 [
                      {
                          "id": 2,
                          "time": "21:00",
                          "name": "ОТКРЫТИЕ КАТКА В ПАРКЕ ЛЕГЕНД",
                          "img": "img_show/ViDL9ki16sCs67G80W6IHL9nsDTCjSbV5S7O3iFt.jpeg",
                          "dateEvent": "2018-01-08",
                          "place": "Каток"
                      },
                      {
                          "id": 8,
                          "time": "18:00",
                          "name": "Чемпионат России по фехтованию",
                          "img": "img_show/96e1JavC6TyogNNOAurtxMrKHKLViSNdKTQBNIs0.jpeg",
                          "dateEvent": "2018-01-27",
                          "place": "Малая арена"
                      },
                      {
                          "id": 12,
                          "time": "18:30",
                          "name": "Новогодний спектакль И. Авербуха ",
                          "img": "img_show/5y9Qfz34TTRrcIDyfQafhDDn3IBOBi4fUnq932vI.jpeg",
                          "dateEvent": "2018-02-03",
                          "place": "Музей Хоккея"
                      }

             ]