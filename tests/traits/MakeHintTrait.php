<?php

use Faker\Factory as Faker;
use App\Models\Hint;
use App\Repositories\HintRepository;

trait MakeHintTrait
{
    /**
     * Create fake instance of Hint and save it in database
     *
     * @param array $hintFields
     * @return Hint
     */
    public function makeHint($hintFields = [])
    {
        /** @var HintRepository $hintRepo */
        $hintRepo = App::make(HintRepository::class);
        $theme = $this->fakeHintData($hintFields);
        return $hintRepo->create($theme);
    }

    /**
     * Get fake instance of Hint
     *
     * @param array $hintFields
     * @return Hint
     */
    public function fakeHint($hintFields = [])
    {
        return new Hint($this->fakeHintData($hintFields));
    }

    /**
     * Get fake data of Hint
     *
     * @param array $postFields
     * @return array
     */
    public function fakeHintData($hintFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'text' => $fake->word,
            'img' => $fake->word,
            'floor_id' => $fake->randomDigitNotNull
        ], $hintFields);
    }
}
