<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AwardApiTest extends TestCase
{
    use MakeAwardTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAward()
    {
        $award = $this->fakeAwardData();
        $this->json('POST', '/api/v1/awards', $award);

        $this->assertApiResponse($award);
    }

    /**
     * @test
     */
    public function testReadAward()
    {
        $award = $this->makeAward();
        $this->json('GET', '/api/v1/awards/'.$award->id);

        $this->assertApiResponse($award->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAward()
    {
        $award = $this->makeAward();
        $editedAward = $this->fakeAwardData();

        $this->json('PUT', '/api/v1/awards/'.$award->id, $editedAward);

        $this->assertApiResponse($editedAward);
    }

    /**
     * @test
     */
    public function testDeleteAward()
    {
        $award = $this->makeAward();
        $this->json('DELETE', '/api/v1/awards/'.$award->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/awards/'.$award->id);

        $this->assertResponseStatus(404);
    }
}
