<?php

namespace App\Http\Controllers;

use App\DataTables\TranslateDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTranslateRequest;
use App\Http\Requests\UpdateTranslateRequest;
use App\Repositories\TranslateRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class TranslateController extends AppBaseController
{
    /** @var  TranslateRepository */
    private $translateRepository;

    public function __construct(TranslateRepository $translateRepo)
    {
        $this->translateRepository = $translateRepo;
    }

    /**
     * Display a listing of the Translate.
     *
     * @param TranslateDataTable $translateDataTable
     * @return Response
     */
    public function index(TranslateDataTable $translateDataTable)
    {
        return $translateDataTable->render('translates.index');
    }

    /**
     * Show the form for creating a new Translate.
     *
     * @return Response
     */
    public function create()
    {
        return view('translates.create');
    }

    /**
     * Store a newly created Translate in storage.
     *
     * @param CreateTranslateRequest $request
     *
     * @return Response
     */
    public function store(CreateTranslateRequest $request)
    {
        $input = $request->all();

        $translate = $this->translateRepository->create($input);

        Flash::success('Translate saved successfully.');

        return redirect(route('translates.index'));
    }

    /**
     * Display the specified Translate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $translate = $this->translateRepository->findWithoutFail($id);

        if (empty($translate)) {
            Flash::error('Translate not found');

            return redirect(route('translates.index'));
        }

        return view('translates.show')->with('translate', $translate);
    }

    /**
     * Show the form for editing the specified Translate.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $translate = $this->translateRepository->findWithoutFail($id);

        if (empty($translate)) {
            Flash::error('Translate not found');

            return redirect(route('translates.index'));
        }

        return view('translates.edit')->with('translate', $translate);
    }

    /**
     * Update the specified Translate in storage.
     *
     * @param  int              $id
     * @param UpdateTranslateRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTranslateRequest $request)
    {
        $translate = $this->translateRepository->findWithoutFail($id);

        if (empty($translate)) {
            Flash::error('Translate not found');

            return redirect(route('translates.index'));
        }

        $translate = $this->translateRepository->update($request->all(), $id);

        Flash::success('Translate updated successfully.');

        return redirect(route('translates.index'));
    }

    /**
     * Remove the specified Translate from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $translate = $this->translateRepository->findWithoutFail($id);

        if (empty($translate)) {
            Flash::error('Translate not found');

            return redirect(route('translates.index'));
        }

        $this->translateRepository->delete($id);

        Flash::success('Translate deleted successfully.');

        return redirect(route('translates.index'));
    }
}
