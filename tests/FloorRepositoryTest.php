<?php

use App\Models\Floor;
use App\Repositories\FloorRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class FloorRepositoryTest extends TestCase
{
    use MakeFloorTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var FloorRepository
     */
    protected $floorRepo;

    public function setUp()
    {
        parent::setUp();
        $this->floorRepo = App::make(FloorRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateFloor()
    {
        $floor = $this->fakeFloorData();
        $createdFloor = $this->floorRepo->create($floor);
        $createdFloor = $createdFloor->toArray();
        $this->assertArrayHasKey('id', $createdFloor);
        $this->assertNotNull($createdFloor['id'], 'Created Floor must have id specified');
        $this->assertNotNull(Floor::find($createdFloor['id']), 'Floor with given id must be in DB');
        $this->assertModelData($floor, $createdFloor);
    }

    /**
     * @test read
     */
    public function testReadFloor()
    {
        $floor = $this->makeFloor();
        $dbFloor = $this->floorRepo->find($floor->id);
        $dbFloor = $dbFloor->toArray();
        $this->assertModelData($floor->toArray(), $dbFloor);
    }

    /**
     * @test update
     */
    public function testUpdateFloor()
    {
        $floor = $this->makeFloor();
        $fakeFloor = $this->fakeFloorData();
        $updatedFloor = $this->floorRepo->update($fakeFloor, $floor->id);
        $this->assertModelData($fakeFloor, $updatedFloor->toArray());
        $dbFloor = $this->floorRepo->find($floor->id);
        $this->assertModelData($fakeFloor, $dbFloor->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteFloor()
    {
        $floor = $this->makeFloor();
        $resp = $this->floorRepo->delete($floor->id);
        $this->assertTrue($resp);
        $this->assertNull(Floor::find($floor->id), 'Floor should not exist in DB');
    }
}
