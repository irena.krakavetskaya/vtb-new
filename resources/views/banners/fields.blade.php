<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    <p>
        {!! Form::label('img', 'Баннер (рекоменд. 150*150 px):') !!}
        <input type="hidden" name="MAX_UPLOAD_SIZE" value="10000000">

        @if(!empty($banner->img))
                <img src="{!! url("/") !!}/{!! $banner->img !!}" class="img-responsive" id="uploadedimage1">
                <input name="img1" type="hidden" value="{!! $banner->img  !!}">
        @endif

        {!! Form::file('img', null, ['class' => 'form-control','id'=>'img', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
        <img id="uploadedimage1" width="100px" />
    </p>
    <p>
        <span id="imageerror1" style="font-weight: bold; color: red"></span>
    </p>
</div>


<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('link', 'Ссылка:') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<!--
<div class="form-group col-sm-6">
    !! Form::label('page_id', 'Page Id:') !!}
    !! Form::number('page_id', null, ['class' => 'form-control']) !!}
</div>
-->

<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('pages.edit', 1) }}" class="btn btn-default">Отмена</a>
</div>
