<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');//return redirect('/ welcome');
});



Auth::routes();


Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index');

    Route::resource('teams', 'TeamController');

    Route::resource('places', 'PlaceController');

    Route::resource('events', 'EventController');
    Route::post('events/get-logo', 'TeamController@getLogo');
    Route::get('events/query', 'EventController@query'); //full-text search ->model, controller
    //Route::get('events/add', 'EventController@add');
    //Route::get('events/delete', 'EventController@delete');

    Route::resource('objects', 'ObjectController');
    Route::get('objects/edit-contacts/{id}', 'ObjectController@editContacts');

    Route::resource('points', 'PointController');
    Route::post('points/change-status', ['uses' => 'PointController@changeStatus']);
    Route::post('points/destroy/{id}', ['uses' => 'PointController@destroy']);

    Route::resource('services', 'ServiceController');

    Route::resource('translates', 'TranslateController');
    Route::resource('variables', 'VariableController');

    Route::resource('docs', 'DocController');

    Route::resource('imgs', 'ImgController');

    Route::resource('floors', 'FloorController');

    Route::resource('transports', 'TransportController');
    Route::post('transports/destroy/{id}', ['uses' => 'TransportController@destroy']);

    Route::resource('hints', 'HintController');

    Route::resource('skyboxes', 'SkyboxController');
    Route::post('skyboxes/destroy/{id}', ['uses' => 'SkyboxController@destroy']);

    Route::resource('tokens', 'TokenController');

    Route::resource('pages', 'PageController');
    //Route::get('pages/create', ['uses' => 'PageController@create']);
    Route::post('pages/destroy/{id}', ['uses' => 'PageController@destroy']);


    Route::resource('awards', 'AwardController');
    Route::post('awards/destroy/{id}', ['uses' => 'AwardController@destroy']);

    Route::resource('partners', 'PartnerController');
    Route::post('partners/destroy/{id}', ['uses' => 'PartnerController@destroy']);

    Route::resource('contacts', 'ContactController');
    Route::post('contacts/destroy/{id}', ['uses' => 'ContactController@destroy']);
    Route::resource('contactTypes', 'ContactTypeController');

    Route::resource('foods', 'FoodController');
    Route::post('foods/destroy/{id}', ['uses' => 'FoodController@destroy']);

    Route::resource('albums', 'AlbumController');
    Route::post('albums/destroy/{id}', ['uses' => 'AlbumController@destroy']);

    Route::resource('photos', 'PhotoController');

    Route::resource('news', 'NewsController');

    Route::resource('questions', 'QuestionController');
    Route::post('questions/destroy/{id}', ['uses' => 'QuestionController@destroy']);

    Route::resource('banners', 'BannerController');
    Route::post('banners/destroy/{id}', ['uses' => 'BannerController@destroy']);


    Route::resource('emails', 'EmailController');

    Route::resource('notifications', 'NotificationController');
    Route::post('notifications/push/{id}', 'NotificationController@push');
});























