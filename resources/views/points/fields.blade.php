


<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название точки:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<div class="form-group col-sm-6">
    {!! Form::label('img_nav', 'Превью:') !!}<br>
    <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">
    @if(!empty($point->img))
        <input name="img3" type="hidden" value="{!! $point->img  !!}">
        @isset($point->img_thumb)
            <input name="img_thumb" type="hidden" value="{!! $point->img_thumb  !!}">
            <img src="{!! url("/") !!}/{!! $point->img_thumb !!}" class="img-responsive" id="uploadedimage1">
        @else
            <img src="{!! url("/") !!}/{!! $point->img !!}" class="img-responsive" id="uploadedimage1">
        @endisset

    @endif

    <img id="uploadedimage1" width="200px" />
    <span id="imageerror1" style="font-weight: bold; color: red"></span>

    {!! Form::file('img', ['id'=>'img_nav', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}

</div>



<!--
<div class="form-group col-sm-6">
    !! Form::label('text', 'Описание:') !!}
!! Form::textarea('text', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-6">
            !! Form::label('position', 'Position:') !!}
            !! Form::number('position', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    !! Form::label('name_en', 'Name En:') !!}
    !! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-12 col-lg-12">
    !! Form::label('text_en', 'Text En:') !!}
    !! Form::textarea('text_en', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    !! Form::label('place_id', 'Place Id:') !!}
    !! Form::number('place_id', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    !! Form::label('floor', 'Floor:') !!}
    !! Form::number('floor', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    !! Form::label('is_active', 'Is Active:') !!}
    !! Form::text('is_active', null, ['class' => 'form-control']) !!}
</div>
-->


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('places.edit',[$point->place_id, 'floor'=>$point->floor_id] ) !!}" class="btn btn-default">Отмена</a>
</div>
