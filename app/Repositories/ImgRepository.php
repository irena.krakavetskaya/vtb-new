<?php

namespace App\Repositories;

use App\Models\Img;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ImgRepository
 * @package App\Repositories
 * @version January 3, 2018, 2:07 pm UTC
 *
 * @method Img findWithoutFail($id, $columns = ['*'])
 * @method Img find($id, $columns = ['*'])
 * @method Img first($columns = ['*'])
*/
class ImgRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'link',
        'object_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Img::class;
    }
}
