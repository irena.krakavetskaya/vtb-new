<div class="col-md-12">

    <fieldset>
        <legend>Описание Арены</legend>

        <!-- Introtext Field -->
        <!--
        <div class="form-group col-sm-6">
            !! Form::label('introtext', 'Заголовок описания:') !!}
            !! Form::text('introtext',$object->introtext, ['class' => 'form-control','maxlength'=>50]) !!}
        </div>
        -->

        <!-- Img Field -->
        <!--
        <div class="form-group col-sm-6">
            !! Form::label('img2', 'Превью:') !!}<br>
            <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
            if(!empty($object->img))
                <input name="img3" type="hidden" value="!! $object->img  !!}">
                <img src="!! url("/") !!}/!! $object->img !!}" class="img-responsive" id="uploadedimage1">
           endif

            <img id="uploadedimage1" width="200px" />
            <span id="imageerror1" style="font-weight: bold; color: red"></span>

            !! Form::file('img', ['id'=>'img2', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}

        </div>-->

        <div class="clearfix"></div>

        <!-- Text Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('text', 'Текст описания:') !!}
            {!! Form::textarea('text', $object->text,  ['class' => 'form-control']) !!} <!--'id'=>'ckeditor1',-->
        </div>

        <!--
        <div class="form-group col-sm-6">
            !! Form::label('phone', 'Номер телефона:') !!}
            !! Form::text('phone', $object->phone,  ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-6">
            !! Form::label('email', 'Email:') !!}
            !! Form::email('email', $object->email,  ['class' => 'form-control']) !!}
        </div>-->

    </fieldset>

    <fieldset>
        <legend>Полезное</legend>

        <!-- Fb Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('usefull', 'Заголовок раздела:') !!}
            {!! Form::text('usefull',$usefull, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-6 toggle-modern">
            <div class="formrow clearfix">
                <label for="is_active">Активный раздел</label>
                <div id="is_active" class="toggle floatright"></div>
                @if(isset($object->is_active))
                    @if($object->is_active=='true')
                        <input type="text"  class="checkbx" name="is_active" value="{!! $object->is_active !!}">
                    @else
                        <input type="text"  class="checkbx" name="is_active"  value="false">
                    @endif
                @else
                    <input   class="text" name="is_active"  value="false">
                @endif

            </div>
        </div>
        <div class="clearfix"></div>



        @foreach($docs as $doc)
            <div class="tr-pdf">
                <div class="form-group col-sm-6">
                    {!! Form::text('name[]', $doc->name, ['class' => 'form-control','id'=>'name_'.$doc->id,'required'=>'required','placeholder'=>'Имя файла']) !!}
                </div>
                <div class="form-group col-sm-3 pdf">
                    <span class="link_name">{!! $doc->link !!}</span>
                </div>
                <div class="form-group col-sm-3 pdf-btn">
                    <div class='btn-group'>

                        <div class="file-input">
                            <input name="ids[]" type="hidden" value="{!!$doc->id  !!}" class="ids">
                            {!! Form::file('link[]',
                                ['id'=>'link_'.$doc->id, 'accept'=>'application/pdf','class'=>'inputfile','data-multiple-caption'=>'{count} files selected']) !!}
                            <label for="link_{!!$doc->id!!}">Загрузить файл</label>

                        </div>

                        <a href="#" class='btn btn-danger btn-sm'>
                            Удалить
                        </a>

                    </div>
                </div>
            </div>
        @endforeach

        <!--<div class="clearfix"></div>-->


        <div class="form-group col-sm-6">
            <a href="#" class='btn btn-primary btn-sm add-pdf'>
                Добавить еще файл
            </a>
        </div>
    </fieldset>

    <fieldset>
        <legend>Соцсети</legend>
        <!-- Fb Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('fb', 'Facebook:') !!}
            {!! Form::text('fb',$object->fb,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <!-- Inst Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('inst', 'Instagram:') !!}
            {!! Form::text('inst',$object->inst, ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <!-- Vk Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('vk', 'ВКонтакте:') !!}
            {!! Form::text('vk', $object->vk,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <!-- Youtube Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('youtube', 'YouTube:') !!}
            {!! Form::text('youtube', $object->youtube,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

    </fieldset>

</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('objects.edit',1) !!}" class="btn btn-default">Отмена</a>
</div>
