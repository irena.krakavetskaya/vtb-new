* Эксукрсии

===================================


Запрос:

    GET /api/v1/services/2


Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 {
                 "id": "2",
                 "name": "Экскурсии",
                 "phone": "+7 (495) 968-22-99",
                 "pdf": [
                     {
                         "name": "Цены",
                         "link": "pdf/Цены.pdf"
                     }
                 ],
                 "events": [
                         {
                             "id": 15,
                             "type": "excursion",
                             "time": "18:00",
                             "kind": "Конференция",
                             "name": "МЕЖДУНАРОДНАЯ КОНФЕРЕНЦИЯ ФХР",
                             "img": "img_show/QfinnPakFQzK4BTrwWr4406OqPs89QvLcJuxiXwA.jpeg",
                             "dateEvent": "2018-03-15",
                             "place": "Музей Хоккея"
                         },
                         {
                             "id": 28,
                             "type": "excursion",
                             "time": "17:00",
                             "kind": "Экскурсия",
                             "name": "Экскурсия по музею и ледовому дворцу",
                             "img": "img_show/83iagllAiuNHahrXW1ZL5fotry58Fga15puIJxTm.jpeg",
                             "dateEvent": "2018-03-19",
                             "place": "Большая арена"
                         },
                         {
                             "id": 31,
                             "type": "excursion",
                             "time": "00:00",
                             "kind": "Экскурсия",
                             "name": "Экскурсия перед матчем",
                             "img": "img_show/QK0DFL0ZwBrIAaqJMVtmhUvjh2UGPKYgtUcfsxiv.jpeg",
                             "dateEvent": "2018-03-30",
                             "place": "Музей Хоккея"
                         }
                 ]
             }