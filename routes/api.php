<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1/'], function () {


    Route::get('events', ['startIndex' => '{startIndex?}', 'endIndex' => '{endIndex?}', 'uses' => 'EventAPIController@index']);//01
    Route::get('events-filter',
        ['year'=>'{year?}','month'=>'{month?}','day'=>'{day?}','category'=>'{category?}','startIndex' => '{startIndex?}', 'endIndex' => '{endIndex?}','uses' => 'EventAPIController@showPlace']);//02
    //'place'=>'{place}',
    Route::get('get-events/{id}', ['uses' => 'EventAPIController@show']); //03

    Route::get('advertisement', ['uses' => 'EventAPIController@showAd']); //07

    //Route::get('places', ['uses' => 'PlaceAPIController@index']);
    Route::get('places/{id}/{floor?}', ['uses' => 'PlaceAPIController@show']); //18

    Route::get('variables', 'VariableAPIController@index'); //05

    //is not used
    //Route::get('translates', 'TranslateAPIController@index');//
    //Route::get('menu', ['uses' => 'TranslateAPIController@showMenu']);//
    //Route::get('lang', ['uses' => 'TranslateAPIController@showLang']);//

    Route::get('objects/{id}', ['uses' => 'ObjectAPIController@show']);//+
    Route::get('get-contacts', ['uses' => 'PageAPIController@getContacts']);//+

    //Route::get('prices/{id}', ['uses' => 'PriceAPIController@show']); //09
    //Route::get('halls/{id}', ['uses' => 'HallAPIController@show']); //10


    Route::resource('services', 'ServiceAPIController');// 15, 21, 22, 23  ??? - convert service to page, delete add fields from excursions  +/-

    Route::resource('transports', 'TransportAPIController'); //+

    Route::resource('hints', 'HintAPIController'); //+

    //Route::get('rink/schedule', ['startIndex' => '{startIndex?}', 'endIndex' => '{endIndex?}', 'uses' => 'RinkScheduleAPIController@index']);//24

    Route::resource('tokens', 'TokenAPIController'); //+

});

//Route::get('rink_events/{id}', ['uses' => 'RinkEventAPIController@show']); //is not used

//Route::resource('points', 'PointAPIController');
//Route::resource('floors', 'FloorAPIController');

//Route::resource('skyboxes', 'SkyboxAPIController');

//Route::resource('view_types', 'ViewTypeAPIController');

//Route::resource('weeks', 'WeekAPIController');
//Route::resource('templates', 'TemplateAPIController');
//Route::resource('days', 'DayAPIController');
//Route::resource('rink_schedules', 'RinkScheduleAPIController');


//Route::resource('translates', 'TranslateAPIController');
//Route::resource('variables', 'VariableAPIController');

//Route::resource('objects', 'ObjectAPIController');

//Route::resource('docs', 'DocAPIController');

//Route::resource('schedules', 'ScheduleAPIController');

//Route::resource('imgs', 'ImgAPIController');

//Route::resource('prices', 'PriceAPIController');

//Route::resource('halls', 'HallAPIController');

//Route::resource('exceptions', 'ExceptionAPIController');

//Route::resource('rink_events', 'RinkEventAPIController');

//Route::resource('pages', 'PageAPIController');

/*
Route::resource('awards', 'AwardAPIController');

Route::resource('partners', 'PartnerAPIController');

Route::resource('contacts', 'ContactAPIController');
Route::resource('contact_types', 'ContactTypeAPIController');

Route::resource('foods', 'FoodAPIController');

Route::resource('albums', 'AlbumAPIController');

Route::resource('photos', 'PhotoAPIController');

Route::resource('news', 'NewsAPIController');

Route::resource('questions', 'QuestionAPIController');

Route::resource('banners', 'BannerAPIController');

Route::resource('emails', 'EmailAPIController');

Route::resource('notifications', 'NotificationAPIController');
*/