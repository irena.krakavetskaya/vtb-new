@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Питание
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'foods.store','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('foods.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
