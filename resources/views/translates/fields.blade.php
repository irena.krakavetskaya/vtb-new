<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Front Field -->
<div class="form-group col-sm-6">
    {!! Form::label('front', 'Front:') !!}
    {!! Form::text('front', null, ['class' => 'form-control']) !!}
</div>

<!-- Ru Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ru', 'Ru:') !!}
    {!! Form::text('ru', null, ['class' => 'form-control']) !!}
</div>

<!-- En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('en', 'En:') !!}
    {!! Form::text('en', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('translates.index') !!}" class="btn btn-default">Cancel</a>
</div>
