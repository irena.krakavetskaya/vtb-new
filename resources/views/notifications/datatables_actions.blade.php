{!! Form::open(['route' => ['notifications.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <!--<a href="{ route('notifications.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>-->
    <a href="{{ route('notifications.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Вы уверены?')"
    ]) !!}
</div>
{!! Form::close() !!}

<form method="POST" action="{{ url("/notifications/push/".$id) }}" accept-charset="UTF-8" id="push">
    @if($type=='push')
        {!!Form::button('Отправить пуш', [
            'id' => 'sendPush1_'.$id,
            'type' => 'submit',
            'name'=>'push',
            'class' => 'btn btn-danger btn-xs',
         ]) !!}
    @endif
    {{ Form::token() }}
</form>

<script>

    $('#dataTableBuilder tbody').on('click', '#sendPush1_{{$id}}', function (event) {
        event.preventDefault();
        if(confirm('Вы уверены, что хотите отпраить пуш-уведомления всем пользователям?')==true){
            $.blockUI({
                message: "<h3>Происходит рассылка пушей, пожалуйста подождите...</h3>",
                timeout: 1000000,
                onUnBlock:$(this).parent("#push").submit()
            });
        }
        //return true;
    });

</script>