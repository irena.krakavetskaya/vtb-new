<?php

namespace App\Http\Controllers;

use App\DataTables\FloorDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateFloorRequest;
use App\Http\Requests\UpdateFloorRequest;
use App\Repositories\FloorRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

class FloorController extends AppBaseController
{
    /** @var  FloorRepository */
    private $floorRepository;

    public function __construct(FloorRepository $floorRepo)
    {
        $this->floorRepository = $floorRepo;
    }

    /**
     * Display a listing of the Floor.
     *
     * @param FloorDataTable $floorDataTable
     * @return Response
     */
    public function index(FloorDataTable $floorDataTable)
    {
        return $floorDataTable->render('floors.index');
    }

    /**
     * Show the form for creating a new Floor.
     *
     * @return Response
     */
    public function create()
    {
        return view('floors.create');
    }

    /**
     * Store a newly created Floor in storage.
     *
     * @param CreateFloorRequest $request
     *
     * @return Response
     */
    public function store(CreateFloorRequest $request)
    {
        $input = $request->all();

        $floor = $this->floorRepository->create($input);

        Flash::success('Floor saved successfully.');

        return redirect(route('floors.index'));
    }

    /**
     * Display the specified Floor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $floor = $this->floorRepository->findWithoutFail($id);

        if (empty($floor)) {
            Flash::error('Floor not found');

            return redirect(route('floors.index'));
        }

        return view('floors.show')->with('floor', $floor);
    }

    /**
     * Show the form for editing the specified Floor.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $floor = $this->floorRepository->findWithoutFail($id);

        if (empty($floor)) {
            Flash::error('Floor not found');

            return redirect(route('floors.index'));
        }

        return view('floors.edit')->with('floor', $floor);
    }

    /**
     * Update the specified Floor in storage.
     *
     * @param  int              $id
     * @param UpdateFloorRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFloorRequest $request)
    {
        $floor = $this->floorRepository->findWithoutFail($id);

        if (empty($floor)) {
            Flash::error('Floor not found');
            return redirect(route('floors.index'));
        }

        //add preview
        $file1 = $request->file('img1');
        $file1_existed = $request->input('img3');

        $file2 = $request->file('img2');
        $file2_existed = $request->input('img4');

        if(isset($file1)) {
            $img1 = $file1->store('img_navigation/'.$floor->place_id);
        }
        else{
            $img1 =$file1_existed?$file1_existed:'';
        }
        if(isset($file2)) {
            $img2 = $file2->store('img_navigation/'.$floor->place_id);
        }
        else{
            $img2 =$file2_existed?$file2_existed:'';
        }
        //end of add preview


        $requestData=$request->all();
        //return $img;
        $requestData['img1'] = $img1;
        $requestData['img2'] = $img2;

        $floor = $this->floorRepository->update($requestData, $id);


        Flash::success('Информация об этаже успешно сохранена.');
        return redirect(route('places.edit',[$floor->place_id, 'floor'=>$id]));

        //return redirect(route('floors.index'));
    }

    /**
     * Remove the specified Floor from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $floor = $this->floorRepository->findWithoutFail($id);

        if (empty($floor)) {
            Flash::error('Floor not found');

            return redirect(route('floors.index'));
        }

        $this->floorRepository->delete($id);

        Flash::success('Floor deleted successfully.');

        return redirect(route('floors.index'));
    }
}
