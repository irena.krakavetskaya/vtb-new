<div class="col-md-12">

    <fieldset>
        <legend>Описание Музея</legend>

        <!-- Introtext Field -->
        <!--<div class="form-group col-sm-6">
            !! Form::label('introtext', 'Заголовок описания:') !!}
            !! Form::text('introtext',$object->introtext, ['class' => 'form-control','maxlength'=>50]) !!}
        </div>-->

        <!-- main img-->
        <!-- <div class="form-group col-sm-6">
            !! Form::label('img2', 'Превью:') !!}<br>
            <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
            if(!empty($object->img))
                <input name="img3" type="hidden" value="!! $object->img  !!}">
                <img src="!! url("/") !!}/!! $object->img !!}" class="img-responsive" id="uploadedimage1">
           endif

            <img id="uploadedimage1" width="200px" />
            <span id="imageerror1" style="font-weight: bold; color: red"></span>

            !! Form::file('img', ['id'=>'img2', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}

        </div>-->

        <!-- Text Field -->
        <div class="form-group col-sm-12">
            {!! Form::label('text', 'Текст описания:') !!}
            {!! Form::textarea('text', $object->text,  ['class' => 'form-control']) !!}<!--'id'=>'ckeditor1', ,'maxlength'=>500]-->
        </div>

        <!--in contacts
        <div class="form-group col-sm-6">
            !! Form::label('address', 'Адрес:') !!}
            !! Form::text('address', $object->address,  ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-6">
            !! Form::label('phone', 'Номер телефона:') !!}
            !! Form::text('phone', $object->phone,  ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-6">
            !! Form::label('email', 'Email:') !!}
            !! Form::email('email', $object->email,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <div class="form-group col-sm-6">
            !! Form::label('open_hours', 'График работы:') !!}
            !! Form::textarea('open_hours', $object->open_hours,  ['class' => 'form-control']) !!}
        </div>
        -->

    </fieldset>

    <fieldset class="schedule">
        <legend>График работы</legend>

        @foreach($schedule as $sch)
            <input type="hidden" name="sch_id[]" value="{!!$sch->id!!}">
            <div class="form-group col-lg-1 col-md-1">
                {!!$sch->day!!}
            </div>

            <div class="form-group  toggle-modern no-padding col-lg-2 col-md-2">
                <div class="formrow clearfix">
                    <div class="toggle1_{!! $sch->id !!} floatright "></div>
                    @if(isset($sch->day_off))
                        @if($sch->day_off==='true')
                            <input type="text"   class="checkbx day_off" name="day_off[]" value="{!! $sch->day_off !!}">
                        @else
                            <input type="text"   class="checkbx day_off" name="day_off[]"  value="false">
                        @endif
                    @else
                        <input   class="text day_off" name="day_off[]"  value="false">
                    @endif
                </div>
            </div>

            <div class="form-group col-lg-2 col-md-2">
                <div class="form-group row">
                    {!! Form::label('work_from', 'Работает с:',  ['class' => 'col-sm-6 col-form-label  no-padding']) !!}
                    <div class="col-sm-6" style="padding:0 5px">
                        {!! Form::time('work_from[]', $sch->work_from,  ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="form-group col-lg-2 col-md-2">
                <div class="form-group row">
                    {!! Form::label('work_till', 'по:',  ['class' => 'col-sm-4 col-form-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::time('work_till[]', $sch->work_till,  ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>

            <div class="form-group  text-right col-lg-2 col-md-2">
                <div class="form-check">
                    <input type="hidden" name="break[]" value="{!! $sch->break !!}">
                    @if($sch->break==='true')
                        {!! Form::checkbox('breaks', $sch->break,  ['class' => 'form-check-input','checked'=>'checked']) !!}
                    @else
                        {!! Form::checkbox('breaks', $sch->break,  []) !!}
                    @endif
                    {!! Form::label('break', 'Перерыв с:',['class'=>'form-check-label']) !!}
                </div>
            </div>

            <div class="form-group  no-padding col-lg-1 col-md-1">
                    {!! Form::time('break_from[]', $sch->break_from,  ['class' => 'form-control']) !!}
            </div>

            <div class="form-group col-lg-2 col-md-2">
                <div class="form-group row">
                    {!! Form::label('break_till', 'по:',  ['class' => 'col-sm-4 col-form-label']) !!}
                    <div class="col-sm-8">
                        {!! Form::time('break_till[]', $sch->break_till,  ['class' => 'form-control']) !!}
                    </div>
                </div>
           </div>
            <div class="clearfix"></div>
        @endforeach

        <a href="#" class='btn btn-danger btn-sm'><!--!! route('exceptions.index') target="_blank" !!}-->
           Исключения
        </a>

    </fieldset>

    <fieldset class="gallery">
        <legend>Галерея</legend>
        <div class="form-group col-md-12">

            @foreach($imgs as $img)
                <div class="col-sm-2">
                    <p>
                        <input name="img_museum[]" type="hidden" value="{!!  $img->link  !!}">
                        <a href="/{!! $img->link !!}" data-toggle="lightbox">
                            <img src="/{!! $img->link !!}" class="img-responsive">
                        </a>
                    </p>
                    <p class="text-center">
                        <input name="del_img[]" type="hidden" value="{!! $img->id !!}" class="del_img">
                        <a href="#" class='btn btn-danger btn-sm delete-img' id="{!! $img->id !!}">
                            Удалить
                        </a>
                    </p>
                </div>
            @endforeach

            <div class="col-sm-2">

                <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
                <img id="uploadedimage_museum" height="160px" class="uploadedimage_museum">
                <span id="imageerror_museum" style="font-weight: bold; color: red"></span>
                {!! Form::file('img_museum[]', ['id'=>'img_museum', 'accept'=>'image/jpg,image/jpeg,image/png', 'class'=>'img_museum']) !!}

            </div>
        </div>
    </fieldset>

    <fieldset class="pr">
        <legend>Цены</legend>

        @foreach($prices as $price)
            <div class="price-row">
                <div class="col-md-2">
                    <input name="id_pr[]" type="hidden" value="{!!$price->id  !!}" class="id_pr">
                    {!! $price->name !!}
                </div>
                <div class="col-md-1">
                    {!! $price->price !!} р.
                </div>
                <div class="col-md-5 text">
                    {!! $price->text !!}
                </div>
                <div class="col-md-2 toggle-modern">
                    <div class="formrow clearfix">
                        <div id="is_active" class="toggle2_{!! $price->id !!} floatright pull-right"></div>
                        @if(isset($price->is_active))
                            @if($price->is_active=='true')
                                <input type="text"  class="checkbx price" name="is_shown[]" value="{!! $price->is_active !!}">
                            @else
                                <input type="text"  class="checkbx price" name="is_shown[]"  value="false">
                            @endif
                        @else
                            <input  type="text" class="checkbx price" name="is_shown[]"  value="true">
                        @endif
                    </div>
                </div>

                <div class='btn-group col-md-2'>
                    <a href="{{ route('prices.edit', $price->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs delete_price',
                        //'onclick' => "return confirm('Вы уверены?')",
                        'id'=>$price->id
                    ]) !!}
                </div>
            </div>
            <div class="clearfix"></div>
        @endforeach


        <div class="form-group col-sm-6">
            <a href="{!! route('prices.create') !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
                Добавить тип билета
            </a>
        </div>
    </fieldset>


    <fieldset class="hall">
        <legend>Описание залов</legend>

        @foreach($halls as $hall)
            <div>
                <div class="col-md-3">
                    <input name="id_hall[]" type="hidden" value="{!!$hall->id  !!}" class="id_hall">
                    {!! $hall->name !!}
                </div>
                <div class="col-md-7 text">
                    {!! $hall->text !!}
                </div>

                <div class='btn-group col-md-2'>
                    <a href="{{ route('halls.edit', $hall->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                        <i class="glyphicon glyphicon-edit"></i>
                    </a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs delete_hall',
                        //'onclick' => "return confirm('Вы уверены?')",
                        'id'=>$hall->id
                    ]) !!}
                </div>

                <div class="clearfix"></div>
            </div>
        @endforeach


        <div class="form-group col-sm-6">
            <a href="{!! route('halls.create') !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
                Добавить зал
            </a>
        </div>
    </fieldset>

    <!--
    <fieldset class="video">
        <legend>Видео</legend>
        <div class="form-group col-md-12">
            <div class="form-group col-sm-6">
                !! Form::label('video', 'Ссылка на ролик в YouTube:') !!}
                !! Form::text('video', $object->video,  ['class' => 'form-control']) !!}
            </div>
        </div>
    </fieldset>
    -->

    <fieldset>
        <legend>Полезное</legend>
        <!-- Fb Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('usefull', 'Заголовок раздела:') !!}
            {!! Form::text('usefull', $usefull, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-6 toggle-modern">
            <div class="formrow clearfix">
                <label for="is_active">Активный раздел</label>
                <div id="is_active" class="toggle floatright"></div>
                @if(isset($object->is_active))
                    @if($object->is_active=='true')
                        <input type="text"  class="checkbx" name="is_active" value="{!! $object->is_active !!}">
                    @else
                        <input type="text"  class="checkbx" name="is_active"  value="false">
                    @endif
                @else
                    <input   class="text" name="is_active"  value="false">
                @endif
            </div>
        </div>
        <div class="clearfix"></div>


        @foreach($docs as $doc)
            <div class="tr-pdf">
                <div class="form-group col-sm-6">
                    {!! Form::text('name[]', $doc->name, ['class' => 'form-control','id'=>'name_'.$doc->id,'required'=>'required','placeholder'=>'Имя файла']) !!}
                </div>
                <div class="form-group col-sm-3 pdf">
                    <span class="link_name">{!! $doc->link !!}</span>
                </div>
                <div class="form-group col-sm-3 pdf-btn">
                    <div class='btn-group'>

                        <div class="file-input">
                            <input name="ids[]" type="hidden" value="{!!$doc->id  !!}" class="ids">
                            {!! Form::file('link[]',
                                ['id'=>'link_'.$doc->id, 'accept'=>'application/pdf','class'=>'inputfile','data-multiple-caption'=>'{count} files selected']) !!}
                            <label for="link_{!!$doc->id!!}">Загрузить файл</label>
                        </div>

                        <a href="#" class='btn btn-danger btn-sm'>
                            Удалить
                        </a>

                    </div>
                </div>
            </div>
        @endforeach

        <div class="form-group col-sm-6">
            <a href="#" class='btn btn-primary btn-sm add-pdf'>
                Добавить еще файл
            </a>
        </div>
    </fieldset>

    <fieldset>
        <legend>Соцсети</legend>
        <!-- Fb Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('fb', 'Facebook:') !!}
            {!! Form::text('fb',$object->fb,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <!-- Inst Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('inst', 'Instagram:') !!}
            {!! Form::text('inst',$object->inst, ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <!-- Vk Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('vk', 'ВКонтакте:') !!}
            {!! Form::text('vk', $object->vk,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <!-- Youtube Field -->
        <div class="form-group col-sm-6">
            {!! Form::label('youtube', 'YouTube:') !!}
            {!! Form::text('youtube', $object->youtube,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

    </fieldset>

    <!-- Submit Field -->
    <div class="form-group col-sm-12">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        <a href="{!! route('objects.edit',2) !!}" class="btn btn-default">Отмена</a>
    </div>


</div>



