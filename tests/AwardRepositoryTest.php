<?php

use App\Models\Award;
use App\Repositories\AwardRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AwardRepositoryTest extends TestCase
{
    use MakeAwardTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AwardRepository
     */
    protected $awardRepo;

    public function setUp()
    {
        parent::setUp();
        $this->awardRepo = App::make(AwardRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAward()
    {
        $award = $this->fakeAwardData();
        $createdAward = $this->awardRepo->create($award);
        $createdAward = $createdAward->toArray();
        $this->assertArrayHasKey('id', $createdAward);
        $this->assertNotNull($createdAward['id'], 'Created Award must have id specified');
        $this->assertNotNull(Award::find($createdAward['id']), 'Award with given id must be in DB');
        $this->assertModelData($award, $createdAward);
    }

    /**
     * @test read
     */
    public function testReadAward()
    {
        $award = $this->makeAward();
        $dbAward = $this->awardRepo->find($award->id);
        $dbAward = $dbAward->toArray();
        $this->assertModelData($award->toArray(), $dbAward);
    }

    /**
     * @test update
     */
    public function testUpdateAward()
    {
        $award = $this->makeAward();
        $fakeAward = $this->fakeAwardData();
        $updatedAward = $this->awardRepo->update($fakeAward, $award->id);
        $this->assertModelData($fakeAward, $updatedAward->toArray());
        $dbAward = $this->awardRepo->find($award->id);
        $this->assertModelData($fakeAward, $dbAward->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAward()
    {
        $award = $this->makeAward();
        $resp = $this->awardRepo->delete($award->id);
        $this->assertTrue($resp);
        $this->assertNull(Award::find($award->id), 'Award should not exist in DB');
    }
}
