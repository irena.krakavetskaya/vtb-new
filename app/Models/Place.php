<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Place
 * @package App\Models
 * @version December 19, 2017, 8:20 am UTC
 *
 * @property string name
 * @property string name_en
 */
class Place extends Model
{
    //use SoftDeletes;

    public $table = 'places';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'name_en',
        'text',
        'text_en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_en' => 'string',
        'text' => 'string',
        'text_en' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'name' => 'required'
    ];

    public $timestamps = false;

    public function events()
    {
        return $this->hasMany('App\Models\Event', 'place_id');
    }

    public function points()
    {
        return $this->hasMany('App\Models\Point', 'place_id');
    }

    public function floors()
    {
        return $this->hasMany('App\Models\Floor', 'place_id');
    }
}
