<?php

use Faker\Factory as Faker;
use App\Models\Partner;
use App\Repositories\PartnerRepository;

trait MakePartnerTrait
{
    /**
     * Create fake instance of Partner and save it in database
     *
     * @param array $partnerFields
     * @return Partner
     */
    public function makePartner($partnerFields = [])
    {
        /** @var PartnerRepository $partnerRepo */
        $partnerRepo = App::make(PartnerRepository::class);
        $theme = $this->fakePartnerData($partnerFields);
        return $partnerRepo->create($theme);
    }

    /**
     * Get fake instance of Partner
     *
     * @param array $partnerFields
     * @return Partner
     */
    public function fakePartner($partnerFields = [])
    {
        return new Partner($this->fakePartnerData($partnerFields));
    }

    /**
     * Get fake data of Partner
     *
     * @param array $postFields
     * @return array
     */
    public function fakePartnerData($partnerFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'logo' => $fake->text,
            'site' => $fake->text
        ], $partnerFields);
    }
}
