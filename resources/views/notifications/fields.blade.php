<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Заголовок:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Текст:') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>
<div class="clearfix"></div>

<!-- Push Datetime Field -->
<div class="form-group col-sm-6">
    {!! Form::label('push_date', 'Дата отправки:') !!}
    @isset($date)
        {!! Form::date('push_date', $date, ['class' => 'form-control']) !!}
    @else
        {!! Form::date('push_date', null, ['class' => 'form-control']) !!}
    @endisset
</div>

<div class="form-group col-sm-6">
    {!! Form::label('push_time', 'Время отправки:') !!}
    @isset($time)
        {!! Form::time('push_time', $time, ['class' => 'form-control']) !!}
    @else
        {!! Form::time('push_time', null, ['class' => 'form-control']) !!}
    @endisset
</div>

<!-- Event Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('event_id', 'Событие Афиши:') !!}
    <select id="event_id" name="event_id" class="form-control">
        <option value="">Выберите событие </option>
        @isset($notification->event_id))
            @foreach ($events as $key=>$value)
                @if($value->id == $notification->event_id)
                    <option value="{!! $value->id !!}" selected>
                        @isset($value->name)
                            {!! $value->name !!}
                        @else
                            {!! date('d-m-Y', strtotime($value->date)) !!} - {!! $value->type !!}
                        @endisset
                    </option>
                @else
                    <option value="{!! $value->id !!}">
                        @isset($value->name)
                            {!! $value->name !!}
                        @else
                            {!! date('d-m-Y', strtotime($value->date)) !!} - {!! $value->type !!}
                        @endisset
                    </option>
                @endif
            @endforeach
        @else
            @foreach ($events as $key=>$value)
                <option value="{!! $value->id !!}">
                    @isset($value->name)
                        {!! $value->name !!}
                    @else
                        {!! date('d-m-Y', strtotime($value->date)) !!} - {!! $value->type !!}
                    @endisset
                </option>
            @endforeach
        @endisset
    </select>
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Тип:') !!}
    <select id="type" name="type" class="form-control">
        @if(isset($notification->type))
            @foreach ($types as $key=>$value)
                @if($key == $notification->type)
                    <option value="{!! $key !!}" selected>
                            {!! $value !!}
                    </option>
                @else
                    <option value="{!! $key !!}">
                            {!! $value !!}
                    </option>
                @endif
            @endforeach
        @else
            @foreach ($types as $key=>$value)
                <option value="{!! $key!!}">
                        {!! $value !!}
                </option>
            @endforeach
        @endif
    </select>
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('notifications.index') !!}" class="btn btn-default">Отмена</a>
</div>
