<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('name_en', 'Название (English):') !!}
    {!! Form::text('name_en', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-12 col-lg-12">
    <p>
        {!! Form::label('img', 'Изображение (рекоменд. 150*150 px):') !!}
        <input type="hidden" name="MAX_UPLOAD_SIZE" value="10000000">

        @if(!empty($food->img))
            <img src="{!! url("/") !!}/{!! $food->img !!}" class="img-responsive" id="uploadedimage1">
            <input name="img1" type="hidden" value="{!! $food->img  !!}">
        @endif

        {!! Form::file('img', null, ['class' => 'form-control','id'=>'img', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
        <img id="uploadedimage1" width="100px" />
    </p>
    <p>
        <span id="imageerror1" style="font-weight: bold; color: red"></span>
    </p>
</div>

<!-- Text Field -->
<div class="form-group col-sm-6">
    {!! Form::label('text', 'Описание:') !!}
    {!! Form::textarea('text', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('text_en', 'Описание (English):') !!}
    {!! Form::textarea('text_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('pages.edit', 20) !!}" class="btn btn-default">Отмена</a>
</div>
