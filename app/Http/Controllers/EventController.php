<?php

namespace App\Http\Controllers;

use App\DataTables\EventDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Models\Event;
use App\Models\EventType;
use App\Models\Place;
use App\Models\Team;
use App\Repositories\EventRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use Illuminate\Http\Request;
use App\DataTables\TeamDataTable;

class EventController extends AppBaseController
{
    /** @var  EventRepository */
    private $eventRepository;

    public function __construct(EventRepository $eventRepo)
    {
        $this->eventRepository = $eventRepo;
    }

    public function query()
    {
        // queries to Algolia search index and returns matched records as Eloquent Models
        $events = Event::search('title')->get();

        // do the usual stuff here
        foreach ($events as $ev) {
            return $ev;
        }
    }

    /*
    public function add()
    {
        // this events should be indexed at Algolia right away!
        $events = new Event();
        $events->setAttribute('name', 'Another Post');
        $events->setAttribute('type', 'show');
        $events->setAttribute('kind', 'Концерт');
        $events->save();
    }

    public function delete()
    {
        // this events should be removed from the index at Algolia right away!
        $post = Event::find(1);
        $post->delete();
    }
    */


    /**
     * Display a listing of the Event.
     *
     * @param EventDataTable $eventDataTable
     * @return Response
     */
    /*public function index(EventDataTable $eventDataTable)
    {
        return $eventDataTable->render('events.index');
    }*/

    public function index(Request $request,eventDataTable $eventDataTable)
    {
        isset($request->type)?$type=$request->type:$type='all';
        isset($request->past)?$past=$request->past:$past='hide';
        isset($request->date_from)?$date_from=$request->date_from:$date_from='';
        isset($request->date_till)?$date_till=$request->date_till:$date_till='';

        return $eventDataTable->forCategory1($type,$past,$date_from,$date_till)->render('events.index',
            ["type" => $type,"past" => $past,"date_from" => $date_from,"date_till" =>$date_till]
        );
    }

    /**
     * Show the form for creating a new Event.
     *
     * @return Response
     */
    public function create()
    {

        $places = Place::whereIn('id',[1,2,3])->get();
        $teams = Team::get();

        return view('events.create')->with('places', $places)->with('teams', $teams);
    }

    /**
     * Store a newly created Event in storage.
     *
     * @param CreateEventRequest $request
     *
     * @return Response
     */
    public function store(CreateEventRequest $request)
    {
        $requestData = $request->all();


        $this->validate($request,[
            'type' => 'required'
        ]);

        if($request->type=='match'){
            $this->validate($request, [
                'team1_id' => 'required',
                'team2_id' => 'required',
            ]);
            $requestData['kind'] = $request->league;
            $requestData['kind_en'] = $request->league_en;

        }
        if($request->type=='show' || $request->type=='excursion'){
            $this->validate($request, [
                'name' => 'required',
                'kind' => 'required',
            ]);
        }

        if($request->is_adv=='true'){
            $this->validate($request, [
                'img' => 'required',
            ]);
        }

        $file = $request->file('img');
        if(isset($file)) {
            $img = $file->store('img_show');
            //$event = new Event();
            //$img = $event->formatImg($file, 'img_show', 375, 200);
        }
        else{
           $img ='';
        }

        $requestData['img'] = $img;

        switch($request->place_id){
            case 1: $object_id=1;break;
            case 2: $object_id=1;break;
            case 3: $object_id=2;break;
            //case 4: $object_id=3;break;
        }
        $requestData['object_id'] = $object_id;


        $num_important_1 = Event::where('is_important','true')
            ->whereIn('place_id',[1,2])
            ->count();
        $num_important_2 = Event::where('is_important','true')
            ->where('place_id',3)
            ->count();
        if($request->is_important=='true'){
            $places = Place::whereIn('id',[1,2,3])->get();
            $teams = Team::get();
            if($num_important_1>=3 && ($request->place_id ==1 || $request->place_id ==2) ){
                return view('events.create')->with('places', $places)->with('teams', $teams)
                    ->withErrors('Вы не можете добавить более 3 событий на главную страницу Ледового дворца');
            }
            /*if($num_important_2>=3 && $request->place_id ==3 ){
                return view('events.create')->with('places', $places)->with('teams', $teams)
                    ->withErrors('Вы не можете добавить более 3 важных событий');
            }*/
            if($request->place_id!=1 && $request->place_id!=2){// && $request->place_id!=3
                return view('events.create')
                    ->with('places', $places)
                    ->with('teams', $teams)
                    ->withErrors('Вы можете добавить важное событие только для Ледового дворца');
            }
        }


        $event = $this->eventRepository->create($requestData);
        $event->searchable();

        Flash::success('Событие успешно добавлено.');

        return redirect(route('events.index'));
    }

    /**
     * Display the specified Event.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $event = $this->eventRepository->findWithoutFail($id);

        $places = Place::get();
        $teams = Team::get();


        if (empty($event)) {
            Flash::error('Событие не найдено');

            return redirect(route('events.index'));
        }

        return view('events.show')->with('event', $event)->with('places', $places)->with('teams', $teams);
    }

    /**
     * Show the form for editing the specified Event.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id, TeamDataTable $teamDataTable)
    {
        $event = $this->eventRepository->findWithoutFail($id);

        $places = Place::whereIn('id',[1,2,3])->get();
        $teams = Team::get();

        if (empty($event)) {
            Flash::error('Событие не найдено');
            return redirect(route('events.index'));
        }

        return view('events.edit')->with('event', $event)->with('places', $places)->with('teams', $teams);

        //return $teamDataTable->render('events.edit')->with('event', $event)->with('places', $places)->with('teams', $teams);
        //return $teamDataTable->render('teams.index')->with('event', $event)->with('places', $places)->with('teams', $teams);
    }

    /**
     * Update the specified Event in storage.
     *
     * @param  int              $id
     * @param UpdateEventRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEventRequest $request)
    {
        $event = $this->eventRepository->findWithoutFail($id);
        $places = Place::whereIn('id',[1,2,3])->get();
        $teams = Team::get();

        if($request->type=='match'){
            $this->validate($request, [
                'team1_id' => 'required',
                'team2_id' => 'required',
            ]);
        }

        if (empty($event)) {
            Flash::error('Событие не найдено');
            return redirect(route('events.index'));
        }

        $file = $request->file('img');
        $file1 = $request->input('img1');

        if($request->is_adv=='true' &&(!isset($file) && !isset($file1))){
            return view('events.edit')->with('event', $event)->with('places', $places)->with('teams', $teams)
                ->withErrors('Поле Изображение обязательно для заполнения.');
        }


        if(isset($file)) {
            $img = $file->store('img_show');
            //$img = $event->formatImg($file, 'img_show', 375, 200);
        }
        else{
            $img =$file1?$file1:'';
            //$img ='';
        }
        $requestData = $request->all();
        $requestData['img'] = $img;
        if(empty($request->is_important)){
            $requestData['is_important'] ='false';
        };
        if(empty($request->is_adv)){
            $requestData['is_adv'] ='false';
        };

        switch($request->place_id){
            case 1: $object_id=1;break;
            case 2: $object_id=1;break;
            case 3: $object_id=2;break;
            case 4: $object_id=3;break;
        }
        $requestData['object_id'] = $object_id;


        $num_important_1 = Event::where('is_important','true')
            ->whereIn('place_id',[1,2])
            ->count();
        $num_important_2 = Event::where('is_important','true')
            ->where('place_id',3)
            ->count();
        $old_is_important=$event->is_important;
        if($request->is_important=='true'){
            if($num_important_1>=3 && ($request->place_id ==1 || $request->place_id ==2) && $old_is_important!=='true'){
                return view('events.edit')->with('event', $event)->with('places', $places)->with('teams', $teams)
                    ->withErrors('Вы не можете добавить более 3 событий на главную страницу Ледового дворца');
            }
            /*if($num_important_2>=3 && $request->place_id ==3 && $old_is_important!=='true'){
                return view('events.edit')->with('event', $event)->with('places', $places)->with('teams', $teams)
                    ->withErrors('Вы не можете добавить более 3 важных событий для одного объекта');
            }*/
            if($request->place_id!=1 && $request->place_id!=2){// && $request->place_id!=3
                return view('events.edit')
                    ->with('event', $event)
                    ->with('places', $places)
                    ->with('teams', $teams)
                    ->withErrors('Вы можете добавить важное событие только для Ледового дворца');
            }
        }


        $event = $this->eventRepository->update($requestData, $id);
        $event->searchable();

        Flash::success('Событие успешно изменено.');

        return redirect(route('events.index'));
    }

    /**
     * Remove the specified Event from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $event = $this->eventRepository->findWithoutFail($id);

        if (empty($event)) {
            Flash::error('Событие не найдено');

            return redirect(route('events.index'));
        }

        $this->eventRepository->delete($id);
        $event->unsearchable();

        Flash::success('Событие успешно удалено.');

        return redirect(route('events.index'));
    }


}
