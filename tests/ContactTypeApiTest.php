<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ContactTypeApiTest extends TestCase
{
    use MakeContactTypeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateContactType()
    {
        $contactType = $this->fakeContactTypeData();
        $this->json('POST', '/api/v1/contactTypes', $contactType);

        $this->assertApiResponse($contactType);
    }

    /**
     * @test
     */
    public function testReadContactType()
    {
        $contactType = $this->makeContactType();
        $this->json('GET', '/api/v1/contactTypes/'.$contactType->id);

        $this->assertApiResponse($contactType->toArray());
    }

    /**
     * @test
     */
    public function testUpdateContactType()
    {
        $contactType = $this->makeContactType();
        $editedContactType = $this->fakeContactTypeData();

        $this->json('PUT', '/api/v1/contactTypes/'.$contactType->id, $editedContactType);

        $this->assertApiResponse($editedContactType);
    }

    /**
     * @test
     */
    public function testDeleteContactType()
    {
        $contactType = $this->makeContactType();
        $this->json('DELETE', '/api/v1/contactTypes/'.$contactType->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/contactTypes/'.$contactType->id);

        $this->assertResponseStatus(404);
    }
}
