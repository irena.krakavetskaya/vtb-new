<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $img->id !!}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{!! $img->link !!}</p>
</div>

<!-- Object Id Field -->
<div class="form-group">
    {!! Form::label('object_id', 'Object Id:') !!}
    <p>{!! $img->object_id !!}</p>
</div>

