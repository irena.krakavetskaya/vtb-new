<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $page->id !!}</p>
</div>

<!-- Pagetitle Field -->
<div class="form-group">
    {!! Form::label('pagetitle', 'Pagetitle:') !!}
    <p>{!! $page->pagetitle !!}</p>
</div>

<!-- Longtitle Field -->
<div class="form-group">
    {!! Form::label('longtitle', 'Longtitle:') !!}
    <p>{!! $page->longtitle !!}</p>
</div>

<!-- Introtext Field -->
<div class="form-group">
    {!! Form::label('introtext', 'Introtext:') !!}
    <p>{!! $page->introtext !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $page->description !!}</p>
</div>

<!-- Content Field -->
<div class="form-group">
    {!! Form::label('content', 'Content:') !!}
    <p>{!! $page->content !!}</p>
</div>

<!-- Menuindex Field -->
<div class="form-group">
    {!! Form::label('menuindex', 'Menuindex:') !!}
    <p>{!! $page->menuindex !!}</p>
</div>

<!-- Menutitle Field -->
<div class="form-group">
    {!! Form::label('menutitle', 'Menutitle:') !!}
    <p>{!! $page->menutitle !!}</p>
</div>

<!-- Alias Field -->
<div class="form-group">
    {!! Form::label('alias', 'Alias:') !!}
    <p>{!! $page->alias !!}</p>
</div>

<!-- Parent Field -->
<div class="form-group">
    {!! Form::label('parent', 'Parent:') !!}
    <p>{!! $page->parent !!}</p>
</div>

<!-- Isfolder Field -->
<div class="form-group">
    {!! Form::label('isfolder', 'Isfolder:') !!}
    <p>{!! $page->isfolder !!}</p>
</div>

<!-- Template Id Field -->
<div class="form-group">
    {!! Form::label('template_id', 'Template Id:') !!}
    <p>{!! $page->template_id !!}</p>
</div>

<!-- Seo Title Field -->
<div class="form-group">
    {!! Form::label('seo_title', 'Seo Title:') !!}
    <p>{!! $page->seo_title !!}</p>
</div>

<!-- Seo Keywords Field -->
<div class="form-group">
    {!! Form::label('seo_keywords', 'Seo Keywords:') !!}
    <p>{!! $page->seo_keywords !!}</p>
</div>

<!-- Seo Description Field -->
<div class="form-group">
    {!! Form::label('seo_description', 'Seo Description:') !!}
    <p>{!! $page->seo_description !!}</p>
</div>

<!-- Pagetitle En Field -->
<div class="form-group">
    {!! Form::label('pagetitle_en', 'Pagetitle En:') !!}
    <p>{!! $page->pagetitle_en !!}</p>
</div>

<!-- Longtitle En Field -->
<div class="form-group">
    {!! Form::label('longtitle_en', 'Longtitle En:') !!}
    <p>{!! $page->longtitle_en !!}</p>
</div>

<!-- Introtext En Field -->
<div class="form-group">
    {!! Form::label('introtext_en', 'Introtext En:') !!}
    <p>{!! $page->introtext_en !!}</p>
</div>

<!-- Description En Field -->
<div class="form-group">
    {!! Form::label('description_en', 'Description En:') !!}
    <p>{!! $page->description_en !!}</p>
</div>

<!-- Content En Field -->
<div class="form-group">
    {!! Form::label('content_en', 'Content En:') !!}
    <p>{!! $page->content_en !!}</p>
</div>

<!-- Menutitle En Field -->
<div class="form-group">
    {!! Form::label('menutitle_en', 'Menutitle En:') !!}
    <p>{!! $page->menutitle_en !!}</p>
</div>

