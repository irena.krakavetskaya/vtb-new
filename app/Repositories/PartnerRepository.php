<?php

namespace App\Repositories;

use App\Models\Partner;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PartnerRepository
 * @package App\Repositories
 * @version July 10, 2018, 10:32 am UTC
 *
 * @method Partner findWithoutFail($id, $columns = ['*'])
 * @method Partner find($id, $columns = ['*'])
 * @method Partner first($columns = ['*'])
*/
class PartnerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'img',
        'site'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Partner::class;
    }
}
