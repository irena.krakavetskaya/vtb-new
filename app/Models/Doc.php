<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

/**
 * Class Doc
 * @package App\Models
 * @version December 28, 2017, 1:06 pm UTC
 *
 * @property string name
 * @property string link
 * @property string name_en
 * @property integer object_id
 */
class Doc extends Model
{
    //use SoftDeletes;

    public $table = 'docs';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'link',
        'name_en',
        'object_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'link' => 'string',
        'name_en' => 'string',
        'object_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'link' => 'required',
        'object_id' => 'required'
    ];

    public $timestamps = false;

    public function uploadDocs($files,$names,$ids,$del,$page_id,$lang){
        $postfix = ($lang=='en')?'_en':'';
        if(isset($files)){
            foreach ($files as $key=>$val){
                $str=$names[$key].'-'.time();
                $filename =$val->storeAs(
                    'pdf', $str.'.pdf'
                );
                $count = Doc::where('id', $key+1)->first();
                if(isset($count)){
                    Doc::where('id', $key+1)
                        ->update([
                            'name'.$postfix => $names[$key],
                            'link'.$postfix=> $filename,
                            'page_id'=> $page_id,
                        ]);
                }
                else{
                    Doc::insert([
                        'name'.$postfix => $names[$key],
                        'link'.$postfix=> $filename,
                        'page_id'=> $page_id,
                    ]);
                }
            }
        }

        if(isset($names) && !isset($files)){
            //return $ids;
            foreach ($ids as $key=>$id_) {
                $str=$names[$key].'-'.time();
                if(!Storage::disk('local')->exists('pdf/'.$str.'.pdf')){

                    $oldName=Doc::where('id', $id_)->value('link'.$postfix);
                    $oldId=Doc::where('id', $id_)->value('id');

                    Storage::move($oldName, 'pdf/'.$str.'.pdf');
                    Doc::where('id', $oldId)
                        ->update([
                            'name'.$postfix => $names[$key],
                            'link'.$postfix=> 'pdf/'.$str.'.pdf',
                        ]);
                }
            }
        }
        if(isset($del)){
            $names= Doc::whereIn('id', $del)->pluck('link');
            Doc::whereIn('id', $del)->delete();
        }
    }


    //for  multilingual api
    public function getDocFields($request)
    {
        $lang = $request->header('X-Language');
        if($lang=='en'){
            return $this->select('name_en as name','link_en as link'); //Doc::
        }
        else{
            return  $this->select('name','link');
        }
    }
}
