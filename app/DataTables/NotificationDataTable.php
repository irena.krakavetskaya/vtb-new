<?php

namespace App\DataTables;

use App\Models\Notification;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class NotificationDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('type', function (Notification $notification) {
                return $notification->type=='push'?'Пуш-уведомление':'Email-сообщение';
            })
            ->editColumn('event_id', function (Notification $notification) {
                if(isset($notification->event->name)){
                    return $notification->event->name;
                }
                elseif(isset($notification->event->team1->id)){
                    return $notification->event->team1->name.' - '.$notification->event->team2->name;
                }
            })
            ->addColumn('action', 'notifications.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Notification $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'buttons' => [
                    'create',
                    'export',
                    'print',
                    'reset',
                    'reload',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'name'=> ['name' => 'name', 'data' => 'name','title'=>'Заголовок'],
            'text'=> ['name' => 'text', 'data' => 'text','title'=>'Текст'],
            'push_datetime'=> ['name' => 'push_datetime', 'data' => 'push_datetime','title'=>'Дата и время отправки'],
            'event_id'=> ['name' => 'event_id', 'data' => 'event_id','title'=>'Событие Афиши'],
            'type'=> ['name' => 'type', 'data' => 'type','title'=>'Тип'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'notificationsdatatable_' . time();
    }
}