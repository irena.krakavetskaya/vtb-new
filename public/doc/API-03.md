* Карточка события:

===================================

Запрос:

    GET /api/v1/get-events/{id}
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			 {
                 "id": 1,
                 "type": "show",
                 "time": "17:00",
                 "kind": "Шоу",
                 "name": "БОЛЬШОЙ ТЕСТ-ДРАЙВ SUBARU XV 1",
                 "img": "img_show/83iagllAiuNHahrXW1ZL5fotry58Fga15puIJxTm.jpeg",
                 "text": "Ледовое шоу Ильи Авербуха",
                 "dateEvent": "2017-12-18",
                 "place": "Малая арена",
                 "phoneVip": "+7 (495) 968-22-33",
                 "phoneBoxOffice": "+ 7 (495) 223-01-39",
                 "openHours": "ежедневно с 10 до 20 часов"
             }
             
Или:   

            {
                "id": 3,
                "type": "match",
                "time": "19:00",
                "kind": "КХЛ",              
                "dateEvent": "2018-01-09",
                "place": "Большая арена",
                "phoneVip": "+7 (495) 968-22-33",
                "phoneBoxOffice": "+ 7 (495) 223-01-39",
                "openHours": "ежедневно с 10 до 20 часов9",
                "team1": {
                    "name": "Спартак",
                    "logo": "logo/muPgF8fGOjanlNeBKQS0kWde3rAwCmXMz8YjTZf3.jpeg"
                },
                "team2": {
                    "name": "ЦСКА",
                    "logo": "logo/Q9R5gkBaoe4x5WHceP2RvAJJYD3YRsWZYsTfpB4y.jpeg"
                }
            }          