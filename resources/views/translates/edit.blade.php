@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Translate
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($translate, ['route' => ['translates.update', $translate->id], 'method' => 'patch']) !!}

                        @include('translates.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection