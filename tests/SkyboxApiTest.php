<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SkyboxApiTest extends TestCase
{
    use MakeSkyboxTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateSkybox()
    {
        $skybox = $this->fakeSkyboxData();
        $this->json('POST', '/api/v1/skyboxes', $skybox);

        $this->assertApiResponse($skybox);
    }

    /**
     * @test
     */
    public function testReadSkybox()
    {
        $skybox = $this->makeSkybox();
        $this->json('GET', '/api/v1/skyboxes/'.$skybox->id);

        $this->assertApiResponse($skybox->toArray());
    }

    /**
     * @test
     */
    public function testUpdateSkybox()
    {
        $skybox = $this->makeSkybox();
        $editedSkybox = $this->fakeSkyboxData();

        $this->json('PUT', '/api/v1/skyboxes/'.$skybox->id, $editedSkybox);

        $this->assertApiResponse($editedSkybox);
    }

    /**
     * @test
     */
    public function testDeleteSkybox()
    {
        $skybox = $this->makeSkybox();
        $this->json('DELETE', '/api/v1/skyboxes/'.$skybox->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/skyboxes/'.$skybox->id);

        $this->assertResponseStatus(404);
    }
}
