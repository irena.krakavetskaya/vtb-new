<?php

use App\Models\Point;
use App\Repositories\PointRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PointRepositoryTest extends TestCase
{
    use MakePointTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var PointRepository
     */
    protected $pointRepo;

    public function setUp()
    {
        parent::setUp();
        $this->pointRepo = App::make(PointRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatePoint()
    {
        $point = $this->fakePointData();
        $createdPoint = $this->pointRepo->create($point);
        $createdPoint = $createdPoint->toArray();
        $this->assertArrayHasKey('id', $createdPoint);
        $this->assertNotNull($createdPoint['id'], 'Created Point must have id specified');
        $this->assertNotNull(Point::find($createdPoint['id']), 'Point with given id must be in DB');
        $this->assertModelData($point, $createdPoint);
    }

    /**
     * @test read
     */
    public function testReadPoint()
    {
        $point = $this->makePoint();
        $dbPoint = $this->pointRepo->find($point->id);
        $dbPoint = $dbPoint->toArray();
        $this->assertModelData($point->toArray(), $dbPoint);
    }

    /**
     * @test update
     */
    public function testUpdatePoint()
    {
        $point = $this->makePoint();
        $fakePoint = $this->fakePointData();
        $updatedPoint = $this->pointRepo->update($fakePoint, $point->id);
        $this->assertModelData($fakePoint, $updatedPoint->toArray());
        $dbPoint = $this->pointRepo->find($point->id);
        $this->assertModelData($fakePoint, $dbPoint->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletePoint()
    {
        $point = $this->makePoint();
        $resp = $this->pointRepo->delete($point->id);
        $this->assertTrue($resp);
        $this->assertNull(Point::find($point->id), 'Point should not exist in DB');
    }
}
