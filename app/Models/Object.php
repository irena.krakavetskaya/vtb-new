<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Image;

/**
 * Class Object
 * @package App\Models
 * @version December 28, 2017, 11:12 am UTC
 *
 * @property string name
 * @property string name_en
 * @property string introtext
 * @property string introtext_en
 * @property string text
 * @property string text_en
 * @property string img
 * @property string fb
 * @property string inst
 * @property string vk
 * @property string youtube
 */
class Object extends Model
{
    //use SoftDeletes;

    public $table = 'objects';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'name_en',
        'introtext',
        'introtext_en',
        'text',
        'text_en',
        'img',
        'fb',
        'inst',
        'vk',
        'youtube',
        'is_active',
        'phone',
        'address',
        'video',
        'email',
        'open_hours',
        'open_hours_en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_en' => 'string',
        'introtext' => 'string',
        'introtext_en' => 'string',
        'text' => 'string',
        'text_en' => 'string',
        'img' => 'string',
        'fb' => 'string',
        'inst' => 'string',
        'vk' => 'string',
        'youtube' => 'string',
        'is_active' => 'string',
        'phone' => 'string',
        'address' => 'string',
        'video' => 'string',
        'email' => 'string',
        'open_hours'=> 'string',
        'open_hours_en'=> 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        //'name' => 'required'
    ];

    public $timestamps = false;

    public function docs()
    {
        return $this->hasMany('App\Models\Doc', 'object_id');
    }

    public function docs_api()
    {
        return $this->hasMany('App\Models\Doc', 'object_id')->select(array('name', 'link'));
    }

    public function imgs()
    {
        return $this->hasMany('App\Models\Img', 'object_id');
    }

    public function prices()
    {
        return $this->hasMany('App\Models\Price', 'object_id');
    }

    public function hints()
    {
        return $this->hasMany('App\Models\Hint', 'object_id')->select('name', 'text', 'img');
    }

    public function getHints()
    {
        return $this->hasMany('App\Models\Hint', 'object_id'); //->select('name', 'text', 'img')->get();

    }


    public function prices_active_ru($id)
    {
         $pr = Price::select(array('id','name', 'price', 'text','is_link'))
             ->where([
                 ['is_active', '=', 'true'],
                 ['object_id', '=', $id],
             ])
            ->get();

        $pr = $pr->each(function ($item) {
            $item['isLink']=$item->is_link;
            /*if(is_null($item->text)){
                 $item['isLink']='false';
            }
            else{
                $item['isLink']='true';
            }*/
            unset($item['text'],$item['is_link']);
        });

        return $pr;
    }

    public function halls()
    {
        return $this->hasMany('App\Models\Hall', 'object_id');
    }

    public function halls_ru()
    {
        return $this->hasMany('App\Models\Hall', 'object_id')->select(array('id','name'));
    }

    public function get_skyboxes()
    {
        return Skybox::select('name','img')->get();
    }

    public function get_skyboxes_ru()
    {
        return Skybox::select('name_en','img')->get();
    }


    //for api
    public function events()
    {
        $date_now= date('Y-m-d');
        $subquery = $this->hasMany('App\Models\Event', 'object_id')
            ->select('id','type','date', 'time', 'kind','place_id','team1_id','team2_id','name','img','text')
            ->whereDate('date','>=', $date_now);
        if($this->id==1){
            return  $subquery ->where('is_important', 'true')
                ->orderBy('date')
                ->limit(3);
        }
        if($this->id==2){
            return  $subquery->orderBy('date')
                ->limit(3);
        }
    }


    public function getContacts()//api
    {


        $object = new Object();
        $contacts = $object->select('name','phone','email','address','open_hours')
            ->whereIn('id',[1,6])
            ->orderBy('pos','asc')
            ->get();
            //->toArray();

        $contacts = $contacts->each(function ($item) {
            $item['openHours']=$item->open_hours;
            unset($item['open_hours']);
        });


        $socials =  $object->where('id',5)
            ->select('fb','inst','vk','youtube')
            ->first()
            ->toArray();
        $socials['name']=Translate::where('id',28)->value('ru');

        $result=[
            'contacts'=>$contacts,
            'socialNetworks'=>$socials
        ];

        return $result;
    }

    public function formatImg($file, $folder, $h, $w)
    {
        $img = time()."-".$file->getClientOriginalName();
        $file->move($folder, $img);
        Image::make(sprintf($folder.'/%s', $img))->fit($h, $w)->save();//fir / crop/resize
        $img= $folder.'/'.$img;
        return $img;
    }
}
