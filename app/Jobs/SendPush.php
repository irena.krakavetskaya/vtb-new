<?php

namespace App\Jobs;

use App\Models\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class SendPush implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //$current_date = date('Y-m-d');
        $minutes = 10;

        $dataTask = Notification::whereRaw('push_datetime > DATE_SUB(NOW(), INTERVAL ' . $minutes . ' MINUTE) AND push_datetime < DATE_SUB(NOW(), INTERVAL ' . ($minutes - 10) . ' MINUTE)')
            ->get();


        if (!$dataTask->isEmpty()){
            foreach ($dataTask as $task) {
                $request = new Request();
                //$request->tokens_arr = $tokens_arr;
                $request->title = $task->title;
                $request->body = $task->introtext;
                $request->data = ['id_event' => $task->id];
                $SiteController = (new AppBaseController);
                $SiteController->sendPush($request);
            }
        }

        if (!$dataTask->isEmpty()){
            $content = "Пуш-уведомление успешно отправлено \n";
            //file_put_contents(base_path().'public/cron.txt', date('Y-m-d H:i:s').'-'.$content, FILE_APPEND);
            file_put_contents(__DIR__.'/cron.txt', date('Y-m-d H:i:s').'-'.$content, FILE_APPEND);
        }
    }
}
