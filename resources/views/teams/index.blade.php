@extends('layouts.app')

@section('content')
    <section class="content-header">

        <div class="pull-left events-btn">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('events.index') !!}" target="_blank">События</a>
        </div>
        <h1 class="pull-left">Команды</h1>

    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>


        <!--<ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a href="{! route('events.create') !!}" class="nav-link"    role="tab" aria-controls="events" aria-selected="true">Событие</a>
            </li>
            <li class="nav-item active">
                <a href="#teams"  role="tab" class="nav-link  active in" aria-controls="teams" aria-selected="true"  >Команды</a></li>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade" id="events" role="tabpanel" aria-labelledby="events-tab">
            </div>
            <div class="tab-pane fade show active in" id="teams" role="tabpanel" aria-labelledby="teams-tab">-->

                <div class="box box-primary">
                     <div class="box-body teams">
                        <h1 class="pull-right">
                            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('teams.create') !!}">Создать новую команду</a>
                        </h1>
                        @include('teams.table')
                    </div>
                </div>
                <div class="text-center">
                </div>

            <!--</div>-->
        </div>


@endsection

