@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            {!! $page->pagetitle !!}
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($page, ['route' => ['pages.update', $page->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                           @include('pages.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection