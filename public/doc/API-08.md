* Аренда льда

===================================

Запрос:

    GET /api/v1/services/1
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			{
                "id": 1,
                "name": "Аренда льда",
                "text": "<body><h3>ВТБ Ледовый Дворец идеально подходит для тренировок и игр как профессионалов, так и любителей хоккея</h3>\r\n\r\n<p>Главная площадка Чемпионата мира по хоккею 2016, Кубка Первого канала по хоккею и матчей регулярного чемпионата КХЛ отвечает самым высоким требованиям Международной федерации хоккея на льду.</p>\r\n\r\n<p>Главная площадка Чемпионата мира по хоккею 2016, Кубка Первого канала по хоккею и матчей регулярного чемпионата КХЛ отвечает самым высоким требованиям Международной федерации хоккея на льду</p>\r\n\r\n<table border='1' cellpadding='1' cellspacing='1' class='table' style='width:500px'>\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td><img alt='' src='http://dev.vtb.indi.by/ckeditor/kcfinder/upload/images/ic_ice_rent_1.png' style='float:left; height:81px; margin-right:10px; width:80px' /></td>\r\n\t\t\t<td>Комфортные раздевалки</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><img alt='' src='http://dev.vtb.indi.by/ckeditor/kcfinder/upload/images/ic_ice_rent_1.png' style='float:left; height:81px; margin-right:10px; width:80px' /></td>\r\n\t\t\t<td>Профессиональное обслуживание</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><img alt='' src='http://dev.vtb.indi.by/ckeditor/kcfinder/upload/images/ic_ice_rent_1.png' style='float:left; height:81px; margin-right:10px; width:80px' /></td>\r\n\t\t\t<td>Прокат и ремонт инвентаря</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><img alt='' src='http://dev.vtb.indi.by/ckeditor/kcfinder/upload/images/ic_ice_rent_1.png' style='float:left; height:81px; margin-right:10px; width:80px' /></td>\r\n\t\t\t<td>Кейтеринг</td>\r\n\t\t</tr>\r\n\t\t<tr>\r\n\t\t\t<td><img alt='' src='http://dev.vtb.indi.by/ckeditor/kcfinder/upload/images/ic_ice_rent_1.png' style='float:left; height:81px; margin-right:10px; width:80px' /></td>\r\n\t\t\t<td>Организация бизнес-встреч</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<h3><img alt='' src='http://dev.vtb.indi.by/ckeditor/kcfinder/upload/images/ic_ice_rent_1.png' style='float:left; height:81px; margin-right:10px; width:80px' /></h3>\r\n\r\n<h3>&nbsp;</h3>\r\n\r\n<h3>Type the text here</h3></body>",
                "email": "milyaeva@corp-ten.ru",
                "phone": "+7 909 689-07-50",
                "additional": "По вопросам аренды льда связывайтесь с Еленой Миляевой в будние дни с 9:00 до 18:00."
            }