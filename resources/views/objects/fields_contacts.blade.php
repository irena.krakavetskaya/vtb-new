        {!! Form::hidden('contacts', 'true',  ['class' => 'form-control']) !!}

        <div class="form-group col-sm-6">
            <h3>{!!$object->name!!}</h3>
        </div>
        <div class="clearfix"></div>

        <div class="form-group col-sm-6">
            {!! Form::label('phone', 'Телефон:') !!}
            {!! Form::text('phone', $object->phone,  ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-6">
            {!! Form::label('email', 'E-mail:') !!}
            {!! Form::email('email', $object->email,  ['class' => 'form-control']) !!}
        </div>
        <div class="clearfix"></div>

        <div class="form-group col-sm-6">
            {!! Form::label('address', 'Адрес:') !!}
            {!! Form::text('address', $object->address,  ['class' => 'form-control']) !!}
        </div>

        @if($object->id==2 || $object->id==6)
            <div class="form-group col-sm-6">
                {!! Form::label('open_hours', 'График работы:') !!}
                {!! Form::text('open_hours', $object->open_hours,  ['class' => 'form-control']) !!}
            </div>
        @endif
        <div class="clearfix"></div>

        @if($object->id==5)
            <div class="form-group col-sm-6 title">
                <h4>{!!$object->name!!} в соцсетях</h4>
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-sm-6">
                {!! Form::label('fb', 'Facebook:') !!}
                {!! Form::text('fb', $object->fb,  ['class' => 'form-control']) !!}
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-sm-6">
                {!! Form::label('inst', 'Instagram:') !!}
                {!! Form::text('inst', $object->inst,  ['class' => 'form-control']) !!}
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-sm-6">
                {!! Form::label('vk', 'ВКонтакте:') !!}
                {!! Form::text('vk', $object->vk,  ['class' => 'form-control']) !!}
            </div>
            <div class="clearfix"></div>

            <div class="form-group col-sm-6">
                {!! Form::label('youtube', 'YouTube:') !!}
                {!! Form::text('youtube', $object->youtube,  ['class' => 'form-control']) !!}
            </div>
            <div class="clearfix"></div>
        @endif

<!-- Submit Field -->
<div class="form-group col-sm-12 btns">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('objects.index') !!}" class="btn btn-default">Отмена</a>
</div>
