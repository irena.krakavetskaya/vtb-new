<?php

namespace App\Repositories;

use App\Models\Skybox;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class SkyboxRepository
 * @package App\Repositories
 * @version January 29, 2018, 8:31 am UTC
 *
 * @method Skybox findWithoutFail($id, $columns = ['*'])
 * @method Skybox find($id, $columns = ['*'])
 * @method Skybox first($columns = ['*'])
*/
class SkyboxRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en',
        'img'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Skybox::class;
    }
}
