<?php

use Faker\Factory as Faker;
use App\Models\Floor;
use App\Repositories\FloorRepository;

trait MakeFloorTrait
{
    /**
     * Create fake instance of Floor and save it in database
     *
     * @param array $floorFields
     * @return Floor
     */
    public function makeFloor($floorFields = [])
    {
        /** @var FloorRepository $floorRepo */
        $floorRepo = App::make(FloorRepository::class);
        $theme = $this->fakeFloorData($floorFields);
        return $floorRepo->create($theme);
    }

    /**
     * Get fake instance of Floor
     *
     * @param array $floorFields
     * @return Floor
     */
    public function fakeFloor($floorFields = [])
    {
        return new Floor($this->fakeFloorData($floorFields));
    }

    /**
     * Get fake data of Floor
     *
     * @param array $postFields
     * @return array
     */
    public function fakeFloorData($floorFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'name_en' => $fake->word,
            'place_id' => $fake->randomDigitNotNull,
            'img' => $fake->word
        ], $floorFields);
    }
}
