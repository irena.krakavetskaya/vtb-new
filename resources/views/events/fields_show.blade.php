<div class="form-group col-sm-12 no-padding">
    <!-- Type Field -->
    <div class="form-group col-md-4">
        {!! Form::label('type', 'Тип события:') !!}
        @isset($event->type)
            {!! Form::select('type', array('match' =>'Матч','show'=>'Мероприятие','excursion'=>'Экскурсия'),$event->type, ['class' => 'form-control','disabled']) !!}
        @else
            {!! Form::select('type', array('match' =>'Матч','show'=>'Мероприятие','excursion'=>'Экскурсия'),null, ['class' => 'form-control']) !!}
        @endisset
    </div>

    <!-- Place Id Field -->
    <div class="form-group col-md-4">
        {!! Form::label('place_id', 'Место проведения:') !!}
        <select id="place_id" name="place_id" class="form-control">
            @if(isset($event->place_id))
                @foreach ($places as $key=>$value)
                    @if($value->id == $event->place_id)
                        <option value="{!! $value->id !!}" selected>{!! $value->name !!}</option>
                    @else
                        <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                    @endif
                @endforeach
            @else
                @foreach ($places as $key=>$value)
                    <option value="{!! $value->id !!}">{!! $value->name !!}</option>
                @endforeach
            @endif
        </select>
    </div>
    <div class="clearfix"></div>

    <!-- Name Field -->
    <div class="form-group col-md-4">
        {!! Form::label('name', 'Название:') !!}
        {!! Form::text('name', null, ['class' => 'form-control','maxlength'=>50]) !!}
    </div>
    <div class="form-group col-md-4 name">
        {!! Form::label('name_en', 'Название (En):') !!}
        {!! Form::text('name_en', null, ['class' => 'form-control','maxlength'=>50]) !!}
    </div>
</div>

<div class="form-group col-md-12 no-padding">
    <div class="form-group col-md-4">
        {!! Form::label('date', 'Дата:') !!}
        @isset($event->date)
            {!! Form::date('date', date('Y-m-d', strtotime($event->date)), ['class' => 'form-control']) !!}
        @else
            {!! Form::date('date', date('Y-m-d'), ['class' => 'form-control']) !!}
        @endisset <!--'id'=> 'datepicker', 'data-date-format'=>'dd/mm/yyyy','data-provide'=>'datepicker'-->
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('time', 'Время:') !!}
        @isset($event->time)
            {!! Form::time('time', null, ['class' => 'form-control']) !!}
        @else
            {!! Form::time('time', '00:00', ['class' => 'form-control']) !!}
        @endisset
    </div>
    <div class="clearfix"></div>

    <div class="form-group col-md-4">
        {!! Form::label('kind', 'Вид мероприятия:') !!}
        {!! Form::text('kind', null, ['class' => 'form-control','maxlength'=>50]) !!}
    </div>
    <div class="form-group col-md-4 league">
        {!! Form::label('kind_en', 'Вид мероприятия (En):') !!}
        {!! Form::text('kind_en', null, ['class' => 'form-control','maxlength'=>50]) !!}
    </div>

</div>

@if($event->place_id==1 || $event->place_id==2 || $event->place_id==3)<!---->
    <div class="form-group col-sm-12 no-padding">
        <div class="form-group col-md-4">
            @isset($event->is_important)
                @if($event->is_important=='true')
                    {!! Form::checkbox('is_important', $event->is_important, true,['class' => 'field', 'checked'=>true]) !!}
                @else
                    {!! Form::checkbox('is_important', $event->is_important, false, ['class' => 'field', 'checked'=>false]) !!}
                @endif
            @else
                {!! Form::checkbox('is_important', null, null, ['class' => 'field', 'checked'=>false]) !!}
            @endisset
            {!! Form::label('is_important', 'Показывать на главной') !!}
        </div>

        <div class="form-group col-md-4 is_adv">
            @isset($event->is_adv)
                @if($event->is_adv=='true')
                    {!! Form::checkbox('is_adv', $event->is_adv, true,['class' => 'field', 'checked'=>true]) !!}
                @else
                    {!! Form::checkbox('is_adv', $event->is_adv, false, ['class' => 'field', 'checked'=>false]) !!}
                @endif
            @else
                {!! Form::checkbox('is_adv', 'false', false, ['class' => 'field', 'checked'=>false]) !!}
            @endisset
            {!! Form::label('is_adv', 'Выводить в баннере') !!}
        </div>
    </div>
@endif

<div class="form-group col-sm-12 no-padding">
    <!-- Text Field-->
    <!--<div class="form-group col-md-8">
        !! Form::label('text', 'Описание:') !!}
        !! Form::textarea('text', null, ['class' => 'form-control','maxlength'=>500]) !!}
    </div>-->


    <!--<script type="text/javascript">
        CKEDITOR.replace( 'ckeditor' );
    </script>-->


    <!-- Text Field -->
    <div class="form-group col-md-4">
        {!! Form::label('img', 'Изображение (рекоменд. 750*400 px):') !!}

        <input type="hidden" name="MAX_UPLOAD_SIZE" value="10000">
        @if(!empty($event->img))
            <input name="img1" type="hidden" value="{!! $event->img  !!}">

            <img src="{!! url("/") !!}/{!! $event->img !!}" class="img-responsive" id="uploadedimage1">
        @else

        @endif
        {!! Form::file('img', null, ['class' => 'form-control','id'=>'img', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}

        <img id="uploadedimage1" width="200px" />
        <span id="imageerror1" style="font-weight: bold; color: red"></span>
    </div>
</div>





<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('events.index') !!}" class="btn btn-default">Отмена</a>
</div>

