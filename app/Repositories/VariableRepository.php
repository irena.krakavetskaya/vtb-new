<?php

namespace App\Repositories;

use App\Models\Variable;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class VariableRepository
 * @package App\Repositories
 * @version December 26, 2017, 2:23 pm UTC
 *
 * @method Variable findWithoutFail($id, $columns = ['*'])
 * @method Variable find($id, $columns = ['*'])
 * @method Variable first($columns = ['*'])
*/
class VariableRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'value',
        'description',
        'admin'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Variable::class;
    }
}
