@extends('layouts.app')

@section('content')

   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="clearfix"></div>

       @include('flash::message')

       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                  <!-- !! Form::model($place, ['route' => ['places.update', $place->id], 'method' => 'patch']) !!}-->

                       @if($place->id===1)
                           @include('places.fields_big')
                       @elseif($place->id===2)
                           @include('places.fields_big')
                       @elseif($place->id===5)
                            {!!   Form::model($place, ['route' => ['places.update', $place->id], 'method' => 'patch']) !!}
                                @include('places.fields_parking')
                            {!! Form::close() !!}
                       @endif


                  <!--!! Form::close() !!}-->
               </div>
           </div>
       </div>
   </div>
@endsection