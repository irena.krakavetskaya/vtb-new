* Список событий афишы

===================================

Типы:

    show - мероприятие, match - матч, excursion - экскурсия				
				
===================================


Запрос:

    GET /api/v1/events?startIndex={1}&endIndex={3} //актуальные события с 1-го по 3-е в порядке возрастания даты
    GET /api/v1/events                             //все актуальные события
    
Заголовки:
    
    Accept: application/json
    X-API-Version: 1.01
    X-Language: ru
   
    
Ответ:

    HTTP/1.1 200 OK

			  [
                     {
                         "id": 2,
                         "type": "show",
                         "time": "21:00",
                         "kind": "Концерт",
                         "name": "ОТКРЫТИЕ КАТКА В ПАРКЕ ЛЕГЕНД",
                         "img": "img_show/ViDL9ki16sCs67G80W6IHL9nsDTCjSbV5S7O3iFt.jpeg",
                         "dateEvent": "2018-01-08",
                         "place": "Каток"
                     },
                     {
                         "id": 3,
                         "type": "match",
                         "time": "19:00",
                         "kind": "КХЛ",
                         "dateEvent": "2018-01-09",
                         "place": "Большая арена",
                         "team1": {
                             "name": "Спартак",
                             "logo": "logo/muPgF8fGOjanlNeBKQS0kWde3rAwCmXMz8YjTZf3.jpeg"
                         },
                         "team2": {
                             "name": "ЦСКА",
                             "logo": "logo/Q9R5gkBaoe4x5WHceP2RvAJJYD3YRsWZYsTfpB4y.jpeg"
                         }
                     },
                     {
                         "id": 4,
                         "type": "match",
                         "time": "18:00",
                         "kind": null,
                         "dateEvent": "2018-01-26",
                         "place": "Каток",
                         "team1": {
                             "name": "ЦСКА",
                             "logo": "logo/Q9R5gkBaoe4x5WHceP2RvAJJYD3YRsWZYsTfpB4y.jpeg"
                         },
                         "team2": {
                             "name": "Слован",
                             "logo": "logo/LNCgp1k3uFZ2uwyW0eqyw27anlXVc404U7upRCIc.jpeg"
                         }
                     }
              ]
            