<section class="content-header">
    <h1>
        {!!$place->name  !!}
    </h1>
</section>

<div class="col-md-12">
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach($floors as $key=>$floor)
                @isset($floor_id)
                    @if($floor_id==$floor->id)
                        <li class="active">
                    @else
                        <li>
                    @endif
                @else
                    <li>
                @endisset
                        <a href="#tab_{!! $key  !!}" data-toggle="tab" aria-expanded="true">
                            {!! $floor->name !!}
                        </a>
                    </li>
            @endforeach

        </ul>
        <div class="tab-content">
            @foreach($floors as $key=>$floor)
                @isset($floor_id)
                    @if($floor_id==$floor->id)
                        <div class="tab-pane active" id="tab_{!! $key  !!}"><!-- active-->
                    @else
                        <div class="tab-pane" id="tab_{!! $key  !!}">
                    @endif
                @else
                    <div class="tab-pane" id="tab_{!! $key  !!}"><!-- active-->
                @endisset


                {!! Form::model($floor, ['route' => ['floors.update', $floor->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}
                        <div class="schema col-md-6">
                            {!! Form::label('img_nav2', 'Схема:') !!}<br>
                            <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">
                            @if(!empty($floor->img2))
                                <a href="/{!! $floor->img2 !!}" data-toggle="lightbox">
                                    <img src="/{!! $floor->img2 !!}" class="img-responsive uploadedimage2">
                                </a>
                                <input name="img4" type="hidden" value="{!! $floor->img2  !!}">
                            @else
                                <img class="uploadedimage2" width="100%" src=""/>
                            @endif
                            <span class="imageerror2" style="font-weight: bold; color: red"></span>
                            <!--<input name="img1" type="hidden" value="!! $floor_id  !!}">-->
                            {!! Form::file('img2', ['class'=>'img_nav2', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
                        </div>


                        <div class="schema col-md-6">
                            {!! Form::label('img_nav1', 'Сектора:') !!}<br>
                            <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">
                            @if(!empty($floor->img1))
                                <a href="/{!! $floor->img1 !!}" data-toggle="lightbox">
                                    <img src="/{!! $floor->img1 !!}" class="img-responsive uploadedimage1">
                                </a>
                                <input name="img3" type="hidden" value="{!! $floor->img1  !!}">
                            @else
                                <img class="uploadedimage1" width="100%" src=""/>
                            @endif
                            <span class="imageerror1" style="font-weight: bold; color: red"></span>
                            <!--<input name="img3" type="hidden" value="!! $floor_id  !!}">-->
                            {!! Form::file('img1', ['class'=>'img_nav1', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-2">
                            {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
                        </div>
                        <div class="clearfix"></div>
                 {!! Form::close() !!}



                    <!-- img scheme/sectors-->
                    <!--
                    !! Form::model($floor, ['route' => ['floors.update', $floor->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}
                    if($floor->id!=1 && $floor->id!=6)
                        <div class="schema col-md-5">
                            !! Form::label('img_nav1', 'Сектора:') !!}<br>
                           <input type="hidden" name="MAX_UPLOAD_SIZE" value="15000">
                            if(!empty($floor->img1))
                                <a href="/!! $floor->img1 !!}" data-toggle="lightbox">
                                    <img src="/!! $floor->img1 !!}" class="img-responsive uploadedimage1">
                                </a>
                                <input name="img3" type="hidden" value="!! $floor->img1  !!}">
                            else
                                <img class="uploadedimage1" width="100%" src=""/>
                            endif
                            <span id="imageerror1" style="font-weight: bold; color: red"></span>
                            !! Form::file('img1',['class'=>'img_nav1', 'accept'=>'image/jpg,image/jpeg,image/png']) !!}
                        </div>
                    endif
 -->



                        <!-- schemes -->
                        <!--
                        if($floor->id!=1 && $floor->id!=6)
                            <div class="schema col-md-5">
                                !! Form::label('img_nav1', 'Сектора:') !!}<br>
                                if(!empty($floor->img1))
                                    <a href="/{! $floor->img1 !!}" data-toggle="lightbox">
                                        <img src="/!! $floor->img1 !!}" class="img-responsive uploadedimage1">
                                    </a>
                                endif
                            </div>
                        endif
                            <div class="schema col-md-5">
                                !! Form::label('img_nav2', 'Схема:') !!}<br>
                                if(!empty($floor->img2))
                                    <a href="/!! $floor->img2 !!}" data-toggle="lightbox">
                                        <img src="/!! $floor->img2 !!}" class="img-responsive uploadedimage2">
                                    </a>
                                endif
                            </div>
                        -->



                    <!--photo sectors-->
                    <!--
                    foreach($points as $key1=>$point)
                        if($point->floor_id == $floor->id)
                            <div class="point-row">

                                <div class="col-md-2 name">
                                <input name="id_point[]" type="hidden" value="!!$point->id  !!}" class="id_point">
                                    !! $point->name !!}
                                </div>

                                <div class="col-md-4  img">
                                    <a href="/!! $point->img !!}" data-toggle="lightbox">
                                        isset($point->img_thumb)
                                            <img src="/!! $point->img_thumb !!}" class="img-fluid img-responsive">
                                        else
                                            <img src="/!! $point->img !!}" class="img-fluid img-responsive">
                                        endisset

                                    </a>
                                </div>

                                <div class="col-md-1 toggle-modern">
                                    <div class="formrow clearfix">
                                        <div id="is_active" class="toggle2_!! $point->id !!} floatright pull-right"></div>
                                        if(isset($point->is_active))
                                            if($point->is_active=='true')
                                                <input type="text"  class="checkbx point" name="is_shown[]" value="!! $point->is_active !!}">
                                            else
                                                <input type="text"  class="checkbx point" name="is_shown[]"  value="false">
                                            endif
                                        else
                                            <input  type="text" class="checkbx point" name="is_shown[]"  value="true">
                                        endif
                                    </div>
                                </div>

                                <div class='btn-group col-md-2'>
                                    <a href="{ route('points.edit', $point->id) }}" class='btn btn-default btn-xs'>
                                        <i class="glyphicon glyphicon-edit"></i>
                                    </a>
                                    !! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs delete_point',
                                        'id'=>$point->id
                                    ]) !!}
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        endif
                    endforeach-->

                </div>
            @endforeach
        </div>

    </div>
</div>


<!-- Submit Field -->
<!--
<div class="form-group col-sm-12">
    !! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="!! route('places.index') !!}" class="btn btn-default">Cancel</a>
</div>
-->