<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 * @package App\Models
 * @version January 17, 2018, 9:11 am UTC
 *
 * @property string name
 * @property string name_en
 * @property string text
 * @property string text_en
 * @property string img
 * @property string email
 * @property string phone
 */
class Service extends Model
{
    //use SoftDeletes;

    public $table = 'services';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'name_en',
        'text',
        'text_en',
        'img',
        'email',
        'phone',
        'additional',
        'additional_en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'name_en' => 'string',
        'text' => 'string',
        'text_en' => 'string',
        'img' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'additional' => 'string',
        'additional_en' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];

    public $timestamps = false;


    public function getService($id)
    {
        if($id==2){ //for excursions
            //$arr = array();
            $date_now= date('Y-m-d');
            $service= Service::find($id);
            $object= Object::find(2);
            //$evs = $object->events()->get();
            $evs= Event::where('type','excursion')
                ->select('id','type','date', 'time', 'kind', 'place_id', 'name','img')
                ->whereDate('date','>=', $date_now)
                ->orderBy('date','asc')
                ->limit(3)
                ->get();
            $ev= new Event();
            $arr=[
                'id'=>$id,
                'name'=>$service->name,
                'phone'=>$service->phone,
                'pdf'=>$object->docs_api,
                'events'=>$ev->formatEvent($evs)
            ];
            return $arr;
        }
        elseif($id==3) {
            $service= Service::find($id);
            return $service->select('id','name','email','phone','additional')->where('id',$id)->first();
        }
        else{
            $res = $this->select('id','name','text','email','phone','additional')->where('id',$id)->first();//'img',

            $collection = collect($res);

            $modified= $collection->map(function ($item, $key) {

                switch($key){
                    case $key == 'name':return $item;break;
                    case $key == 'id':return $item;break;
                    case $key == 'email':return $item;break;
                    case $key == 'img':return $item;break;
                    case $key == 'phone':return $item;break;
                    case $key == 'additional':return $item;break;
                    case $key == 'text':
                        $host=Variable::where('id',3)->value('value');
                        $item = str_replace('"','\'',$item);
                        $pattern = '/src=\'/';
                        $replacement = 'src=\''.$host; //'src=\'http://dev.vtb.indi.by';
                        $item = preg_replace($pattern, $replacement,$item);
                        return  $item =  '<body>'.$item.'</body>';
                }
            });

            return $modified;
        }
    }

}
