<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Img
 * @package App\Models
 * @version January 3, 2018, 2:07 pm UTC
 *
 * @property string link
 * @property integer object_id
 */
class Img extends Model
{
    //use SoftDeletes;

    public $table = 'imgs';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'link',
        'object_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'link' => 'string',
        'object_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'link' => 'required',
        'object_id' => 'required'
    ];

    public $timestamps = false;

    //add/delete imgs from galleries
    public function uploadImgs($imgs,$del_img,$page_id){
        $insertedIds=[];
        if (isset($imgs)) {
            $last_id = Img::all()->last()->id;
            foreach ($imgs as $key => $val) {
                $filename = $val->store('image');
                $insertedId= Img::insertGetId([
                    'id' => ++$last_id,
                    'link' => $filename,
                    'page_id' => $page_id,
                ]);
                array_push($insertedIds,$insertedId);
            }
        }
        if(isset($del_img)){
            if(!empty($insertedIds)){
                array_push($del_img,$insertedIds );
            }
            Img::whereNotIn('id', $del_img)
                ->where('page_id', $page_id)
                ->delete();
        }
        else{
            Img::where('page_id', $page_id)
                ->delete();
        }
    }
}
