@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Translate
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('translates.show_fields')
                    <a href="{!! route('translates.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
