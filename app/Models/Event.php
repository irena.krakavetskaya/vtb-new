<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Image;

use Laravel\Scout\Searchable;

/**
 * Class Event
 * @package App\Models
 * @version December 19, 2017, 8:41 am UTC
 *
 * @property date date
 * @property time time
 * @property string name
 * @property string name_en
 * @property string text
 * @property string text_en
 * @property string type
 * @property integer place_id
 * @property integer event_type_id
 * @property integer team1_id
 * @property integer team2_id
 * @property string link
 */
class Event extends Model
{
    //use SoftDeletes;
    use Searchable;

    public $table = 'events';
    

   // protected $dates = ['deleted_at'];


    public $fillable = [
        'date',
        'time',
        'name',
        'name_en',
        'text',
        'text_en',
        'type',
        'place_id',
        'event_type_id',
        'team1_id',
        'team2_id',
        'link',
        'img',
        'kind',
        'kind_en',
        'object_id',
        'is_important',
        'is_adv'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date',
        'name' => 'string',
        'name_en' => 'string',
        'text' => 'string',
        'text_en' => 'string',
        'type' => 'string',
        'place_id' => 'integer',
        //'event_type_id' => 'integer',
        'team1_id' => 'integer',
        'team2_id' => 'integer',
        'link' => 'string',
        'img' => 'string',
        'kind'=>'string',
        'kind_en'=>'string',
        'object_id' => 'integer',
        'is_important'=>'string',
        'is_adv'=>'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'date' => 'required',
        'time' => 'required',
        'name' => 'max:50',//required|
        //'type' => 'required',
        'place_id' => 'required',
        //'event_type_id' => 'required',
        'img' => 'mimes:jpeg,jpg,png|max:10000',
        //'kind'=>'max:50',
    ];

    public $timestamps = false;

    public function places()
    {
        return $this->belongsTo('App\Models\Place', 'place_id');
    }

    public function objects()
    {
        return $this->belongsTo('App\Models\Object', 'object_id');
    }

    /*public function event_types()
    {
        return $this->belongsTo('App\Models\EventType', 'event_type_id');
    }*/


    public function team1()
    {
        return $this->belongsTo('App\Models\Team', 'team1_id');
    }
    public function team2()
    {
        return $this->belongsTo('App\Models\Team', 'team2_id');
    }


    public function getAd()
    {
        $date_now= date('Y-m-d');
        $time_now =date('H:i');

        $evs = Event::select('id','type','date', 'time','place_id', 'name','img')//,'text','kind'
                ->whereDate('date','>=', $date_now)
                //->whereTime('time','>=', $time_now)
                ->where('is_adv','true')
                ->where('type','!=','match')
                ->orderBy('date', 'asc')
                //->limit(3)
                ->get();
        foreach($evs as $ev){
            $ev->dateEvent = date('Y-m-d',strtotime($ev->date));//$ev->date->format('Y-m-d');
            //$day_en = $ev->date->format('D');
            //$ev->day_week= \DB::table('translates')->where('front', $day_en)->value('ru');
            $ev->time = date('H:i', strtotime($ev->time));
            $ev->place = $ev->places->name;
            unset($ev->type,$ev->place_id,$ev->places,$ev->team1_id,$ev->team2_id,$ev->date);
        }
        return $evs;
    }



    public function getMonths($month_now){
        //is not used now
        $months_1=\DB::table('translates')
            ->where('id', '<',13)
            ->where('id', '>=',$month_now)
            ->get();

        $months_2=\DB::table('translates')
            ->where('id', '<',13)
            ->where('id', '<',$month_now)
            ->get();
        $concatenated = $months_1->concat($months_2);
        $month_names =$concatenated->all();

        return $month_names;
    }


    public function getPlace($place){
        //is not used now ???
        if($place==1 || $place==2){
            $place=array(1, 2);
        }
        elseif($place=='all'){
            $place=array(1, 2, 3, 4);
        }
        else{
            $place=array($place);
        }
        return $place;
    }


    public function formatEvent($evs, $lang=null)
    {
        if(isset($evs->id)){
            $evs->dateEvent = date('Y-m-d', strtotime($evs->date));//$ev->date->format('Y-m-d');
            //$day_en = $evs->date->format('D');
            //$evs->day_week = \DB::table('translates')->where('front', $day_en)->value('ru');
            $evs->time = date('H:i', strtotime($evs->time));
            $evs->place = $evs->places->name;
            unset($evs->place_id, $evs->places, $evs->date);

            if ($evs->type == 'match') {
                if($lang=='en'){
                    unset($evs->name, $evs->img, $evs->text, $evs->team1['id'], $evs->team2['id'], $evs->team1['name'], $evs->team2['name']);
                }
                if($lang=='ru'){
                    unset($evs->name, $evs->img, $evs->text, $evs->team1['id'], $evs->team2['id'], $evs->team1['name_en'], $evs->team2['name_en']);
                }

            }
            unset($evs->team1_id, $evs->team2_id);
        }
        else{
            foreach ($evs as $ev) {
                $ev->dateEvent = date('Y-m-d', strtotime($ev->date));//$ev->date->format('Y-m-d');
                //$day_en = $ev->date->format('D');
                //$ev->day_week = \DB::table('translates')->where('front', $day_en)->value('ru');
                $ev->time = date('H:i', strtotime($ev->time));
                $ev->place = $ev->places->name;
                if ($ev->type == 'match') {
                    if($lang=='en'){
                        unset($ev->name, $ev->img, $ev->text, $ev->team1['id'], $ev->team2['id'], $ev->team1['name'], $ev->team2['name']);//$ev->team1['id'], $ev->kind,
                    }
                    if($lang=='ru'){
                        unset($ev->name, $ev->img, $ev->text, $ev->team1['id'], $ev->team2['id'], $ev->team1['name_en'], $ev->team2['name_en']);//$ev->team1['id'], $ev->kind,
                    }
                }
                unset($ev->place_id, $ev->places, $ev->team1_id, $ev->team2_id, $ev->date,$ev->text);
            }
        }
        return $evs;
    }

    public function formatImg($file, $folder, $h, $w)
    {
        $img = time()."-".$file->getClientOriginalName();
        $file->move($folder, $img);
        Image::make(sprintf($folder.'/%s', $img))->fit($h, $w)->save();
        $img= $folder.'/'.$img;
        return $img;
    }

    //for search on site
    public function searchableAs()
    {
        return 'events_index';
    }

    public function toSearchableArray()
    {
        $array = $this->toArray();
        // Customize array...
        return $array;
    }



    //for  multilingual api
    public function getEventFields($request)
    {
        $lang = $request->header('X-Language');
        //$model = new \App\Models\Event();
        if($lang=='en'){
            return $this->select('id','type','date', 'time', 'kind_en', 'place_id', 'name_en','img', 'team1_id', 'team2_id');
        }
        else{
            return  $this->select('id','type','date', 'time', 'kind', 'place_id', 'name','img', 'team1_id', 'team2_id');
        }

    }
}
