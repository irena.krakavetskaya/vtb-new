<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Image;
/**
 * Class Team
 * @package App\Models
 * @version December 18, 2017, 1:07 pm UTC
 *
 * @property string name
 * @property string logo
 * @property string name_en
 */
class Team extends Model
{
   // use SoftDeletes;

    public $table = 'teams';
    

    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'logo',
        'name_en'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'logo' => 'string',
        'name_en' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'logo'=> 'mimes:jpeg,jpg,png|max:10000'
    ];

    public $timestamps = false;

    public function events1()
    {
        return $this->hasMany('App\Models\Event','team1_id');
    }
    public function events2()
    {
        return $this->hasMany('App\Models\Event','team2_id');
    }

    public function formatImg($file, $folder, $h, $w)
    {
        /*
        $path = $file->store('/logo');
        $canvas = Image::canvas(66, 66, '#ffffff');

        $image = Image::make($path)->resize($w, $h);

        //$image = Image::make($path)->fit($w, $h);  //"app/public/" . $path  :make(sprintf($folder.'/%s', $img))

        $canvas->insert($image, 'center');
        $canvas->save( $path );
        return $path;
        */

        $img = time()."-".$file->getClientOriginalName();
        $file->move($folder, $img);
        Image::make(sprintf($folder.'/%s', $img))->fit($h, $w)->save();
        $img= $folder.'/'.$img;
        return $img;
    }
}
