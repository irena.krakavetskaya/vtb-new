<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Album",
 *      required={""},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="img",
 *          description="img",
 *          type="string"
 *      )
 * )
 */
class Album extends Model
{
    //use SoftDeletes;

    public $table = 'albums';
    
    //const CREATED_AT = 'created_at';
    //const UPDATED_AT = 'updated_at';


    //protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'name_en',
        'img',
        'date',
        'page_id',
        'type'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'img' => 'string',
        'page_id'=>'integer',
        'date'=>'datetime',
        'type'=> 'string',
        'name_en'=> 'string'

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function photos()
    {
        return $this->hasMany(\App\Models\Photo::class);
    }

    public function page()
    {
        return $this->belongsTo(\App\Models\Page::class);
    }

    public $timestamps = false;
}
