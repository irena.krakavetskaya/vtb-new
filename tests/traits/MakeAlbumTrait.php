<?php

use Faker\Factory as Faker;
use App\Models\Album;
use App\Repositories\AlbumRepository;

trait MakeAlbumTrait
{
    /**
     * Create fake instance of Album and save it in database
     *
     * @param array $albumFields
     * @return Album
     */
    public function makeAlbum($albumFields = [])
    {
        /** @var AlbumRepository $albumRepo */
        $albumRepo = App::make(AlbumRepository::class);
        $theme = $this->fakeAlbumData($albumFields);
        return $albumRepo->create($theme);
    }

    /**
     * Get fake instance of Album
     *
     * @param array $albumFields
     * @return Album
     */
    public function fakeAlbum($albumFields = [])
    {
        return new Album($this->fakeAlbumData($albumFields));
    }

    /**
     * Get fake data of Album
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAlbumData($albumFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->text,
            'img' => $fake->text
        ], $albumFields);
    }
}
