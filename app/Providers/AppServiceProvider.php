<?php

namespace App\Providers;

use App\Models\Page;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        if(env('APP_ENV') != 'local') {
        //if($_ENV['APP_ENV']!= 'local') { //another version
                URL::forceScheme('https');
        }

        $firstLevel= Page::where('parent_id',null)->get();
        //$level1_2 =Page::with('items')->where('parent_id',2)->get();
        $page = new Page();

        view()->share('firstLevel', $firstLevel);
        view()->share('page', $page);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
