<fieldset class="skybox-adv">
    <legend>Альбомы</legend>
    @foreach($albums as $album)
        <div  class="album">
            <div class="col-md-3">
                {!! $album->name !!}
            </div>
            <div class="col-md-3">
                @if($album->type=='img')
                    <a href="/{!! $album->img !!}" data-toggle="lightbox">
                        <img src="/{!! $album->img !!}" class="img-responsive">
                    </a>
                @elseif($album->type=='video')
                    <a href="{{ $video[$album->id]}}" style="background-image:url('http://img.youtube.com/vi/{{$video_img[$album->id]}}/hqdefault.jpg');"
                       title="{{ $album->name }}" class="video">
                    </a>
                @endif
            </div>

            <div class='col-md-3'>
                {!! date('d-m-Y', strtotime($album->date ))!!}
            </div>

            <div class='btn-group col-md-3'>
                <a href="{{ route('albums.edit', $album->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs delete_item',
                    'id'=>$album->id
                ]) !!}
            </div>
            <div class="clearfix"></div>
        </div>
    @endforeach
    <div class="form-group col-sm-6">
        <a href="{!! route('albums.create', ['page_id'=>$album->page_id]) !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
            Добавить альбом
        </a>
    </div>
</fieldset>