<?php

namespace App\Console;

use App\Jobs\DeleteImportance;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->job(new DeleteImportance())
            ->everyMinute()//->daily()//every day at midnight
            ->after(function () {
                $content = "Важное удалено со старых событий \n";
                //file_put_contents('./public/cron.txt', date('Y-m-d H:i:s').'-'.$content, FILE_APPEND);
                file_put_contents('./cron.txt', date('Y-m-d H:i:s').'-'.$content, FILE_APPEND);
            });

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
