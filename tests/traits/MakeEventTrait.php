<?php

use Faker\Factory as Faker;
use App\Models\Event;
use App\Repositories\EventRepository;

trait MakeEventTrait
{
    /**
     * Create fake instance of Event and save it in database
     *
     * @param array $eventFields
     * @return Event
     */
    public function makeEvent($eventFields = [])
    {
        /** @var EventRepository $eventRepo */
        $eventRepo = App::make(EventRepository::class);
        $theme = $this->fakeEventData($eventFields);
        return $eventRepo->create($theme);
    }

    /**
     * Get fake instance of Event
     *
     * @param array $eventFields
     * @return Event
     */
    public function fakeEvent($eventFields = [])
    {
        return new Event($this->fakeEventData($eventFields));
    }

    /**
     * Get fake data of Event
     *
     * @param array $postFields
     * @return array
     */
    public function fakeEventData($eventFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'date' => $fake->word,
            'time' => $fake->word,
            'name' => $fake->word,
            'name_en' => $fake->word,
            'text' => $fake->word,
            'text_en' => $fake->word,
            'type' => $fake->word,
            'place_id' => $fake->randomDigitNotNull,
            'event_type_id' => $fake->randomDigitNotNull,
            'team1_id' => $fake->randomDigitNotNull,
            'team2_id' => $fake->randomDigitNotNull,
            'link' => $fake->word
        ], $eventFields);
    }
}
