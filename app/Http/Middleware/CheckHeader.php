<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckHeader
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('X-API-Version');
        $value = \DB::table('variables')
            ->where('key', 'x_api_version')
            ->value('value');
        if (! isset($header) || $header != $value) {
            header("HTTP/1.0 666 X-API-Version");
            exit();
        }
        return $next($request);
    }
}
