<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $object->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $object->name !!}</p>
</div>

<!-- Name En Field -->
<div class="form-group">
    {!! Form::label('name_en', 'Name En:') !!}
    <p>{!! $object->name_en !!}</p>
</div>

<!-- Introtext Field -->
<div class="form-group">
    {!! Form::label('introtext', 'Introtext:') !!}
    <p>{!! $object->introtext !!}</p>
</div>

<!-- Introtext En Field -->
<div class="form-group">
    {!! Form::label('introtext_en', 'Introtext En:') !!}
    <p>{!! $object->introtext_en !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $object->text !!}</p>
</div>

<!-- Text En Field -->
<div class="form-group">
    {!! Form::label('text_en', 'Text En:') !!}
    <p>{!! $object->text_en !!}</p>
</div>

<!-- Img Field -->
<div class="form-group">
    {!! Form::label('img', 'Img:') !!}
    <p>{!! $object->img !!}</p>
</div>

<!-- Fb Field -->
<div class="form-group">
    {!! Form::label('fb', 'Fb:') !!}
    <p>{!! $object->fb !!}</p>
</div>

<!-- Inst Field -->
<div class="form-group">
    {!! Form::label('inst', 'Inst:') !!}
    <p>{!! $object->inst !!}</p>
</div>

<!-- Vk Field -->
<div class="form-group">
    {!! Form::label('vk', 'Vk:') !!}
    <p>{!! $object->vk !!}</p>
</div>

<!-- Youtube Field -->
<div class="form-group">
    {!! Form::label('youtube', 'Youtube:') !!}
    <p>{!! $object->youtube !!}</p>
</div>

