<?php

namespace App\Repositories;

use App\Models\Transport;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TransportRepository
 * @package App\Repositories
 * @version January 24, 2018, 9:50 am UTC
 *
 * @method Transport findWithoutFail($id, $columns = ['*'])
 * @method Transport find($id, $columns = ['*'])
 * @method Transport first($columns = ['*'])
*/
class TransportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en',
        'text',
        'text_en',
        'img'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Transport::class;
    }

    public function getFields($request)
    {
        $lang = $request->header('X-Language');
        if($lang=='en'){
            return Transport::select('id','name_en as name','text_en as text','img');
        }
        else{
            return Transport::select('id','name','text','img');
        }
    }


}
