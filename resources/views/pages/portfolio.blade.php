<fieldset>
    <legend>Презентация</legend>
    @foreach($docs as $doc)
        <div class="tr-pdf">
            <div class="form-group col-sm-6">
                {!! Form::text('name[]', $doc->name, ['class' => 'form-control','id'=>'name_'.$doc->id,'required'=>'required','placeholder'=>'Имя файла']) !!}
            </div>
            <div class="form-group col-sm-3 pdf">
                <span class="link_name">{!! $doc->link !!}</span>
            </div>
            <div class="form-group col-sm-3 pdf-btn">
                <div class='btn-group'>

                    <div class="file-input">
                        <input name="ids[]" type="hidden" value="{!!$doc->id  !!}" class="ids">
                        {!! Form::file('link[]',
                            ['id'=>'link_'.$doc->id, 'accept'=>'application/pdf','class'=>'inputfile','data-multiple-caption'=>'{count} files selected']) !!}
                        <label for="link_{!!$doc->id!!}">Загрузить файл</label>
                    </div>

                    <a href="#" class='btn btn-danger btn-sm'>
                        Удалить
                    </a>

                </div>
            </div>
        </div>
    @endforeach

    <div class="form-group col-sm-6">
        <a href="#" class='btn btn-primary btn-sm add-pdf'>
            Добавить презентацию
        </a>
    </div>
</fieldset>