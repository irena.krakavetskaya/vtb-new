<fieldset class="skybox-adv">
    <legend>Вопросы</legend>
    @foreach($questions as $item)
        <div class="question">
            <div class="col-md-3 text">
                {!! $item->question !!}
            </div>

            <div class="col-md-3  text">
                {!! $item->answer !!}
            </div>

            <div class="col-md-2">
                {!! date('d-m-Y', strtotime($item->date)) !!}
            </div>
            <div class="col-md-2">
                {!! $item->status?'Опубликован':'Не опубликован'; !!}
            </div>
            <div class='btn-group col-md-2'>
                <a href="{{ route('questions.edit', $item->id) }}" class='btn btn-default btn-xs'>
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs delete_item',
                    'id'=>$item->id
                ]) !!}
            </div>
            <div class="clearfix"></div>
        </div>
    @endforeach
</fieldset>