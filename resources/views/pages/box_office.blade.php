@include('pages.img')

<!--
<fieldset class="gallery">
    <legend>Галерея</legend>
    <div class="form-group col-md-12">
        foreach($imgs as $img)
            <div class="col-sm-2">
                <p>
                    <input name="img_skybox[]" type="hidden" value="!!  $img->link  !!}">
                    <a href="/!! $img->link !!}" data-toggle="lightbox">
                        <img src="/!! $img->link !!}" class="img-responsive">
                    </a>
                </p>
                <p class="text-center">
                    <input name="del_img[]" type="hidden" value="!! $img->id !!}" class="del_img">
                    <a href="#" class='btn btn-danger btn-sm delete-img' id="!! $img->id !!}">
                        Удалить
                    </a>
                </p>
            </div>
        endforeach
        <div class="col-sm-2">
            <input type="hidden" name="MAX_UPLOAD_SIZE" value="3000">
            <img id="uploadedimage_museum" height="160px" class="uploadedimage_museum">
            <span id="imageerror_museum" style="font-weight: bold; color: red"></span>
            !! Form::file('img_museum[]', ['id'=>'img_museum', 'accept'=>'image/jpg,image/jpeg,image/png', 'class'=>'img_museum']) !!}
        </div>
    </div>
</fieldset>
-->

@include('pages.contact',$contacts)

<!--
<fieldset>
    <legend>Контакты</legend>
    foreach($contacts as $contact)
        <div class="form-group col-sm-6">
            !! Form::label($contact->contactType->type, $contact->contactType->name.':') !!}
            !! Form::text($contact->contactType->type, $contact->value,  ['class' => 'form-control']) !!}
        </div>
    endforeach
</fieldset>
-->



@include('pages.doc')
<!--
<fieldset>
    <legend>Скачать презентацию</legend>
    foreach($docs as $doc)
        <div class="tr-pdf">
            <div class="form-group col-sm-6">
                !! Form::text('name[]', $doc->name, ['class' => 'form-control','id'=>'name_'.$doc->id,'required'=>'required','placeholder'=>'Имя файла']) !!}
            </div>
            <div class="form-group col-sm-3 pdf">
                <span class="link_name">!! $doc->link !!}</span>
            </div>
            <div class="form-group col-sm-3 pdf-btn">
                <div class='btn-group'>

                    <div class="file-input">
                        <input name="ids[]" type="hidden" value="!!$doc->id  !!}" class="ids">
                        !! Form::file('link[]',
                            ['id'=>'link_'.$doc->id, 'accept'=>'application/pdf','class'=>'inputfile','data-multiple-caption'=>'{count} files selected']) !!}
                        <label for="link_!!$doc->id!!}">Загрузить файл</label>
                    </div>

                    <a href="#" class='btn btn-danger btn-sm'>
                        Удалить
                    </a>

                </div>
            </div>
        </div>
    endforeach

    <div class="form-group col-sm-6">
        <a href="#" class='btn btn-primary btn-sm add-pdf'>
            Добавить файл
        </a>
    </div>
</fieldset>
    -->