<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePlaceAPIRequest;
use App\Http\Requests\API\UpdatePlaceAPIRequest;
use App\Models\Place;
use App\Models\Point;
use App\Models\Translate;
use App\Models\Variable;
use App\Repositories\PlaceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PlaceController
 * @package App\Http\Controllers\API
 */

class PlaceAPIController extends AppBaseController
{
    /** @var  PlaceRepository */
    private $placeRepository;

    public function __construct(PlaceRepository $placeRepo)
    {
        $this->placeRepository = $placeRepo;
    }

    /**
     * Display a listing of the Place.
     * GET|HEAD /places
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->placeRepository->pushCriteria(new RequestCriteria($request));
        //$this->placeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $places = $this->placeRepository->all();

        $translates=new Translate();
        $ice_palace= $translates->getRu('ice_palace');//Translate::where('front', 'ice_palace')->first()->ru;
        $all=$translates->getRu('all');//$translates->where('front', 'all')->first()->ru;

        $arr=[];
        foreach ($places as $key=>$val){
            if($val->id==1){
                $val->name=$all;
                $val->key='all';
            }
            elseif($val->id==2){
                $val->name=$ice_palace;
                $val->key='1';
            }
            else{
                $val->key=(string)$val->id;
            }
            unset($val->id,$val->name_en);
            array_push($arr,$val);
        }

        return $arr;

        //return $this->sendResponse($places->toArray(), 'Places retrieved successfully');
    }

    /**
     * Store a newly created Place in storage.
     * POST /places
     *
     * @param CreatePlaceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePlaceAPIRequest $request)
    {
        $input = $request->all();

        $places = $this->placeRepository->create($input);

        return $this->sendResponse($places->toArray(), 'Place saved successfully');
    }

    /**
     * Display the specified Place.
     * GET|HEAD /places/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id,$floor=null)
    {
        /** @var Place $place */
        $place = $this->placeRepository->findWithoutFail($id);

        if (empty($place)) {
            return $this->sendError('Place not found');
        }

        if($id==1 || $id==2){  //arena - big & small
            $point = new Point();
            if (empty($floor)) {
                $points = $point->getPoints($id);
            }
            else{
                $points =$point->getPointsOnFloor($id,$floor);
            }
        }
        else{ //parking
            $item = $place->text;
            $host=Variable::where('id',3)->value('value');
            $item = str_replace('"','\'',$item);
            $pattern = '/src=\'/';
            $replacement = 'src=\''.$host; //'src=\'http://dev.vtb.indi.by';
            $item = preg_replace($pattern, $replacement,$item);
            $item =  '<body>'.$item.'</body>';
            return array('text'=>$item);
            /*
            $string = file_get_contents('webview.html');
            $delimiter = '<body>';
            $arr =explode ( $delimiter , $string);
            $str1=$arr[0];
            $str2=$arr[1];
            $item =  $str1. $delimiter.$place->text.$str2;
            return array('text'=>$item);
            */
        }

        return $points;

        //return $this->sendResponse($place->toArray(), 'Place retrieved successfully');
    }

    /**
     * Update the specified Place in storage.
     * PUT/PATCH /places/{id}
     *
     * @param  int $id
     * @param UpdatePlaceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePlaceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Place $place */
        $place = $this->placeRepository->findWithoutFail($id);

        if (empty($place)) {
            return $this->sendError('Place not found');
        }

        $place = $this->placeRepository->update($input, $id);

        return $this->sendResponse($place->toArray(), 'Place updated successfully');
    }

    /**
     * Remove the specified Place from storage.
     * DELETE /places/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Place $place */
        $place = $this->placeRepository->findWithoutFail($id);

        if (empty($place)) {
            return $this->sendError('Place not found');
        }

        $place->delete();

        return $this->sendResponse($id, 'Place deleted successfully');
    }
}
