@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="pull-left events-btn">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('events.index') !!}" target="_blank">События</a>
        </div>
        <h1 class="pull-left">Команды</h1>
   </section>
   <div class="content">
       <div class="clearfix"></div>
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($team, ['route' => ['teams.update', $team->id], 'method' => 'patch','files'=>true,'enctype'=>'multipart/form-data']) !!}

                        @include('teams.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection