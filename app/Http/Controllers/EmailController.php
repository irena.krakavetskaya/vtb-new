<?php

namespace App\Http\Controllers;

use App\DataTables\EmailDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEmailRequest;
use App\Http\Requests\UpdateEmailRequest;
use App\Models\Notification;
use App\Models\Variable;
use App\Repositories\EmailRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Unisender\ApiWrapper\UnisenderApi;

class EmailController extends AppBaseController
{


    /** @var  EmailRepository */
    private $emailRepository;

    public function __construct(EmailRepository $emailRepo)
    {
        $this->emailRepository = $emailRepo;
        $this->createList();
    }

    /**
     * Display a listing of the Email.
     *
     * @param EmailDataTable $emailDataTable
     * @return Response
     */
    public function index(EmailDataTable $emailDataTable)
    {
        return $emailDataTable->render('emails.index');
    }

    /**
     * Show the form for creating a new Email.
     *
     * @return Response
     */
    public function create()
    {
        return view('emails.create');
    }

    /**
     * Store a newly created Email in storage.
     *
     * @param CreateEmailRequest $request
     *
     * @return Response
     */
    public function store(CreateEmailRequest $request)
    {
        $input = $request->all();

        $email = $this->emailRepository->create($input);

        $uni = $this->connectUnisender();
        $list_id  = $this->getList($uni);
        $this->subscribeEmail($email->email, $email->name, $list_id);

        Flash::success('Email saved successfully.');
        return redirect(route('emails.index'));
    }

    /**
     * Display the specified Email.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $email = $this->emailRepository->findWithoutFail($id);

        if (empty($email)) {
            Flash::error('Email not found');

            return redirect(route('emails.index'));
        }

        return view('emails.show')->with('email', $email);
    }

    /**
     * Show the form for editing the specified Email.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $email = $this->emailRepository->findWithoutFail($id);

        if (empty($email)) {
            Flash::error('Email not found');

            return redirect(route('emails.index'));
        }

        return view('emails.edit')->with('email', $email);
    }

    /**
     * Update the specified Email in storage.
     *
     * @param  int              $id
     * @param UpdateEmailRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmailRequest $request)
    {
        $email = $this->emailRepository->findWithoutFail($id);

        if (empty($email)) {
            Flash::error('Email not found');

            return redirect(route('emails.index'));
        }

        $email = $this->emailRepository->update($request->all(), $id);

        $uni = $this->connectUnisender();
        $list_id  = $this->getList($uni);
        $this->subscribeEmail($email->email, $email->name, $list_id);


        Flash::success('Email updated successfully.');
        return redirect(route('emails.index'));
    }

    /**
     * Remove the specified Email from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $email = $this->emailRepository->findWithoutFail($id);
        $email_field = $email->email;

        if (empty($email)) {
            Flash::error('Email not found');

            return redirect(route('emails.index'));
        }

        $this->emailRepository->delete($id);

        $uni = $this->connectUnisender();
        $list_id  = $this->getList($uni);
        $this->unsubscribeEmail('email', $email_field,$list_id);

        Flash::success('Email deleted successfully.');
        return redirect(route('emails.index'));
    }

    /*public function connectUnisender()
    {
        $platform = 'VTB Ice Palace';
        $uni = new UnisenderApi('api key here', 'UTF-8', 4, null, false, $platform);

        return $uni;
    }*/

    public function subscribeEmail($user_email, $user_name,$user_lists)
    {
        $user_name = iconv('cp1251', 'utf-8',  $user_name);
        $user_lists = "354168";
        //$user_tag = urlencode("Added using API");

        // Создаём POST-запрос
        $post = array (
            'api_key' => env('UNISENDER_KEY', ''),
            'list_ids' => $user_lists,
            'fields[email]' => $user_email,
            'fields[Name]' => $user_name,
            //'tags' => $user_tag
        );

        $this->makeCurl($post, 'subscribe');

        // Новый пользователь успешно добавлен
        // echo "Added. ID is " . $jsonObj->result->person_id;
    }

    public function unsubscribeEmail($user_type, $user_email, $user_lists)
    {
        // Создаём POST-запрос
        $post = array (
            'api_key' => env('UNISENDER_KEY', ''),
            'list_ids' => $user_lists,
            'contact_type' => $user_type,
            'contact' => $user_email
        );

        $this->makeCurl($post, 'unsubscribe');
        // Адресат успешно отписан
        //echo "Unsubscribed";
    }



    public function createList()
    {
        $platform = 'VTB';
        $uni=new UnisenderApi(env('UNISENDER_KEY', ''), 'UTF-8', 4, null, false, $platform);
        $result=$uni->createList(Array("title"=>"OurNewList")); //{"result":{"id":353358}}
        //$list = json_decode($result);
        //return $list->id;
        //var_dump($list);

    }


}
