<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePointAPIRequest;
use App\Http\Requests\API\UpdatePointAPIRequest;
use App\Models\Point;
use App\Repositories\PointRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PointController
 * @package App\Http\Controllers\API
 */

class PointAPIController extends AppBaseController
{
    /** @var  PointRepository */
    private $pointRepository;

    public function __construct(PointRepository $pointRepo)
    {
        $this->pointRepository = $pointRepo;
    }

    /**
     * Display a listing of the Point.
     * GET|HEAD /points
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->pointRepository->pushCriteria(new RequestCriteria($request));
        $this->pointRepository->pushCriteria(new LimitOffsetCriteria($request));
        $points = $this->pointRepository->all();

        return $this->sendResponse($points->toArray(), 'Points retrieved successfully');
    }

    /**
     * Store a newly created Point in storage.
     * POST /points
     *
     * @param CreatePointAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePointAPIRequest $request)
    {
        $input = $request->all();

        $points = $this->pointRepository->create($input);

        return $this->sendResponse($points->toArray(), 'Point saved successfully');
    }

    /**
     * Display the specified Point.
     * GET|HEAD /points/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Point $point */
        $point = $this->pointRepository->findWithoutFail($id);

        if (empty($point)) {
            return $this->sendError('Point not found');
        }

        return $this->sendResponse($point->toArray(), 'Point retrieved successfully');
    }

    /**
     * Update the specified Point in storage.
     * PUT/PATCH /points/{id}
     *
     * @param  int $id
     * @param UpdatePointAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePointAPIRequest $request)
    {
        $input = $request->all();

        /** @var Point $point */
        $point = $this->pointRepository->findWithoutFail($id);

        if (empty($point)) {
            return $this->sendError('Point not found');
        }

        $point = $this->pointRepository->update($input, $id);

        return $this->sendResponse($point->toArray(), 'Point updated successfully');
    }

    /**
     * Remove the specified Point from storage.
     * DELETE /points/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Point $point */
        $point = $this->pointRepository->findWithoutFail($id);

        if (empty($point)) {
            return $this->sendError('Point not found');
        }

        $point->delete();

        return $this->sendResponse($id, 'Point deleted successfully');
    }
}
