API приложения "ВТБ"
===================================

* HTTP заголовок, версия API

    X-API-Version: 1.01

===================================

* HTTP заголовок с указанием языка

	X-Language: ru

===================================

* HTTP заголовок для формата:

	Accept: application/json

===================================

* api/v1/{end-point}[?param1=value1[&param2=value2...]]

Пути к API указаны относительно корня хоста. Корень API расположен здесь:

    http://dev.vtb.indi.by/

Запросы 
-------

Афиша:

01) [Список событий афишы](public/doc/API-01.md)

02) [Фильтры событий](public/doc/API-02.md)

03) [Карточка мероприятия](public/doc/API-03.md)

04) [Анонсы](public/doc/API-04.md)


Базовые:

05) [Список переменных](public/doc/API-05.md)


Главная:

06) [Ледовый дворец](public/doc/API-06.md)

07) [VIP-ложи](public/doc/API-07.md)


<!--Навигация:

13) [Список точек арен с описанием](public/doc/API-13.md)-->


Услуги:

<!--14) [Список услуг](public/doc/API-14.md)-->

08) [Аренда льда](public/doc/API-08.md)

09) [Экскурсии](public/doc/API-09.md)

10) [Бюро находок](public/doc/API-10.md)

11) [Детский день рождения](public/doc/API-11.md)


Контакты:

12) [Телефоны и адреса](public/doc/API-12.md)

13) [Страница схемы проезда](public/doc/API-13.md)

14) [Паркинг](public/doc/API-14.md)


Подсказки:

15) [Подсказки](public/doc/API-15.md)


Токены и пуши:

16) [Токены](public/doc/API-16.md)