<fieldset>
    <legend>Контакты</legend>
    @foreach($contacts as $contact)
        <div class="form-group col-sm-6">
            {!! Form::label($contact->contactType->type, $contact->contactType->name.':') !!}
            {!! Form::text($contact->contactType->type, $contact->value,  ['class' => 'form-control']) !!}
        </div>

            @if(isset($contact) && ($contact->contact_type_id==3 || $contact->contact_type_id==4))
                <div class="form-group col-sm-6">
                    {!! Form::label('value_en', $contact->contactType->name .' (English):') !!}
                    {!! Form::text($contact->contactType->type.'_en', $contact->value_en, ['class' => 'form-control']) !!}
                </div>
            @endif
            <div class="clearfix"></div>

    @endforeach
</fieldset>