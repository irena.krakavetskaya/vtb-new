{!! Form::open(['route' => ['objects.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <!--<a href="{ route('objects.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    <a href="{ route('objects.edit-contacts', ['id'=>$id,'contacts'=>'true']) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>-->
    <a href="objects/edit-contacts/{!! $id !!}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "return confirm('Вы уверены?')"
    ]) !!}
</div>
{!! Form::close() !!}
