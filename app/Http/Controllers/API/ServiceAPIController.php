<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateServiceAPIRequest;
use App\Http\Requests\API\UpdateServiceAPIRequest;
use App\Models\Doc;
use App\Models\Event;
use App\Models\Object;
use App\Models\Page;
use App\Models\Service;
use App\Models\Variable;
use App\Repositories\ServiceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ServiceController
 * @package App\Http\Controllers\API
 */

class ServiceAPIController extends AppBaseController
{
    /** @var  ServiceRepository */
    private $serviceRepository;

    public function __construct(ServiceRepository $serviceRepo)
    {
        $this->serviceRepository = $serviceRepo;
    }

    /**
     * Display a listing of the Service.
     * GET|HEAD /services
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //$this->serviceRepository->pushCriteria(new RequestCriteria($request));
        //$this->serviceRepository->pushCriteria(new LimitOffsetCriteria($request));

        //$services = $this->serviceRepository->all(['id','name']);//->all(); //old version

        //$services=Page::where('parent_id', 4)->select('id','pagetitle AS name')->get();
        $services=$this->serviceRepository->getFields($request);
        $services=$services->where('parent_id', 4)->get();

        return $services;

        //return $this->sendResponse($services->toArray(), 'Services retrieved successfully');
    }

    /**
     * Store a newly created Service in storage.
     * POST /services
     *
     * @param CreateServiceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateServiceAPIRequest $request)
    {
        $input = $request->all();

        $services = $this->serviceRepository->create($input);

        return $this->sendResponse($services->toArray(), 'Service saved successfully');
    }

    /**
     * Display the specified Service.
     * GET|HEAD /services/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        /** @var Service $service */
        /*
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            return $this->sendError('Service not found');
        }
        */
        //$service = $service->getService($id); //old

        $page_model=new Page();
        $page = $page_model->find($id);//where('id',$id);
        //$page= Page::find($id);

        //get pdf
        $doc= new Doc();
        $pdf=$doc->getDocFields($request);
        $pdf=$pdf->where('page_id',$id)->get();
        $arr =['pdf'=>$pdf];

        //get contacts
        $contacts =$page->contacts()->get();
        foreach ($contacts as $contact){
            $type= $contact->contactType->type;
            $value = $contact->value;
            $arr[$type]=$value;
        }

        //get events
        $date_now= date('Y-m-d');
        $ev= new Event();
        $events=$ev->getEventFields($request);
        $evs= $events->where('type','excursion')
            ->whereDate('date','>=', $date_now)
            ->orderBy('date','asc')
            ->limit(3)
            ->get();
        $events=['events'=>$ev->formatEvent($evs)];

        //get page fields
        $service=$page_model->getPageFields($request);
        $service=$service
            ->where('id',$id)
            ->first();

        //modify text
        if($page->content!=''){
            $page->content=$page_model->modifyContent($page->content);
        }


        $service =$service->toArray();
        $service =array_merge($service,$arr);
        if($page->id==29){
            $service =array_merge($service,$events);
        }

        return $service;
        //return $this->sendResponse($service->toArray(), 'Service retrieved successfully');
    }

    /**
     * Update the specified Service in storage.
     * PUT/PATCH /services/{id}
     *
     * @param  int $id
     * @param UpdateServiceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServiceAPIRequest $request)
    {
        $input = $request->all();

        /** @var Service $service */
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            return $this->sendError('Service not found');
        }

        $service = $this->serviceRepository->update($input, $id);

        return $this->sendResponse($service->toArray(), 'Service updated successfully');
    }

    /**
     * Remove the specified Service from storage.
     * DELETE /services/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Service $service */
        $service = $this->serviceRepository->findWithoutFail($id);

        if (empty($service)) {
            return $this->sendError('Service not found');
        }

        $service->delete();

        return $this->sendResponse($id, 'Service deleted successfully');
    }
}
