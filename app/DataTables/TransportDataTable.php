<?php

namespace App\DataTables;

use App\Models\Transport;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class TransportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'transports.datatables_actions')
            ->editColumn('img', function(Transport $transport) {
                $str ='<a href="/'.$transport->img.'" data-toggle="lightbox"><img src="/'.$transport->img.'" class="img-responsive tr-img"></a>';
                return $str;
            })
            ->editColumn('text', function(Transport $transport) {
                $len=840;
                $what = $transport->text;
                if (strlen($what) > $len) {
                    $what = preg_replace('/^(.{' . $len. ',}? ).*$/is', '$1', $what) . '...';
                }
                $what =preg_replace('/\r\n/', '<br>', $what);
                return '<div class="text">'.$what.'<div>';
            })
            ->editColumn('text_en', function(Transport $transport) {
                $len=840;
                $what = $transport->text_en;
                if (strlen($what) > $len) {
                    $what = preg_replace('/^(.{' . $len. ',}? ).*$/is', '$1', $what) . '...';
                }
                $what =preg_replace('/\r\n/', '<br>', $what);
                return '<div class="text">'.$what.'<div>';
            })
            ->rawColumns(['text', 'text_en', 'img', 'action']);

        //return $dataTable->addColumn('action', 'transports.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Post $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transport $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px','title'  => 'Действия'])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'asc']],
                'pageLength' => 50,
                'searching'=> false,
                'language' => [
                    'url' => url('//cdn.datatables.net/plug-ins/1.10.15/i18n/Russian.json') //js/dataTables/language.json
                ],
                'buttons' => [
                    'create',
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id' => ['name' => 'id', 'data' => 'id','title'=>'№', 'width'=>20],
            'name' => ['name' => 'name', 'data' => 'name','title'=>'Название', 'width'=>15],
            'name_en'=>['name' => 'name_en', 'data' => 'name_en','title'=>'Название (English)', 'width'=>15],
            'img'=> ['name' => 'img', 'data' => 'img','searchable' => false,'orderable'=> false,'title'=>'Схема', 'width'=>20],
            'text'=> ['name' => 'text', 'data' => 'text','title'=>'Описание', 'width'=>20],
            'text_en'=>['name' => 'text_en', 'data' => 'text_en','title'=>'Текст (English)', 'width'=>40],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'transportsdatatable_' . time();
    }
}