<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class HintApiTest extends TestCase
{
    use MakeHintTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateHint()
    {
        $hint = $this->fakeHintData();
        $this->json('POST', '/api/v1/hints', $hint);

        $this->assertApiResponse($hint);
    }

    /**
     * @test
     */
    public function testReadHint()
    {
        $hint = $this->makeHint();
        $this->json('GET', '/api/v1/hints/'.$hint->id);

        $this->assertApiResponse($hint->toArray());
    }

    /**
     * @test
     */
    public function testUpdateHint()
    {
        $hint = $this->makeHint();
        $editedHint = $this->fakeHintData();

        $this->json('PUT', '/api/v1/hints/'.$hint->id, $editedHint);

        $this->assertApiResponse($editedHint);
    }

    /**
     * @test
     */
    public function testDeleteHint()
    {
        $hint = $this->makeHint();
        $this->json('DELETE', '/api/v1/hints/'.$hint->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/hints/'.$hint->id);

        $this->assertResponseStatus(404);
    }
}
