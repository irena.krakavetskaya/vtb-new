<?php

namespace App\Http\Controllers;

use App\DataTables\ObjectDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateObjectRequest;
use App\Http\Requests\UpdateObjectRequest;
use App\Models\Doc;
use App\Models\Img;
use App\Models\Object;
use App\Models\Price;
use App\Models\RinkEvent;
use App\Models\Schedule;
use App\Models\Skybox;
use App\Repositories\ObjectRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;

use App\Models\Translate;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Http\File;
use Illuminate\Validation\Rule;
use Validator;

class ObjectController extends AppBaseController
{
    /** @var  ObjectRepository */
    private $objectRepository;

    public function __construct(ObjectRepository $objectRepo)
    {
        $this->objectRepository = $objectRepo;
    }

    /**
     * Display a listing of the Object.
     *
     * @param ObjectDataTable $objectDataTable
     * @return Response
     */
    public function index(ObjectDataTable $objectDataTable)//
    {
        //return route('events.index');//objects.index1
        return $objectDataTable->render('objects.index');
    }

    /**
     * Show the form for creating a new Object.
     *
     * @return Response
     */
    public function create()
    {
        return view('objects.create');
    }

    /**
     * Store a newly created Object in storage.
     *
     * @param CreateObjectRequest $request
     *
     * @return Response
     */
    public function store(CreateObjectRequest $request)
    {
        $input = $request->all();

        $object = $this->objectRepository->create($input);

        Flash::success('Object saved successfully.');

        return redirect(route('objects.index'));
    }

    /**
     * Display the specified Object.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            Flash::error('Object not found');

            return redirect(route('objects.index'));
        }

        return view('objects.show')->with('object', $object);
    }

    public function editContacts($id)//,$contacts=false
    {

        $object = $this->objectRepository->findWithoutFail($id);
        if (empty($object)) {
            Flash::error('Object not found');
            return redirect(route('objects.index'));
        }

       return view('objects.edit_contacts')->with('object', $object);
    }
    /**
     * Show the form for editing the specified Object.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)//,$contacts=false
    {
        $object = $this->objectRepository->findWithoutFail($id);


        $usefull=Translate::where('front','usefull')->value('ru');
        $docs=$object->docs;


        if($id==1) {
            $data = [
                'object' => $object,
                'usefull' => $usefull,
                'docs' => $docs
            ];
        }

        elseif($id==2){
            $schedule = Schedule::get();
            $imgs = $object->imgs;
            $prices = $object->prices;
            $halls = $object->halls;
            $data = [
                'object' => $object,
                'usefull' => $usefull,
                'docs' => $docs,
                'schedule' => $schedule,
                'imgs' =>$imgs,
                'prices' => $prices,
                'halls' =>$halls,
            ];
        }

        elseif($id==3) {
            $prices = $object->prices;
            $rink_events= new RinkEvent();
            $rink_events = $rink_events->getRinkEvents1();
            $imgs = $object->imgs;
            $data = [
                'object' => $object,
                'prices' => $prices,
                //'rink_events' =>$rink_events,
                'imgs' =>$imgs,
            ];
        }
        elseif($id==4) {
            $skyboxes = Skybox::all();
            $imgs = $object->imgs;
            $data = [
                'object' => $object,
                'imgs' =>$imgs,
                'skyboxes' => $skyboxes,
            ];
        }
        else{
            $data=[
                'object' => $object
            ];
        }

        if (empty($object)) {
            Flash::error('Object not found');
            return redirect(route('objects.index'));
        }


        return view('objects.edit')->with($data);//with('object', $object);
    }

    /**
     * Update the specified Object in storage.
     *
     * @param  int              $id
     * @param UpdateObjectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateObjectRequest $request)
    {
        $object = $this->objectRepository->findWithoutFail($id);


        //museum
        //schedule in museum
        if($id==2){
            $sch= [
                    'sch_id' => $request->sch_id,
                    'day_off' => $request->day_off,
                    'work_from' => $request->work_from,
                    'work_till' =>$request->work_till,
                    'break' => $request->break,
                    'break_from' =>  $request->break_from,
                    'break_till' => $request->break_till,
                    ];
            //return $sch;

            for($i=0;$i<7;$i++){
                Schedule::where('id',  $sch['sch_id'][$i])
                            ->update([
                                'day_off' =>  $sch['day_off'][$i],
                                'work_from' => $sch['work_from'][$i],
                                'work_till' => $sch['work_till'][$i],
                                'break' => $sch['break'][$i],
                                'break_from' =>  $sch['break_from'][$i],
                                'break_till' => $sch['break_till'][$i],
                            ]);
            }

            //gallery in museum & skyboxes

        }

        //museum or vip
        if($id==2 || $id==4 || $id==3) {
            if ($id == 2) {
                $folder = 'img_museum';
            }
            if ($id == 4) {
                $folder = 'img_skybox';
            }
            if ($id == 3) {
                $folder = 'img_rink';
            }
            $imgs = $request->file('img_museum');
            $del_img = $request->del_img;
            $insertedIds=[];
            if (isset($imgs)) {
                $last_id = Img::all()->last()->id;
                foreach ($imgs as $key => $val) {

                    if($id == 2 || $id==4 ){
                        $filename = $object->formatImg($val, $folder, 639, 480);
                    }
                    else{
                        $filename = $val->store($folder);
                    }

                    $insertedId= Img::insertGetId([ //insert
                        'id' => ++$last_id,
                        'link' => $filename,
                        'object_id' => $id,
                    ]);
                    array_push($insertedIds,$insertedId);
                    //return $id;
                }
            }
            if(isset($del_img)){
                if(!empty($insertedIds)){
                    array_push($del_img,$insertedIds );
                }

                Img::whereNotIn('id', $del_img)
                     ->where('object_id', $id)
                     ->delete();
            }
        }


        //price
        if(isset($request->id_pr)){
            $id_pr = $request->id_pr;
            $is_shown = $request->is_shown;
            $collection = collect($id_pr);
            $combined = $collection->combine($is_shown);
            $combined->all();
            foreach ($combined as $key=>$val){
                Price::where('id',  $key)
                    ->update([
                        'is_active' =>  $val
                    ]);
            }
        }
        //end of price


        //add pdf
        $files = $request->file('link');
        $names=$request->name;
        $ids=$request->ids;
        $del=$request->del;

        if(isset($files)){
                foreach ($files as $key=>$val){
                     $str=$names[$key].'-'.time();

                     $filename =$val->storeAs(//0
                            'pdf', $str.'.pdf'//0
                     );
                     $count = Doc::where('id', $key+1)->first();

                     if(isset($count)){
                        Doc::where('id', $key+1)
                            ->update([
                                'name' => $names[$key],//0
                                'link'=> $filename,
                                'object_id'=> $id,
                            ]);
                     }
                     else{
                        Doc::insert([
                            'name' => $names[$key],//0
                            'link'=> $filename,
                            'object_id'=> $id,
                        ]);
                     }
                }
        }


       if(isset($names) && !isset($files)){
            //return $ids;
            foreach ($ids as $key=>$id_) {

                $str=$names[$key].'-'.time();
                if(!Storage::disk('local')->exists('pdf/'.$str.'.pdf')){

                    $oldName=Doc::where('id', $id_)->value('link');
                    $oldId=Doc::where('id', $id_)->value('id');

                    Storage::move($oldName, 'pdf/'.$str.'.pdf');

                    Doc::where('id', $oldId)
                        ->update([
                            'name' => $names[$key],
                            'link'=> 'pdf/'.$str.'.pdf',
                        ]);
                }
            }
       }

       if(isset($del)){
            $names= Doc::whereIn('id', $del)->pluck('link');
            Doc::whereIn('id', $del)->delete();
            /*foreach($names as $name){
                Storage::disk('local')->delete($name);
            }*/
       }
       //end of add pdf

        //add preview
        $file = $request->file('img');
        $file1 = $request->input('img3');
        if(isset($file)) {
            $img = $file->store('image');
        }
        else{
            $img =$file1?$file1:'';
        }
        //end of add preview

        //title of pdf block
        if(isset($request->usefull)){
            Translate::where('front','usefull')->update(['ru'=>$request->usefull]);
        }


        if (empty($object)) {
            Flash::error('Object not found');
            return redirect(route('objects.index'));
        }

        //$object = $this->objectRepository->update($request->all(), $id);

        if(isset($request->contacts)){
            $email=$request->email;
            $phone=$request->phone;
            $address=$request->address;
            $open_hours=$request->open_hours;
        }
        else{
            $email=isset($request->email)?$request->email:$object->email;
            $phone=isset($request->phone)?$request->phone:$object->phone;
            $address=isset($request->address)?$request->address:$object->address;
            $open_hours=isset($request->open_hours)?$request->open_hours:$object->open_hours;
        }

        if(isset($request->contacts)){
            $object->update([
                    'address'=>$address,
                    'phone'=>$phone,
                    'email'=>$email,
                    'open_hours'=>$open_hours,
                ]);
            if($id==5){
                $object->update([
                    'vk' => $request->vk,
                    'fb'=> $request->fb,
                    'inst'=> $request->inst,
                    'youtube'=> $request->youtube
                ]);
            }
        }
        else{
            $object->update([
                //'introtext' => $request->introtext,
                'text'=> $request->text,
                'is_active'=> $request->is_active,
                'vk' => $request->vk,
                'fb'=> $request->fb,
                'inst'=> $request->inst,
                'youtube'=> $request->youtube,
                'img'=>$img,
                'address'=>$address,
                'phone'=>$phone,
                'email'=>$email,
                'video'=>$request->video,
                'open_hours'=>$open_hours,
            ]);
        }



        switch($id){
            case(1): $obj_name='арене';break;
            case(2): $obj_name='музее';break;
            case(3): $obj_name='катке';break;
            case(4): $obj_name='Vip-ложе';break;
            default:$obj_name='объекте';
        }

        Flash::success('Информация о '.$obj_name.' успешно обновлена.');


        return isset($request->contacts)?redirect(route('objects.index')):redirect(route('objects.edit',$id));
        //return redirect(route('objects.edit',$id));
    }

    /**
     * Remove the specified Object from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $object = $this->objectRepository->findWithoutFail($id);

        if (empty($object)) {
            Flash::error('Object not found');

            return redirect(route('objects.index'));
        }

        $this->objectRepository->delete($id);

        Flash::success('Object deleted successfully.');

        return redirect(route('objects.index'));
    }
}
