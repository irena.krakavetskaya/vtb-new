<?php

use Faker\Factory as Faker;
use App\Models\Doc;
use App\Repositories\DocRepository;

trait MakeDocTrait
{
    /**
     * Create fake instance of Doc and save it in database
     *
     * @param array $docFields
     * @return Doc
     */
    public function makeDoc($docFields = [])
    {
        /** @var DocRepository $docRepo */
        $docRepo = App::make(DocRepository::class);
        $theme = $this->fakeDocData($docFields);
        return $docRepo->create($theme);
    }

    /**
     * Get fake instance of Doc
     *
     * @param array $docFields
     * @return Doc
     */
    public function fakeDoc($docFields = [])
    {
        return new Doc($this->fakeDocData($docFields));
    }

    /**
     * Get fake data of Doc
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDocData($docFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'link' => $fake->word,
            'name_en' => $fake->word,
            'object_id' => $fake->randomDigitNotNull
        ], $docFields);
    }
}
