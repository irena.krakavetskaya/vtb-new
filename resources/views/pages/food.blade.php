<fieldset class="skybox-adv">
    <legend>Точки питания</legend>
    @foreach($food as $item)
        <div class="food">
            <div class="col-md-3 text">
                {!! $item->name !!}
            </div>

            <div class="col-md-4">
                <a href="/{!! $item->img !!}" data-toggle="lightbox">
                    <img src="/{!! $item->img !!}" class="img-responsive">
                </a>
            </div>

            <div class="col-md-3 text">
                {!! $item->text !!}
            </div>
            <div class='btn-group col-md-2'>
                <a href="{{ route('foods.edit', $item->id) }}" class='btn btn-default btn-xs'><!-- target="_blank"-->
                    <i class="glyphicon glyphicon-edit"></i>
                </a>
                {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs delete_item',
                    'id'=>$item->id
                ]) !!}
            </div>
            <div class="clearfix"></div>
        </div>
    @endforeach
    <div class="form-group col-sm-6">
        <a href="{!! route('foods.create') !!}" class='btn btn-primary btn-sm'><!-- target="_blank"-->
            Добавить заведение
        </a>
    </div>
</fieldset>