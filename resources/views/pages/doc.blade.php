<fieldset>
    <legend>Документы</legend>

    @foreach($docs as $doc)
        <div class="tr-pdf">
            <div class="form-group col-sm-6">
                {!! Form::text('name[]', $doc->name, ['class' => 'form-control','id'=>'name_'.$doc->id,'required'=>'required','placeholder'=>'Имя файла']) !!}
            </div>
            <div class="form-group col-sm-3 pdf">
                <span class="link_name">{!! $doc->link !!}</span>
            </div>
            <div class="form-group col-sm-3 pdf-btn">
                <div class='btn-group'>

                    <div class="file-input">
                        <input name="ids[]" type="hidden" value="{!!$doc->id  !!}" class="ids">
                        {!! Form::file('link[]',
                        ['id'=>'link_'.$doc->id, 'accept'=>'application/pdf','class'=>'inputfile','data-multiple-caption'=>'{count} files selected']) !!}
                        <label for="link_{!!$doc->id!!}">Загрузить файл</label>
                    </div>

                    <a href="#" class='btn btn-danger btn-sm'>
                        Удалить
                    </a>

                </div>
            </div>
        </div>
    @endforeach

    <div class="form-group col-sm-6">
        <a href="#" class='btn btn-primary btn-sm add-pdf'>
            Добавить файл
        </a>
    </div>
</fieldset>

<fieldset>
    <legend>Документы (English)</legend>
    @foreach($docs_en as $doc)
        <div class="tr-pdf-en">
            <div class="form-group col-sm-6">
                {!! Form::text('name_en[]', $doc->name_en, ['class' => 'form-control','id'=>'name_'.$doc->id,'required'=>'required','placeholder'=>'Имя файла']) !!}
            </div>
            <div class="form-group col-sm-3 pdf">
                <span class="link_name">{!! $doc->link_en !!}</span>
            </div>
            <div class="form-group col-sm-3 pdf-btn">
                <div class='btn-group'>

                    <div class="file-input-en">
                        <input name="ids_en[]" type="hidden" value="{!!$doc->id  !!}" class="ids">
                        {!! Form::file('link_en[]',
                        ['id'=>'link_en_'.$doc->id, 'accept'=>'application/pdf','class'=>'inputfile','data-multiple-caption'=>'{count} files selected']) !!}
                        <label for="link_en_{!!$doc->id!!}">Загрузить файл</label>
                    </div>

                    <a href="#" class='btn btn-danger btn-sm'>
                        Удалить
                    </a>

                </div>
            </div>
        </div>
    @endforeach

    <div class="form-group col-sm-6">
        <a href="#" class='btn btn-primary btn-sm add-pdf-en'>
            Добавить файл
        </a>
    </div>
</fieldset>