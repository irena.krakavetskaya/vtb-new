<?php

namespace App\Repositories;

use App\Models\Place;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PlaceRepository
 * @package App\Repositories
 * @version December 19, 2017, 8:20 am UTC
 *
 * @method Place findWithoutFail($id, $columns = ['*'])
 * @method Place find($id, $columns = ['*'])
 * @method Place first($columns = ['*'])
*/
class PlaceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'name_en'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Place::class;
    }
}
