<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $event->id !!}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{!! $event->date !!}</p>
</div>

<!-- Time Field -->
<div class="form-group">
    {!! Form::label('time', 'Time:') !!}
    <p>{!! $event->time !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $event->name !!}</p>
</div>

<!-- Name En Field -->
<div class="form-group">
    {!! Form::label('name_en', 'Name En:') !!}
    <p>{!! $event->name_en !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('text', 'Text:') !!}
    <p>{!! $event->text !!}</p>
</div>

<!-- Text En Field -->
<div class="form-group">
    {!! Form::label('text_en', 'Text En:') !!}
    <p>{!! $event->text_en !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $event->type !!}</p>
</div>

<!-- Place Id Field -->
<div class="form-group">
    {!! Form::label('place_id', 'Place Id:') !!}
    <p>{!! $event->place_id !!}</p>
</div>

<!-- Event Type Id Field -->
<div class="form-group">
    {!! Form::label('event_type_id', 'Event Type Id:') !!}
    <p>{!! $event->event_type_id !!}</p>
</div>

<!-- Team1 Id Field -->
<div class="form-group">
    {!! Form::label('team1_id', 'Team1 Id:') !!}
    <p>{!! $event->team1_id !!}</p>
</div>

<!-- Team2 Id Field -->
<div class="form-group">
    {!! Form::label('team2_id', 'Team2 Id:') !!}
    <p>{!! $event->team2_id !!}</p>
</div>

<!-- Link Field -->
<div class="form-group">
    {!! Form::label('link', 'Link:') !!}
    <p>{!! $event->link !!}</p>
</div>

