<?php

use Faker\Factory as Faker;
use App\Models\ContactType;
use App\Repositories\ContactTypeRepository;

trait MakeContactTypeTrait
{
    /**
     * Create fake instance of ContactType and save it in database
     *
     * @param array $contactTypeFields
     * @return ContactType
     */
    public function makeContactType($contactTypeFields = [])
    {
        /** @var ContactTypeRepository $contactTypeRepo */
        $contactTypeRepo = App::make(ContactTypeRepository::class);
        $theme = $this->fakeContactTypeData($contactTypeFields);
        return $contactTypeRepo->create($theme);
    }

    /**
     * Get fake instance of ContactType
     *
     * @param array $contactTypeFields
     * @return ContactType
     */
    public function fakeContactType($contactTypeFields = [])
    {
        return new ContactType($this->fakeContactTypeData($contactTypeFields));
    }

    /**
     * Get fake data of ContactType
     *
     * @param array $postFields
     * @return array
     */
    public function fakeContactTypeData($contactTypeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'type' => $fake->word,
            'name' => $fake->text
        ], $contactTypeFields);
    }
}
