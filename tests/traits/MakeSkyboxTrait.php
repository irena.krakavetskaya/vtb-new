<?php

use Faker\Factory as Faker;
use App\Models\Skybox;
use App\Repositories\SkyboxRepository;

trait MakeSkyboxTrait
{
    /**
     * Create fake instance of Skybox and save it in database
     *
     * @param array $skyboxFields
     * @return Skybox
     */
    public function makeSkybox($skyboxFields = [])
    {
        /** @var SkyboxRepository $skyboxRepo */
        $skyboxRepo = App::make(SkyboxRepository::class);
        $theme = $this->fakeSkyboxData($skyboxFields);
        return $skyboxRepo->create($theme);
    }

    /**
     * Get fake instance of Skybox
     *
     * @param array $skyboxFields
     * @return Skybox
     */
    public function fakeSkybox($skyboxFields = [])
    {
        return new Skybox($this->fakeSkyboxData($skyboxFields));
    }

    /**
     * Get fake data of Skybox
     *
     * @param array $postFields
     * @return array
     */
    public function fakeSkyboxData($skyboxFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'name_en' => $fake->word,
            'img' => $fake->word
        ], $skyboxFields);
    }
}
