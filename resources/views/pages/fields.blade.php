<div class="col-md-12">
    <fieldset>
        <legend>Заголовок</legend>

        {!! Form::hidden('parent_id', $page->parent_id) !!}

        <div class="form-group col-sm-6">
            {!! Form::label('pagetitle', 'Pagetitle:') !!}
            {!! Form::text('pagetitle', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('pagetitle_en', 'Pagetitle En:') !!}
            {!! Form::text('pagetitle_en', null, ['class' => 'form-control']) !!}
        </div>
    </fieldset>



    @if($page->id==1)
        @include('pages.banner')
    @elseif($page->id==8)
        @include('pages.img')
    @elseif($page->id==9)
        @include('pages.arena')
        @include('pages.img')
    @elseif($page->id==12)
        @include('pages.award')
    @elseif($page->id==13)
        @include('pages.partner')
    @elseif($page->id==14)
        @include('pages.timetable')

    @elseif($page->id==15)
        <div class="form-group col-sm-12">
            <a href="{!! route('places.edit',1) !!}" class="btn btn-primary" target="_blank">Большая арена</a>
            <a href="{!! route('places.edit',2) !!}" class="btn btn-primary" target="_blank">Малая арена</a>
        </div>

    @elseif($page->id==16)
        @include('pages.vip')
    @elseif($page->id==17)
        @include('pages.box_office')
    @elseif($page->id==20)
        @include('pages.food')
    @elseif($page->id==21)
        @include('pages.transport')
    @elseif($page->id==23 || $page->id==24)
        @include('pages.doc')
    @elseif($page->parent_id==4)
        @include('pages.contact')
        @include('pages.doc')

        <fieldset>
            <legend>Доп. текст</legend>
            <div class="form-group col-sm-6">
                {!! Form::label('introtext', 'Introtext:') !!}
                {!! Form::textarea('introtext', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group col-sm-6">
                {!! Form::label('introtext_en', 'Introtext En:') !!}
                {!! Form::textarea('introtext_en', null, ['class' => 'form-control']) !!}
            </div>
        </fieldset>

    @elseif($page->id==31 || $page->id==34)
       @include('pages.news')
    @elseif($page->id==32 || $page->id==33)
       @include('pages.album')
    @elseif($page->id==35)
       @include('pages.contacts')
    @elseif($page->id==37)
        @include('pages.doc')
    @elseif($page->id==38)
       @include('pages.question')

@elseif($page->id==39 || $page->id==40 || $page->id==41)
   @include('pages.img')

@else
   <!--include('pages.img')-->
@endif

    <fieldset>
        <legend>Текст</legend>
        <div class="form-group col-sm-6">
            {!! Form::label('content', 'Content:') !!}
            {!! Form::textarea('content', null, ['class' => 'form-control', 'id'=>'ckeditor1']) !!}
        </div>
        <div class="form-group col-sm-6">
            {!! Form::label('content_en', 'Content En:') !!}
            {!! Form::textarea('content_en', null, ['class' => 'form-control', 'id'=>'ckeditor2']) !!}
        </div>
    </fieldset>

<fieldset>
   <legend>Seo</legend>
   <div class="form-group col-sm-4">
       {!! Form::label('seo_title', 'Seo Title:') !!}
       {!! Form::text('seo_title', null, ['class' => 'form-control']) !!}
   </div>

   <!-- Seo Keywords Field -->
   <div class="form-group col-sm-4">
       {!! Form::label('seo_keywords', 'Seo Keywords:') !!}
       {!! Form::text('seo_keywords', null, ['class' => 'form-control']) !!}
   </div>

   <!-- Seo Description Field -->
   <div class="form-group col-sm-4">
       {!! Form::label('seo_description', 'Seo Description:') !!}
       {!! Form::textarea('seo_description', null, ['class' => 'form-control']) !!}
   </div>
</fieldset>


<fieldset>
   <legend>Дополнительно</legend>
   <div class="form-group col-sm-3">
       {!! Form::label('menuindex', 'Menuindex:') !!}
       {!! Form::number('menuindex', null, ['class' => 'form-control']) !!}
   </div>

   <div class="form-group col-sm-3">
       {!! Form::label('menutitle', 'Menutitle:') !!}
       {!! Form::text('menutitle', null, ['class' => 'form-control']) !!}
   </div>

   <div class="form-group col-sm-3">
       {!! Form::label('menutitle_en', 'Menutitle En:') !!}
       {!! Form::text('menutitle_en', null, ['class' => 'form-control']) !!}
   </div>


   <div class="form-group col-sm-3">
       {!! Form::label('alias', 'Alias:') !!}
       {!! Form::text('alias', null, ['class' => 'form-control']) !!}
   </div>
   <div class="clearfix"></div>
</fieldset>


</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
{!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
<a href="{!! route('pages.edit',$page->id) !!}" class="btn btn-default">Отмена</a>
</div>

